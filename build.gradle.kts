import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import io.gitlab.arturbosch.detekt.Detekt

plugins {
    kotlin("jvm") version "1.9.20"
    // Apply the java-library plugin for API and implementation separation.
    `java-library`
    `maven-publish`
    signing
    id("io.github.gradle-nexus.publish-plugin") version "1.3.0"
    id("io.gitlab.arturbosch.detekt") version "1.23.1"
    id("com.diffplug.spotless") version "6.20.0"
    id("net.razvan.jacoco-to-cobertura") version "1.1.2"
    jacoco
}

val isRelease = !System.getenv("VERSION").isNullOrEmpty()
val signingKey: String? = System.getenv()["SIGNING_KEY"]?.ifBlank { null }
val signingPassword: String? = System.getenv()["SIGNING_PASSWORD"]?.ifBlank { null }

java {
    sourceCompatibility = JavaVersion.VERSION_11
    withSourcesJar()
    withJavadocJar()
}

repositories {
    mavenCentral()
    maven(url = "https://gitlab.com/api/v4/projects/25796063/packages/maven")
}

spotless { kotlin { ktfmt().kotlinlangStyle() } }

detekt {
    basePath = rootDir.toString()
}

tasks.withType<KotlinCompile> { kotlinOptions.jvmTarget = JavaVersion.VERSION_11.toString() }

tasks.withType<Test> { useJUnitPlatform() }

tasks.withType<Detekt> {
    reports {
        custom {
            reportId = "DetektGitlabReport"
            // This tells detekt, where it should write the report to,
            // you have to specify this file in the gitlab pipeline config.
            outputLocation.set(file("$buildDir/reports/detekt/gitlab.json"))
        }
    }
}


dependencies {
    detektPlugins("com.gitlab.cromefire:detekt-gitlab-report:0.3.2")

    // Align versions of all Kotlin components
    api(platform("org.jetbrains.kotlin:kotlin-bom"))

    // Use the Kotlin JDK 8 standard library.
    api("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    testImplementation(platform("io.kotest:kotest-bom:5.6.2"))
    testImplementation("io.kotest:kotest-runner-junit5")
    testImplementation("io.kotest:kotest-assertions-core")
    testImplementation("io.kotest:kotest-property")
}

signing {
    if (signingKey != null && signingPassword != null) {
        useInMemoryPgpKeys(signingKey, signingPassword)
        val extension = extensions
            .getByName("publishing") as PublishingExtension
        sign(extension.publications)
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.nrf110"
            artifactId = "labfile"
            version = System.getenv("VERSION") ?: "0.0.1-SNAPSHOT"

            from(components["kotlin"])
            artifact(tasks["sourcesJar"])
            artifact(tasks["javadocJar"])

            pom {
                name.set("Labfile")
                description.set("Kotlin DSL for dynamic generation of Gitlab CI/CD YAML files")
                url.set("https://nrf110.gitlab.io/labfile/")

                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("nrf110")
                        name.set("Nick Fisher")
                        email.set("nfisher110@gmail.com")
                    }
                }
                scm {
                    connection.set("scm:git:git://gitlab.com/nrf110/labfile")
                    developerConnection.set("scm:git:ssh://gitlab.com/nrf110/labfile.git")
                    url.set("http://gitlab.com/nrf110/labfile")
                }
            }
        }
    }
}

nexusPublishing {
    this.repositories {
        sonatype {
            stagingProfileId.set("364804e9b53468")
            nexusUrl.set(uri("https://s01.oss.sonatype.org/service/local/"))
            snapshotRepositoryUrl.set(uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
            username.set(System.getenv("MAVEN_USER"))
            password.set(System.getenv("MAVEN_PASSWORD"))
        }
    }
}