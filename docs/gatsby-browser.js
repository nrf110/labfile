import Prism from 'prism-react-renderer/prism'

(typeof global !== 'undefined' ? global : window).Prism = Prism

require("prismjs/themes/prism-dark.css");
require('prismjs/components/prism-kotlin')
require('prismjs/components/prism-ruby')
require('prismjs/components/prism-yaml')