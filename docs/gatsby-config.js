module.exports = {
  siteMetadata: {
    siteTitle: `Labfile Docs`,
    defaultTitle: `Labfile Docs`,
    siteTitleShort: `Rocket Docs`,
    siteDescription: `Labfile - Kotlin-based Gitlab CI/CD pipeline generator`,
    siteUrl: `https://nrf110.gitlab.io`,
    siteAuthor: `@nrf110`,
    siteImage: `/banner.png`,
    siteLanguage: `en`,
    themeColor: `#8257E6`,
    basePath: process.env.BASE_PATH ? process.env.BASE_PATH : '/',
  },
  plugins: [
    {
      resolve: `@rocketseat/gatsby-theme-docs`,
      options: {
        configPath: `src/config`,
        docsPath: `src/docs`,
        homePath: `src/home`,
        yamlFilesPath: `src/yamlFiles`,
        repositoryUrl: `https://gitlab.com/nrf110/labfile`,
        baseDir: `docs`,
        gatsbyRemarkPlugins: [
          `gatsby-transformer-remark`,
          `gatsby-remark-prismjs`,
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Labfile Docs`,
        short_name: `Labfile Docs`,
        start_url: `/`,
        background_color: `#ffffff`,
        display: `standalone`,
        icon: `static/favicon.png`,
      },
    },
    `gatsby-plugin-sitemap`,
    // {
    //   resolve: `gatsby-plugin-google-analytics`,
    //   options: {
    //     trackingId: `YOUR_ANALYTICS_ID`,
    //   },
    // },
    `gatsby-plugin-remove-trailing-slashes`,
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `https://nrf110.gitlab.io/labfile`,
      },
    },
    {
      resolve: 'gatsby-transformer-remark',
      options: {
        plugins: [
          {
            resolve: 'gatsby-remark-prismjs',
          }
        ]
      }
    },
    `gatsby-plugin-offline`,
  ],
};
