---
title: script
---

Use script to specify a shell script for the runner to execute.

All jobs except [trigger](../trigger) jobs require a `script` keyword.

For example:

```kotlin
job("job") {
    script {
        +shell("uname -a")
        +shell("bundle exec rspec")
    }
}
```

Sometimes, `script` commands must be wrapped in single or double quotes. For example, commands that contain a colon (`:`) must be wrapped in single quotes (`'`). The YAML parser needs to interpret the text as a string rather than a “key: value” pair.

For example, this script uses a colon:

```kotlin
job {
    script {
        shell("curl --request POST --header 'Content-Type: application/json' \"https://gitlab/api/v4/projects\"")
    }
}
```

To be considered valid YAML, you must wrap the entire command in single quotes. If the command already uses single quotes, you should change them to double quotes (`"`) if possible:

```kotlin
job {
    script {
        +shell("'curl --request POST --header \"Content-Type: application/json\" \"https://gitlab/api/v4/projects\"'")
    }
}
```

You can verify the syntax is valid with the CI Lint tool. **NOTE**: in Kotlin, your double-quotes must be escaped with a forward-slash (`\`) to produce the expected YAML.

Be careful when using these characters as well:

* `{`, `}`, `[`, `]`, `,`, `&`, `*`, `#`, `?`, `|`, `-`, `<`, `>`, `=`, `!`, `%`, `@`, ``` ` ```.
If any of the script commands return an exit code other than zero, the job fails and further commands are not executed. Store the exit code in a variable to avoid this behavior:

```kotlin
job("job") {
    script {
        +shell("false || exit_code=\$?")
        +shell("if [ \$exit_code -ne 0 ]; then echo \"Previous command failed\"; fi;")
    }
}
```

## `beforeScript`
Use `beforeScript` to define an array of commands that should run before each job, but after [artifacts](../artifacts) are restored.

Scripts you specify in `beforeScript` are concatenated with any scripts you specify in the main script. The combine scripts execute together in a single shell.

You can overwrite a globally-defined `beforeScript` if you define it in a job:

```kotlin
globalDefaults {
    beforeScript {
        +shell("echo \"Execute this script in all jobs that don't already have a before_script section.\"")
    }
}

job("job1") {
    script {
        +shell("echo \"This script executes after the global before_script.\"")
    }
}

job("job") {
    beforeScript {
        +shell("echo \"Execute this script instead of the global before_script.\"")
    }
    script {
        +shell("echo \"This script executes after the job's `before_script`\"")
    }
}
```

## `afterScript`
###
Use `afterScript` to define an array of commands that run after each job, including failed jobs.

If a job times out or is cancelled, the `afterScript` commands do not execute. An [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/15603) exists to support executing `afterScript` commands for timed-out or cancelled jobs.

Scripts you specify in `afterScript` execute in a new shell, separate from any `beforeScript` or `script` scripts. As a result, they:

* Have a current working directory set back to the default.
* Have no access to changes done by scripts defined in `beforeScript` or `script`, including:
** Command aliases and variables exported in `script` scripts.
** Changes outside of the working tree (depending on the runner executor), like software installed by a `beforeScript` or `script` script.
* Have a separate timeout, which is hard coded to 5 minutes. See the [related issue](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/2716) for details.
* Don’t affect the job’s exit code. If the `script` section succeeds and the `afterScript` times out or fails, the job exits with code `0` (`Job Succeeded`).

```kotlin
globalDefaults {
    afterScript {
        +shell("echo \"Execute this script in all jobs that don't already have an after_script section.\"")
    }
}

job("job1") {
    script {
        +shell("echo \"This script executes first. When it completes, the global after_script executes.\"")
    }
}

job("job") {
    script {
        +shell("echo \"This script executes first. When it completes, the job's `after_script` executes.\"")
    }
    afterScript {
        +shell("echo \"Execute this script instead of the global after_script.\"")
    }
}
```
