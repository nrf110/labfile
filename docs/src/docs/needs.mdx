---
title: needs
---

Use `needs` to execute jobs out-of-order. Relationships between jobs that use `needs` can be visualized as a [directed acyclic graph](https://docs.gitlab.com/ee/ci/directed_acyclic_graph/index.html).

You can ignore stage ordering and run some jobs without waiting for others to complete. Jobs in multiple stages can run concurrently.

**Keyword type**: Job keyword. You can use it only as part of a job.

**Possible inputs**:

- An array of jobs.
- An empty array (`[]`), to set the job to start as soon as the pipeline is created.

**Example of `needs`**:

```kotlin
pipeline {
    val build by stage()
    val test by stage()

    job("linux:build") {
      stage = build
      script {
        +shell("echo \"Building linux...\"")
      }
    }

    job("mac:build") {
        stage = build
        script {
            +shell("echo \"Building mac...\"")
        }
    }

    job("lint") {
        stage = test
        needs {}
        script {
            +shell("echo \"Linting...\"")
        }
    }

    job("linux:rspec") {
      stage = test
      needs {
        +"linux:build"
      }
      script {
        +shell("echo \"Running rspec on linux...\"")
      }
    }

    job("mac:rspec") {
      stage = test
      needs {
        +"mac:build"
      }
      script {
        +shell("echo \"Running rspec on mac...\"")
      }
    }

    job("production") {
      stage = deploy
      script {
        +shell("echo \"Running production...\"")
      }
      environment = production
    }
}
```

This example creates four paths of execution:

- Linter: The `lint` job runs immediately without waiting for the `build` stage to complete because it has no needs (`needs: []`).
- Linux path: The `linux:rspec` job runs as soon as the `linux:build` job finishes, without waiting for `mac:build` to finish.
- macOS path: The `mac:rspec` jobs runs as soon as the `mac:build` job finishes, without waiting for `linux:build` to finish.
- The `production` job runs as soon as all previous jobs finish: `linux:build`, `linux:rspec`, `mac:build`, `mac:rspec`.

**Additional details**:

- The maximum number of jobs that a single job can have in the `needs` array is limited:
    - For GitLab.com, the limit is 50. For more information, see this [infrastructure issue](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/7541).
    - For self-managed instances, the default limit is 50. This limit [can be changed](https://docs.gitlab.com/ee/administration/cicd.html#set-the-needs-job-limit).
- If `needs` refers to a job that uses the [`parallel`](../parallel) keyword, it depends on all jobs created in parallel, not just one job. It also downloads artifacts from all the parallel jobs by default. If the artifacts have the same name, they overwrite each other and only the last one downloaded is saved.
    - To have `needs` refer to a subset of parallelized jobs (and not all of the parallelized jobs), use the [`needs:parallel:matrix`](#needsparallelmatrix) keyword.
- In [GitLab 14.1](https://gitlab.com/gitlab-org/gitlab/-/issues/30632) and later you can refer to jobs in the same stage as the job you are configuring. This feature is enabled on GitLab.com and ready for production use. On self-managed [GitLab 14.2](https://gitlab.com/gitlab-org/gitlab/-/issues/30632) and later this feature is available by default.
- In GitLab 14.0 and older, you can only refer to jobs in earlier stages. Stages must be explicitly defined for all jobs that use the `needs` keyword, or are referenced in a job’s `needs` section.
- In GitLab 13.9 and older, if `needs` refers to a job that might not be added to a pipeline because of `only`, `except`, or `rules`, the pipeline might fail to create. In GitLab 13.10 and later, use the [`needs:optional`](#needsoptional) keyword to resolve a failed pipeline creation.
- If a pipeline has jobs with `needs: []` and jobs in the `.pre` stage, they will all start as soon as the pipeline is created. Jobs with `needs: []`` start immediately, and jobs in the `.pre` stage also start immediately.

## `needs:artifacts`
Introduced in GitLab 12.6.

When a job uses `needs`, it no longer downloads all artifacts from previous stages by default, because jobs with `needs` can start before earlier stages complete. With `needs` you can only download artifacts from the jobs listed in the `needs` configuration.

Use `artifacts: true` (default) or `artifacts: false` to control when artifacts are downloaded in jobs that use `needs`.

**Keyword type**: Job keyword. You can use it only as part of a job. Must be used with `needs:job`.

**Possible inputs**:

- `true` (default) or `false`.

**Example of `needs:artifacts`**:

```kotlin
pipeline {
    val test by stage()

    job("test-job1") {
        stage = test
        needs {
            +need {
                job = "build_job1"
                artifacts = true
            }
        }
    }

    job("test-job2") {
        stage = test
        needs {
            +need {
                job = "build_job2"
                artifacts = false
            }
        }
    }

    job("test-job3") {
        needs {
            +need {
                job = "build_job1"
                artifacts = true
            }
            +need {
                job = "build_job2"
            }
            +"build_job3"
        }
    }
}
```

In this example:

- The `test-job1` job downloads the `build_job1` artifacts
- The `test-job2` job does not download the `build_job2` artifacts.
- The `test-job3` job downloads the artifacts from all three `build_jobs`, because `artifacts` is `true`, or defaults to `true`, for all three needed jobs.

**Additional details**:

- In GitLab 12.6 and later, you can’t combine the [`dependencies`](../dependencies) keyword with `needs`.

## `needs:project`

Use `needs:project` to download artifacts from up to five jobs in other pipelines. The artifacts are downloaded from the latest successful specified job for the specified ref. To specify multiple jobs, add each as separate array items under the `needs` keyword.

If there is a pipeline running for the ref, a job with `needs:project` does not wait for the pipeline to complete. Instead, the artifacts are downloaded from the latest successful run of the specified job.

`needs:project` must be used with `job`, `ref`, and `artifacts`.

**Keyword type**: Job keyword. You can use it only as part of a job.

**Possible inputs**:

- `needs:project`: A full project path, including namespace and group.
- `job`: The job to download artifacts from.
- `ref`: The ref to download artifacts from.
- `artifacts`: Must be `true` to download artifacts.

**Examples of `needs:project`**:

```kotlin
val build by stage()

job("build_job")
  stage = build
  script {
    +shell("ls -lhR")
  }
  needs {
    +need {
      project = "namespace/group/project-name"
      job = "build-1"
      ref = "main"
      artifacts = true
    }
    +need {
      project = "namespace/group/project-name-2"
      job = "build-2"
      ref = "main"
      artifacts = true
    }
  }
}
```

In this example, `build_job` downloads the artifacts from the latest successful `build-1` and `build-2` jobs on the `main` branches in the `group/project-name` and `group/project-name-2` projects.

In GitLab 13.3 and later, you can use [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html#gitlab-ciyml-file) in `needs:project`, for example:

```kotlin
val build by stage()

job("build_job") {
  stage = build
  script {
    +"ls -lhR"
  }
  needs {
    +need {
      project = "$CI_PROJECT_PATH"
      job = "$DEPENDENCY_JOB_NAME"
      ref = "$ARTIFACTS_DOWNLOAD_REF"
      artifacts = true
    }
  }
```

**Additional details**:

- To download artifacts from a different pipeline in the current project, set `project` to be the same as the current project, but use a different ref than the current pipeline. Concurrent pipelines running on the same ref could override the artifacts.
- The user running the pipeline must have at least the Reporter role for the group or project, or the group/project must have public visibility.
- You can’t use `needs:project` in the same job as [`trigger`](../trigger).
- When using `needs:project` to download artifacts from another pipeline, the job does not wait for the needed job to complete. [Directed acyclic graph](https://docs.gitlab.com/ee/ci/directed_acyclic_graph/index.html) behavior is limited to jobs in the same pipeline. Make sure that the needed job in the other pipeline completes before the job that needs it tries to download the artifacts.
- You can’t download artifacts from jobs that run in [`parallel`](../parallel).
- Support for [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/index.html) in `project`, `job`, and `ref` was [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/202093) in GitLab 13.3. [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/235761) in GitLab 13.4.

**Related topics**:

- To download artifacts between [parent-child pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#parent-child-pipelines), use [`needs:pipeline:job`](#needspipelinejob).

## `needs:pipeline:job`

A [child pipeline](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#parent-child-pipelines) can download artifacts from a job in its parent pipeline or another child pipeline in the same parent-child pipeline hierarchy.

**Keyword type**: Job keyword. You can use it only as part of a job.

**Possible inputs**:

- `needs:pipeline`: A pipeline ID. Must be a pipeline present in the same parent-child pipeline hierarchy.
- `job`: The job to download artifacts from.

**Example of `needs:pipeline:job`**:

- Parent pipeline (`.gitlab-ci.yml`):
```kotlin
pipeline {
  val build by stage()

  job("create-artifact") {
    stage = build
    script {
      +shell("echo \"sample artifact\" > artifact.txt")
    }
    artifacts {
      paths {
        +"artifact.txt"
      }
    }
  }

  job("child-pipeline") {
    stage = test
    trigger {
      include = "child.yml"
      strategy = Trigger.Strategy.DEPEND
    }
    variables {
      "PARENT_PIPELINE_ID" to "$CI_PIPELINE_ID"
    }
  }
}
```
- Child pipeline (`child.yml`):
```kotlin
pipeline {
  job("use-artifact") {
    script {
      +shell("cat artifact.txt")
    }
    needs {
      +need {
        pipeline = "$PARENT_PIPELINE_ID"
        job = "create-artifact"
      }
    }
  }
}
```

In this example, the `create-artifact` job in the parent pipeline creates some artifacts. The `child-pipeline` job triggers a child pipeline, and passes the `CI_PIPELINE_ID` variable to the child pipeline as a new `PARENT_PIPELINE_ID` variable. The child pipeline can use that variable in `needs:pipeline` to download artifacts from the parent pipeline.

**Additional details**:

- The `pipeline` attribute does not accept the current pipeline ID (`$CI_PIPELINE_ID`). To download artifacts from a job in the current pipeline, use [`needs`](#needsartifacts).

## `needs:optional`

To need a job that sometimes does not exist in the pipeline, add `optional: true` to the `needs` configuration. If not defined, `optional: false` is the default.

Jobs that use [`rules`](../rules), [`only`](../only), or [`except`](../except) and that are added with [`include`](../include) might not always be added to a pipeline. GitLab checks the `needs` relationships before starting a pipeline:

- If the `needs` entry has `optional: true` and the needed job is present in the pipeline, the job waits for it to complete before starting.
- If the needed job is not present, the job can start when all other needs requirements are met.
- If the `needs` section contains only optional jobs, and none are added to the pipeline, the job starts immediately (the same as an empty `needs` entry: `needs: []`).
- If a needed job has `optional: false`, but it was not added to the pipeline, the pipeline fails to start with an error similar to: ``'job1' job needs 'job2' job, but it was not added to the pipeline`.

**Keyword type**: Job keyword. You can use it only as part of a job.

**Example of `needs:optional`**:

```kotlin
pipeline {
  val build by stage()
  val test by stage()
  val deploy by stage()

  job("build-job") {
    stage = build
  }

  job("test-job1") {
    stage = test
  }

  job("test-job2") {
    stage = test
    rules {
      +`if`("\$CI_COMMIT_BRANCH == \$CI_DEFAULT_BRANCH")
    }
  }

  job("deploy-job") {
    stage = deploy
    needs {
      +need {
        job = "test-job2"
        optional = true
      }
      +need {
        job = "test-job1"
      }
    }
    environment = "production"
  }

  job("review-job") {
    stage = deploy
    needs {
      +need {
        job = "test-job2"
        optional = true
      }
    }
    environment = "review"
  }
}
```

In this example:

- `build-job`, `test-job1`, and `test-job2` start in stage order.
- When the branch is the default branch, `test-job2` is added to the pipeline, so:
    - `deploy-job` waits for both `test-job1` and `test-job2` to complete.
    - `review-job` waits for `test-job2` to complete.
- When the branch is not the default branch, `test-job2` is not added to the pipeline, so:
    - `deploy-job` waits for only `test-job1` to complete, and does not wait for the missing `test-job2`.
    - `review-job` has no other needed jobs and starts immediately (at the same time as `build-job`), like `needs: []`.

## `needs:pipeline`

You can mirror the pipeline status from an upstream pipeline to a job by using the `needs:pipeline` keyword. The latest pipeline status from the default branch is replicated to the job.

**Keyword type**: Job keyword. You can use it only as part of a job.

**Possible inputs**:

- A full project path, including namespace and group. If the project is in the same group or namespace, you can omit them from the `project` keyword. For example: `project: group/project-name` or `project: project-name`.

**Example of needs:pipeline**:

```kotlin
pipeline {
  val test by stage()

  job("upstream_status") {
    stage = test
    needs {
      +need {
        pipeline = "other/project"
      }
    }
  }
}
```

**Additional details**:

If you add the `job` keyword to `needs:pipeline`, the job no longer mirrors the pipeline status. The behavior changes to [`needs:pipeline:job`](#needspipelinejob).

## `needs:parallel:matrix`

Introduced in GitLab 16.3.

Jobs can use [`parallel:matrix`](../parallel#parallelmatrix) to run a job multiple times in parallel in a single pipeline, but with different variable values for each instance of the job.

Use `needs:parallel:matrix` to execute jobs out-of-order depending on parallelized jobs.

**Keyword type**: Job keyword. You can use it only as part of a job. Must be used with needs:job.

**Possible inputs**: An array of hashes of variables:

- The variables and values must be selected from the variables and values defined in the `parallel:matrix` job.

**Example of `needs:parallel:matrix`**:

```kotlin
pipeline {
  val build by stage()
  val test by stage()

  job("linux:build") {
    stage = build
    script {
      +shell("echo \"Building linux...\"")
    }
    parallel {
      matrix {
        +PROVIDER: aws
        STACK:
          - monitoring
          - app1
          - app2
      }
    }
  }

  job("linux:rspec") {
    stage = test
    needs {
      +need {
        job = "linux:build"
        parallel {
          matrix {
            +mapOf(
                "PROVIDER" to listOf("aws"),
                "STACK" to listOf("app1")
            )
          }
        }
      }
    }
    script {
      +shell("echo \"Running rspec on linux...\"")
    }
  }
}
```

The above example generates the following jobs:

```
linux:build: [aws, monitoring]
linux:build: [aws, app1]
linux:build: [aws, app2]
linux:rspec
```

The `linux:rspec` job runs as soon as the `linux:build: [aws, app1]` job finishes.

**Related topics**:

- [Specify a parallelized job using needs with multiple parallelized jobs](https://docs.gitlab.com/ee/ci/jobs/job_control.html#specify-a-parallelized-job-using-needs-with-multiple-parallelized-jobs).
