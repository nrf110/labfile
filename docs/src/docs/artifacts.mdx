---
title: artifacts
---

Use `artifacts` to specify a list of files and directories that are attached to the job when it [succeeds, fails, or always](#when).

The artifacts are sent to GitLab after the job finishes. They are available for download in the GitLab UI if the size is not larger than the [maximum artifact size](https://docs.gitlab.com/ee/user/gitlab_com/index.html#gitlab-cicd).

By default, jobs in later stages automatically download all the artifacts created by jobs in earlier stages. You can control artifact download behavior in jobs with [dependencies](#dependencies).

When using the [`needs`](../needs#artifact-downloads-with-needs) keyword, jobs can only download artifacts from the jobs defined in the `needs` configuration.

Job artifacts are only collected for successful jobs by default, and artifacts are restored after [caches](../cache).

[Read more about artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).

## dependencies
By default, all `artifacts` from previous stages are passed to each job. However, you can use the `dependencies` keyword to define a limited list of jobs to fetch artifacts from. You can also set a job to download no artifacts at all.

To use this feature, define `dependencies` in context of the job and pass a list of all previous jobs the artifacts should be downloaded from.

You can define jobs from stages that were executed before the current one. An error occurs if you define jobs from the current or an upcoming stage.

To prevent a job from downloading artifacts, define an empty array.

When you use `dependencies`, the status of the previous job is not considered. If a job fails or it’s a manual job that isn’t triggered, no error occurs.

The following example defines two jobs with artifacts: `build:osx` and `build:linux`. When the `test:osx` is executed, the artifacts from `build:osx` are downloaded and extracted in the context of the build. The same happens for `test:linux` and artifacts from `build:linux`.

The job `deploy` downloads artifacts from all previous jobs because of the [stage](../stage) precedence:

```kotlin
val build by stage()
val test by stage()
val deploy by stage()

job("build:osx") {
    stage = build
    script {
        +shell("make build:osx")
    }

    artifacts {
        paths {
            +"binaries/"
        }
    }
}

job("build:linux") {
    stage = build
    script  {
        +shell("make build:linux")
    }
    artifacts {
        paths {
            +"binaries/"
        }
    }
}

job("test:osx") {
    stage = test
    script {
        +shell("make test:osx")
    }
    dependencies {
        +"build:osx"
    }
}

job("test:linux") {
    stage = test
    script {
        +shell(make test:linux")
    }
    dependencies {
        +"build:linux"
    }
}

job("deploy") {
    stage = deploy
    script {
        +shell("make deploy")
    }
}
```

### When a dependent job fails
If the artifacts of the job that is set as a dependency are [expired](#artifactsexpireIn) or [deleted](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#delete-job-artifacts), then the dependent job fails.

## `artifacts:exclude`
`exclude` makes it possible to prevent files from being added to an artifacts archive.

Similar to [`artifacts:paths`](#artifactspaths), `exclude` paths are relative to the project directory. You can use Wildcards that use [glob](https://en.wikipedia.org/wiki/Glob_(programming)) or [doublestar.PathMatch](https://pkg.go.dev/github.com/bmatcuk/doublestar@v1.2.2?tab=doc#PathMatch) patterns.

For example, to store all files in `binaries/`, but not `*.o` files located in subdirectories of `binaries/`:

```kotlin
artifacts {
    paths {
        +"binaries/"
    }
    exclude {
        +"binaries/**/*.o"
    }
}
```

Unlike `artifacts:paths`, `exclude` paths are not recursive. To exclude all of the contents of a directory, you can match them explicitly rather than matching the directory itself.

For example, to store all files in `binaries/` but nothing located in the `temp/` subdirectory:

```kotlin
artifacts {
    paths {
        +"binaries/"
    }
    exclude {
        +"binaries/temp/**/*"
    }
}
```

Files matched by [`artifacts:untracked`](#artifactsuntracked) can be excluded using `artifacts:exclude` too.

## `artifacts:expireIn`
Use `expireIn` to specify how long job artifacts are stored before they expire and are deleted. The `expireIn` setting does not affect:

* Artifacts from the latest job, unless this keeping the latest job artifacts is:
    * [Disabled at the project level](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#keep-artifacts-from-most-recent-successful-jobs).
    * [Disabled instance-wide](https://docs.gitlab.com/ee/user/admin_area/settings/continuous_integration.html#keep-the-latest-artifacts-for-all-jobs-in-the-latest-successful-pipelines).
* [Pipeline artifacts](https://docs.gitlab.com/ee/ci/pipelines/pipeline_artifacts.html). It’s not possible to specify an expiration date for these:
    * Pipeline artifacts from the latest pipeline are kept forever.
    * Other pipeline artifacts are erased after one week.

The value of `expireIn` is an elapsed time in seconds, unless a unit is provided. Valid values include:
* `42`
* `42 seconds`
* `3 mins 4 sec`
* `2 hrs 20 min`
* `2h20min`
* `6 mos 1 day`
* `47 yrs 6 mos and 4d`
* `3 weeks and 2 days`
* `never`

To expire artifacts one week after being uploaded:

```kotlin
job("job") {
    artifacts {
        expireIn = "1 week"
    }
}
```

The expiration time period begins when the artifact is uploaded and stored on GitLab. If the expiry time is not defined, it defaults to the [instance wide setting](https://docs.gitlab.com/ee/user/admin_area/settings/continuous_integration.html#default-artifacts-expiration) (30 days by default).

To override the expiration date and protect artifacts from being automatically deleted:
* Use the **Keep** button on the job page.
* [In GitLab 13.3 and later](https://gitlab.com/gitlab-org/gitlab/-/issues/22761), set the value of `expireIn` to `never`.

After their expiry, artifacts are deleted hourly by default (using a cron job), and are not accessible anymore.

## `artifacts:exposeAs`
Use the `exposeAs` keyword to expose [job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) in the [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/index.html) UI.

For example, to match a single file:

```kotlin
job("test") {
    script {
        +shell("echo 'test' > file.txt")
    }
    artifacts {
        exposeAs = "artifact 1"
        paths {
            +"file.txt"
        }
    }
}
```

With this configuration, GitLab adds a link **artifact 1** to the relevant merge request that points to `file1.txt`. To access the link, select **View exposed artifact** below the pipeline graph in the merge request overview.

An example that matches an entire directory:

```kotlin
job("test") {
    script {
        +shell("mkdir test && echo 'test' > test/file.txt")
    }
    artifacts {
        exposeAs = "artifact 1"
        paths {
            +"test/"
        }
    }
}
```

Note the following:
* Artifacts do not display in the merge request UI when using variables to define the `artifacts:paths`.
* A maximum of 10 job artifacts per merge request can be exposed.
* Glob patterns are unsupported.
* If a directory is specified, the link is to the job [artifacts browser](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#download-job-artifacts) if there is more than one file in the directory.
* For exposed single file artifacts with `.html`, `.htm`, `.txt`, `.json`, `.xml`, and `.log` extensions, if [GitLab Pages](https://docs.gitlab.com/ee/administration/pages/index.html) is:
    * Enabled, GitLab automatically renders the artifact.
    * Not enabled, the file is displayed in the artifacts browser.

## `artifacts:name`
Use the `name` directive to define the name of the created artifacts archive. You can specify a unique name for every archive. The `artifacts:name` variable can make use of any of the [predefined variables](https://docs.gitlab.com/ee/ci/variables/index.html). The default name is `artifacts`, which becomes `artifacts.zip` when you download it.

To create an archive with a name of the current job:

```kotlin
job("job") {
    artifacts {
        name = "\$CI_JOB_NAME"

        paths {
            +"binaries/"
        }
    }
}
```

To create an archive with a name of the current branch or tag including only the binaries directory:

```kotlin
job("job") {
    artifacts {
        name = "\$CI_COMMIT_REF_NAME"

        paths {
            +"binaries/"
        }
    }
}
```

If your branch-name contains forward slashes (for example `feature/my-feature`) it’s advised to use `$CI_COMMIT_REF_SLUG` instead of `$CI_COMMIT_REF_NAME` for proper naming of the artifact.

To create an archive with a name of the current job and the current branch or tag including only the binaries directory:

```kotlin
job("job") {
    artifacts {
        name = "\$CI_JOB_NAME-\$CI_COMMIT_REF_NAME"

        paths {
            +"binaries/"
        }
    }
}
```

To create an archive with a name of the current [stage](../stage) and branch name:

```kotlin
job("job") {
    artifacts {
        name = "\$CI_JOB_STAGE-\$CI_COMMIT_REF_NAME"

        paths {
            +"binaries/"
        }
    }
}
```

If you use **Windows Batch** to run your shell scripts you need to replace `$` with `%`:

```kotlin
job("job") {
    artifacts {
        name = "%CI_JOB_STAGE%-%CI_COMMIT_REF_NAME%"

        paths {
            +"binaries/"
        }
    }
}
```

If you use **Windows PowerShell** to run your shell scripts you need to replace `\$` with `\$env:`:

```kotlin
job("job") {
    artifacts {
        name = "\$env:CI_JOB_STAGE-\$env:CI_COMMIT_REF_NAME"

        paths {
            +"binaries/"
        }
    }
}
```

## `artifacts:paths`
Paths are relative to the project directory ($CI_PROJECT_DIR) and can’t directly link outside it. You can use Wildcards that use [glob](https://en.wikipedia.org/wiki/Glob_(programming)) patterns and:
* In [GitLab Runner 13.0](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/2620) and later, [doublestar.Glob](https://pkg.go.dev/github.com/bmatcuk/doublestar@v1.2.2?tab=doc#Match).
* In GitLab Runner 12.10 and earlier, [filepath.Match](https://pkg.go.dev/path/filepath#Match).

To restrict which jobs a specific job fetches artifacts from, see [dependencies](#dependencies).

Send all files in `binaries` and `.config`:

```kotlin
artifacts {
    paths {
        +"binaries/"
        +".config"
    }
}
```

To disable artifact passing, define the job with empty [dependencies](#dependencies):

```kotlin
val build by stage()

job("job") {
    stage = build

    script {
        +shell("make build")
    }

    dependencies {}
}
```

You may want to create artifacts only for tagged releases to avoid filling the build server storage with temporary build artifacts.

Create artifacts only for tags (`default-job` doesn’t create artifacts):

```kotlin
job("default-job") {
    script {
        +shell("mvn test -U")
    }
    rules {
        +rule {
            `if`("\$CI_COMMIT_BRANCH")
        }
    }
}

job("release-job") {
    script {
        +shell("mvn package -U")
    }
    artifacts {
        paths {
            +"target/*.war"
        }
    }
    rules {
        +rule {
            `if`("\$CI_COMMIT_TAG")
        }
    }
}
```

You can use wildcards for directories too. For example, if you want to get all the files inside the directories that end with `xyz`:

```kotlin
job("job") {
    artifacts {
        paths {
            +"path/*xyz/*"
        }
    }
}
```

## `artifacts:public`
Use `artifacts:public` to determine whether the job artifacts should be publicly available.

The default for `artifacts:public` is `true` which means that the artifacts in public pipelines are available for download by anonymous and guest users:

```kotlin
artifacts {
    `public` = true
}
```

To deny read access for anonymous and guest users to artifacts in public pipelines, set `artifacts:public` to `false`:

```kotlin
artifacts {
    `public` = false
}
```

## `artifacts:reports`
Use `artifacts:reports` to collect test reports, code quality reports, and security reports from jobs. It also exposes these reports in the GitLab UI (merge requests, pipeline views, and security dashboards).

The test reports are collected regardless of the job results (success or failure). You can use [`artifacts:expireIn`](#artifactsexpirein) to set up an expiration date for their artifacts.

If you also want the ability to browse the report output files, include the [`artifacts:paths`](#artifactspath) keyword.

### `artifacts:reports:apiFuzzing`
The `apiFuzzing` report collects [API Fuzzing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/index.html) bugs as artifacts.

The collected API Fuzzing report uploads to GitLab as an artifact and is summarized in merge requests and the pipeline view. It’s also used to provide data for security dashboards.

### `artifacts:reports:cobertura`
The `cobertura` report collects [Cobertura coverage XML files](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html). The collected Cobertura coverage reports upload to GitLab as an artifact and display in merge requests.

Cobertura was originally developed for Java, but there are many third party ports for other languages like JavaScript, Python, Ruby, and so on.

### `artifacts:reports:codequality`
The `codequality` report collects [Code Quality issues](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) as artifacts.

The collected Code Quality report uploads to GitLab as an artifact and is summarized in merge requests.

### `artifacts:reports:containerScanning`
The `containerScanning` report collects [Container Scanning vulnerabilities](https://docs.gitlab.com/ee/user/application_security/container_scanning/index.html) as artifacts.

The collected Container Scanning report uploads to GitLab as an artifact and is summarized in merge requests and the pipeline view. It’s also used to provide data for security dashboards.

### `artifacts:reports:coverageFuzzing`
The `coverageFuzzing` report collects [coverage fuzzing bugs](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/index.html) as artifacts.

The collected coverage fuzzing report uploads to GitLab as an artifact and is summarized in merge requests and the pipeline view. It’s also used to provide data for security dashboards.

### `artifacts:reports:clusterImageScanning`
The `clusterImageScanning` report collects `CLUSTER_IMAGE_SCANNING` vulnerabilities as artifacts.

The collected `CLUSTER_IMAGE_SCANNING` report uploads to GitLab as an artifact and is summarized in the pipeline view. It’s also used to provide data for security dashboards.

### `artifacts:reports:dast`
The `dast` report collects [DAST vulnerabilities](https://docs.gitlab.com/ee/user/application_security/dast/index.html) as artifacts.

The collected DAST report uploads to GitLab as an artifact and is summarized in merge requests and the pipeline view. It’s also used to provide data for security dashboards.

### `artifacts:reports:dependencyScanning`
The `dependencyScanning` report collects [Dependency Scanning vulnerabilities](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html) as artifacts.

The collected Dependency Scanning report uploads to GitLab as an artifact and is summarized in merge requests and the pipeline view. It’s also used to provide data for security dashboards.

### `artifacts:reports:dotenv`
The `dotenv` report collects a set of environment variables as artifacts.

The collected variables are registered as runtime-created variables of the job, which is useful to [set dynamic environment URLs after a job finishes](https://docs.gitlab.com/ee/ci/environments/index.html#set-dynamic-environment-urls-after-a-job-finishes).

There are a couple of exceptions to the [original dotenv rules](https://github.com/motdotla/dotenv#rules):
* The variable key can contain only letters, digits, and underscores (`_`).
* The maximum size of the `.env` file is 5 KB.
* In GitLab 13.5 and older, the maximum number of inherited variables is 10.
* In [GitLab 13.6 and later](https://gitlab.com/gitlab-org/gitlab/-/issues/247913), the maximum number of inherited variables is 20.
* Variable substitution in the `.env` file is not supported.
* The `.env` file can’t have empty lines or comments (starting with `#`).
* Key values in the `env` file cannot have spaces or newline characters (`\n`), including when using single or double quotes.
* Quote escaping during parsing (`key = 'value'` -> `{key: "value"}`) is not supported.

### `artifacts:reports:junit`
The `junit` report collects [JUnit report format XML files](https://www.ibm.com/docs/en/adfz/developer-for-zos/14.1.0?topic=formats-junit-xml-format) as artifacts. Although JUnit was originally developed in Java, there are many third party ports for other languages like JavaScript, Python, Ruby, and so on.

See [Unit test reports](https://docs.gitlab.com/ee/ci/unit_test_reports.html) for more details and examples. Below is an example of collecting a JUnit report format XML file from Ruby’s RSpec test tool:

```kotlin
val test by stage()

job("rspec") {
    stage = test
    script {
        +shell("bundle install")
        +shell("rspec --format RspecJunitFormatter --out rspec.xml")
    }
    artifacts {
        reports {
            junit {
                +"rspec.xml"
            }
        }
    }
}
```

The collected Unit test reports upload to GitLab as an artifact and display in merge requests.

If the JUnit tool you use exports to multiple XML files, specify multiple test report paths within a single job to concatenate them into a single file. Use a filename pattern (`junit: rspec-*.xml`), an array of filenames (`junit: [rspec-1.xml, rspec-2.xml, rspec-3.xml]`), or a combination thereof (`junit: [rspec.xml, test-results/TEST-*.xml]`).

### `artifacts:reports:licenseScanning`
The `licenseScanning` report collects [Licenses](https://docs.gitlab.com/ee/user/compliance/license_compliance/index.html) as artifacts.

The License Compliance report uploads to GitLab as an artifact and displays automatically in merge requests and the pipeline view, and provide data for security dashboards.

### `artifacts:reports:loadPerformance`
The `loadPerformance` report collects [Load Performance Testing metrics](https://docs.gitlab.com/ee/user/project/merge_requests/load_performance_testing.html) as artifacts.

The report is uploaded to GitLab as an artifact and is shown in merge requests automatically.

### `artifacts:reports:metrics`
The `metrics` report collects [Metrics](https://docs.gitlab.com/ee/ci/metrics_reports.html) as artifacts.

The collected Metrics report uploads to GitLab as an artifact and displays in merge requests.

### `artifacts:reports:browserPerformance`
The `browserPerformance` report collects [Browser Performance Testing metrics](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html) as artifacts.

The collected Browser Performance report uploads to GitLab as an artifact and displays in merge requests.

### `artifacts:reports:requirements`
The `requirements` report collects `requirements.json` files as artifacts.

The collected Requirements report uploads to GitLab as an artifact and existing [requirements](https://docs.gitlab.com/ee/user/project/requirements/index.html) are marked as Satisfied.

### `artifacts:reports:sast`
The `sast` report collects [SAST vulnerabilities](https://docs.gitlab.com/ee/user/application_security/sast/index.html) as artifacts.

The collected SAST report uploads to GitLab as an artifact and is summarized in merge requests and the pipeline view. It’s also used to provide data for security dashboards.

### `artifacts:reports:secretDetection`
The `secretDetection` report collects [detected secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/index.html) as artifacts.

The collected Secret Detection report is uploaded to GitLab as an artifact and summarized in the merge requests and pipeline view. It’s also used to provide data for security dashboards.

### `artifacts:reports:terraform`
The `terraform` report obtains a Terraform `tfplan.json` file. [JQ processing required to remove credentials](https://docs.gitlab.com/ee/user/infrastructure/mr_integration.html#configure-terraform-report-artifacts). The collected Terraform plan report uploads to GitLab as an artifact and displays in merge requests. For more information, see [Output `terraform plan` information into a merge request](https://docs.gitlab.com/ee/user/infrastructure/mr_integration.html).

## `artifacts:untracked`
Use `artifacts:untracked` to add all Git untracked files as artifacts (along with the paths defined in `artifacts:paths`). `artifacts:untracked` ignores configuration in the repository’s `.gitignore` file.

Send all Git untracked files:

```kotlin
artifacts {
    untracked = true
}
```

Send all Git untracked files and files in `binaries`:

```kotlin
artifacts {
    untracked = true
    paths {
        +"binaries/"
    }
}
```

Send all untracked files but [exclude](#artifactsexclude) `*.txt`:

```kotlin
artifacts {
    untracked = true
    exclude {
        +"*.txt"
    }
}
```

## `artifacts:when`
Use `artifacts:when` to upload artifacts on job failure or despite the failure.

`artifacts:when` can be set to one of the following values:
1. `When.ON_SUCCESS` (default): Upload artifacts only when the job succeeds.
2. `When.ON_FAILURE`: Upload artifacts only when the job fails.
3. `When.ALWAYS`: Always upload artifacts. Useful, for example, when [uploading artifacts](https://docs.gitlab.com/ee/ci/unit_test_reports.html#viewing-junit-screenshots-on-gitlab) required to troubleshoot failing tests.

For example, to upload artifacts only when a job fails:

```kotlin
job("job") {
    artifacts {
        `when` = When.ON_FAILURE
    }
}
```