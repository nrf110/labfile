---
title: variables
---

Introduced in GitLab Runner v0.5.0.

[CI/CD variables](https://docs.gitlab.com/ee/ci/variables/README.html) are configurable values that are passed to jobs. They can be set globally and per-job.

There are two types of variables:
* [Custom variables](https://docs.gitlab.com/ee/ci/variables/README.html#custom-cicd-variables): You can define their values in the `.gitlab-ci.yml` file, in the GitLab UI, or by using the API. You can also input variables in the GitLab UI when [running a pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/index.html#run-a-pipeline-manually).
* [Predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html): These values are set by the runner itself. One example is `CI_COMMIT_REF_NAME`, which is the branch or tag the project is built for.
After you define a variable, you can use it in all executed commands and scripts.

Variables are meant for non-sensitive project configuration, for example:

```kotlin
val deploy by stage()

variables {
    "DEPLOY_SITE" to "https://example.com/"
}

job("deploy_job") {
    stage = deploy
    script {
        +shell("deploy-script --url \$DEPLOY_SITE --path \"/\"")
    }
}

job("deploy_review_job") {
    stage = deploy
    variables {
        "REVIEW_PATH" to "/review"
    }
    script {
        +shell("deploy-review-script --url \$DEPLOY_SITE --path \$REVIEW_PATH")
    }
}
```

You can use only integers, booleans, and strings for the variable’s name and value.

If you define a variable at the top level of the `gitlab-ci.yml` file, it is global, meaning it applies to all jobs. If you define a variable in a job, it’s available to that job only.

If a variable of the same name is defined globally and for a specific job, the [job-specific variable overrides the global variable](https://docs.gitlab.com/ee/ci/variables/README.html#cicd-variable-precedence).

All YAML-defined variables are also set to any linked [Docker service containers](https://docs.gitlab.com/ee/ci/services/index.html).

## Prefill variables in manual pipelines
[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/30101) in GitLab 13.7.

Use the `value` and `description` keywords to define [pipeline-level (global) variables that are prefilled](https://docs.gitlab.com/ee/ci/pipelines/index.html#prefill-variables-in-manual-pipelines) when [running a pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/index.html#run-a-pipeline-manually):

```kotlin
variables {
    "DEPLOY_ENVIRONMENT" to variable {
        value = "staging"  // Deploy to staging by default
        description = "The deployment target. Change this variable to 'canary' or 'production' if needed."
    }
}
```

You cannot set job-level variables to be pre-filled when you run a pipeline manually.

## Configure runner behavior with variables
You can use [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/README.html) to configure how the runner processes Git requests:

* [`GIT_STRATEGY`](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#git-strategy)
* [`GIT_SUBMODULE_STRATEGY`](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#git-submodule-strategy)
* [`GIT_CHECKOUT`](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#git-checkout)
* [`GIT_CLEAN_FLAGS`](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#git-clean-flags)
* [`GIT_FETCH_EXTRA_FLAGS`](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#git-fetch-extra-flags)
* [`GIT_DEPTH`](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#shallow-cloning) (shallow cloning)
* [`GIT_CLONE_PATH`](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#custom-build-directories) (custom build directories)
* [`TRANSFER_METER_FREQUENCY`](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#artifact-and-cache-settings) (artifact/cache meter update frequency)
* [`ARTIFACT_COMPRESSION_LEVEL`](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#artifact-and-cache-settings) (artifact archiver compression level)
* [`CACHE_COMPRESSION_LEVEL`](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#artifact-and-cache-settings) (cache archiver compression level)

You can also use variables to configure how many times a runner [attempts certain stages of job execution](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#job-stages-attempts).

