---
title: release
---

##
Use `release` to create a [release](https://docs.gitlab.com/ee/user/project/releases/index.html). Requires the [release-cli](https://gitlab.com/gitlab-org/release-cli/-/tree/master/docs) to be available in your GitLab Runner Docker or shell executor.

These keywords are supported:

* [`tagName`](#releasetagname)
* [`tagMessage`](#releasetagmessage)
* [`description`](#releasedescription)
* [`name`](#releasename) (optional)
* [`ref`](#releaseref) (optional)
* [`milestones`](#releasemilestone) (optional)
* [`releasedAt`](#releasereleasedat) (optional)
* [`assets:links`](#releaseassetslinks) (optional)

The release is created only if the job processes without error. If the Rails API returns an error during release creation, the `release` job fails.

## `release-cli` Docker image
You must specify the Docker image to use for the `release-cli`:

```kotlin
image("registry.gitlab.com/gitlab-org/release-cli:latest")
```

## `release-cli` for shell executors
For GitLab Runner shell executors, you can download and install the `release-cli` manually for your [supported OS and architecture](https://release-cli-downloads.s3.amazonaws.com/latest/index.html). Once installed, the `release` keyword should be available to you.

### Install on Unix/Linux

1. Download the binary for your system, in the following example for amd64 systems:
```bash
curl --location --output /usr/local/bin/release-cli "https://release-cli-downloads.s3.amazonaws.com/latest/release-cli-linux-amd64"
```

2. Give it permissions to execute:
```bash
sudo chmod +x /usr/local/bin/release-cli
```

3. Verify release-cli is available:
```bash
$ release-cli -v

release-cli version 0.6.0
```

### Install on Windows PowerShell

1. Create a folder somewhere in your system, for example `C:\GitLab\Release-CLI\bin`
```
New-Item -Path 'C:\GitLab\Release-CLI\bin' -ItemType Directory
```

2. Download the executable file:
```
PS C:\> Invoke-WebRequest -Uri "https://release-cli-downloads.s3.amazonaws.com/latest/release-cli-windows-amd64.exe" -OutFile "C:\GitLab\Release-CLI\bin\release-cli.exe"

      Directory: C:\GitLab\Release-CLI
Mode                LastWriteTime         Length Name
  ----                -------------         ------ ----
d-----        3/16/2021   4:17 AM                bin
```

3. Add the directory to your `$env:PATH`:
```
$env:PATH += ";C:\GitLab\Release-CLI\bin"
```

4. Verify release-cli is available:
```
PS C:\> release-cli -v

release-cli version 0.6.0
```

### Use a custom SSL CA certificate authority
You can use the `ADDITIONAL_CA_CERT_BUNDLE` CI/CD variable to configure a custom SSL CA certificate authority, which is used to verify the peer when the `release-cli` creates a release through the API using HTTPS with custom certificates. The `ADDITIONAL_CA_CERT_BUNDLE` value should contain the [text representation of the X.509 PEM public-key certificate](https://tools.ietf.org/html/rfc7468#section-5.1) or the `path/to/file` containing the certificate authority. For example, to configure this value in the `.gitlab-ci.yml` file, use the following:

```kotlin
job("release") {
    variables {
        "ADDITIONAL_CA_CERT_BUNDLE" to """
-----BEGIN CERTIFICATE-----
MIIGqTCCBJGgAwIBAgIQI7AVxxVwg2kch4d56XNdDjANBgkqhkiG9w0BAQsFADCB
...
jWgmPqF3vUbZE0EyScetPJquRFRKIesyJuBFMAs=
-----END CERTIFICATE-----
"""
    }
    script {
        +shell("echo \"Create release\"")
    }
    release {
        name = "My awesome release"
        tagName = "\$CI_COMMIT_TAG"
    }
}
```

The `ADDITIONAL_CA_CERT_BUNDLE` value can also be configured as a [custom variable in the UI](https://docs.gitlab.com/ee/ci/variables/README.html#custom-cicd-variables), either as a `file`, which requires the path to the certificate, or as a variable, which requires the text representation of the certificate.

## `script`
All jobs except [trigger](../trigger) jobs must have the `script` keyword. A `release` job can use the output from script commands, but you can use a placeholder script if the script is not needed:

```kotlin
script {
  +shell("echo 'release job'")
}
```

An [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/223856) exists to remove this requirement in an upcoming version of GitLab.

A pipeline can have multiple `release` jobs, for example:

```kotlin
job("ios-release") {
    script {
        +shell("echo 'iOS release job'")
    }
    release {
        tagName = "v1.0.0-ios"
        description = "iOS release v1.0.0"
    }
}

job("android-release") {
    script {
        +shell("echo 'Android release job'")
    }
    release {
        tagName = "v1.0.0-android"
        description = "Android release v1.0.0"
    }
}
```

## `release:tagName`
You must specify a `tagName` for the release. The tag can refer to an existing Git tag or you can specify a new tag.

When the specified tag doesn’t exist in the repository, a new tag is created from the associated SHA of the pipeline.

For example, when creating a release from a Git tag:

```kotlin
job("job") {
    release {
        tagName = "\$CI_COMMIT_TAG"
        description = "Release description"
    }
}
```

It is also possible to create any unique tag, in which case `only: tags` is not mandatory. A semantic versioning example:

```kotlin
job("job") {
    release {
        tagName = "\${MAJOR}_\${MINOR}_\${REVISION}"
        description = "Release description"
    }
}
```

* The release is created only if the job’s main script succeeds.
* If the release already exists, it is not updated and the job with the `release` keyword fails.
* The `release` section executes after the `script` tag and before the `afterScript`.

## `release:tagMessage`
If the tag does not exist, the newly created tag is annotated with the message specified by `tag_message`. If omitted, a lightweight tag is created.

**Keyword type**: Job keyword. You can use it only as part of a job.

**Possible inputs**:

- A text string.

**Example of `release:tag_message`**:

```kotlin
pipeline {
  val release by stage()

  job("release_job") {
    stage = release
    release {
      tagName = "\$CI_COMMIT_TAG"
      description = "Release description"
      tagMessage = "Annotated tag message"
    }
}
```

## `release:name`
The release name. If omitted, it is populated with the value of `release: tagName`.

## `release:description`
Specifies the long description of the release. You can also specify a file that contains the description.

### Read description from a file
You can specify a file in `$CI_PROJECT_DIR` that contains the description. The file must be relative to the project directory (`$CI_PROJECT_DIR`), and if the file is a symbolic link it can’t reside outside of `$CI_PROJECT_DIR`. The `./path/to/file` and filename can’t contain spaces.

```kotlin
job("job") {
    release {
        tagName = "\${MAJOR}_\${MINOR}_\${REVISION}"
        description = "./path/to/CHANGELOG.md"
    }
}
```

## `release:ref`
If the `release: tagName` doesn’t exist yet, the release is created from `ref`. `ref` can be a commit SHA, another tag name, or a branch name.

## `release:milestones`
The title of each milestone the release is associated with.

## `release:releasedAt`
The date and time when the release is ready. Defaults to the current date and time if not defined. Should be enclosed in quotes and expressed in ISO 8601 format.

```kotlin
releasedAt = "2021-03-15T08:00:00Z"
```

## `release:assets:links`
Include [asset links](https://docs.gitlab.com/ee/user/project/releases/index.html#release-assets) in the release.

Requires release-cli version v0.4.0 or higher.

```kotlin
assets {
    links {
        +link {
            name = "asset1"
            url = "https://example.com/assets/1"
        }
        +link {
            name = "asset2"
            url = "https://example.com/assets/2"
            filepath = "/pretty/url/1" // optional
            linkType = "other" // optional
        }
    }
}
```

## Complete example for `release`
If you combine the previous examples for `release`, you get two options, depending on how you generate the tags. You can’t use these options together, so choose one:

* To create a release when you push a Git tag, or when you add a Git tag in the UI by going to **Repository > Tags**:

```kotlin
val release by stage()

job("release_job") {
    stage = release
    image("registry.gitlab.com/gitlab-org/release-cli:latest")
    rules {
        +rule {
            `if`("\"$CI_COMMIT_TAG") // Run this job when a tag is created manually
        }
    }
    script {
        +shell("echo 'running release_job'")
    }
    release {
        name = "Release \$CI_COMMIT_TAG"
        description = "Created using the release-cli \$EXTRA_DESCRIPTION"  // $EXTRA_DESCRIPTION must be defined
        tagName = "\$CI_COMMIT_TAG" // elsewhere in the pipeline.
        ref = "\$CI_COMMIT_TAG"
        milestones {
            +"m1"
            +"m2"
            +"m3"
        }
        releasedAt = "2020-07-15T08:00:00Z"  // Optional, is auto generated if not defined, or can use a variable.
        assets { // Optional, multiple asset links
            links {
                +link {
                    name = "asset1"
                    url = "https://example.com/assets/1"
                }

                +link {
                    name = "asset2"
                    url = "https://example.com/assets/2"
                    filepath = "/pretty/url/1" // optional
                    linkType = "other" // optional
                }
            }
        }
    }
}
```

* To create a release automatically when commits are pushed or merged to the default branch, using a new Git tag that is defined with variables:

Environment variables set in `before_script` or `script` are not available for expanding in the same job. Read more about [potentially making variables available for expanding](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/6400).

```kotlin
val prepare by stage()
val release by stage()

job("prepare_job") {
    stage = prepare // This stage must run before the release stage
    rules {
        +rule {
            `if`("\$CI_COMMIT_TAG")
            `when` = When.NEVER // Do not run this job when a tag is created manually
        }
        +rule {
            `if`("\$CI_COMMIT_BRANCH == \$CI_DEFAULT_BRANCH") // Run this job when commits are pushed or merged to the default branch
        }
    }
    script {
        +shell("echo \"EXTRA_DESCRIPTION=some message\" >> variables.env") // Generate the EXTRA_DESCRIPTION and TAG environment variables
        +shell("echo \"TAG=v\$(cat VERSION)\" >> variables.env") // and append to the variables.env file
    }
    artifacts {
        reports {
            dotenv = "variables.env" // Use artifacts:reports:dotenv to expose the variables to other jobs
        }
    }
}

job("release_job") {
    stage = release
    image("registry.gitlab.com/gitlab-org/release-cli:latest")
    needs {
        +need {
            job = "prepare_job"
            artifacts = true
        }
    }
    rules {
        +rule {
            `if`("\$CI_COMMIT_TAG")
            `when` = When.NEVER // Do not run this job when a tag is created manually
        }
        +rule {
            `if`("\$CI_COMMIT_BRANCH == \$CI_DEFAULT_BRANCH") // Run this job when commits are pushed or merged to the default branch
        }
    }
    script {
        +shell("echo 'running release_job for \$TAG'")
    }
    release {
        name = "Release \$TAG"
        description = "Created using the release-cli \$EXTRA_DESCRIPTION" // $EXTRA_DESCRIPTION and the $TAG
        tagName = "$TAG" // variables must be defined elsewhere
        ref = "\$CI_COMMIT_SHA" // in the pipeline. For example, in the prepare_job
        milestones {
            +"m1"
            +"m2"
            +"m3"
        }
        releasedAt = "2020-07-15T08:00:00Z"  // Optional, is auto generated if not defined, or can use a variable.
        assets {
            links {
                +link {
                    name = "asset1"
                    url = "https://example.com/assets/1"
                }
                +link {
                    name = "asset2"
                    url = "https://example.com/assets/2"
                    filepath = "/pretty/url/1" // optional
                    linkType = "other" // optional
                }
            }
        }
    }
}
```

## Release assets as Generic packages
You can use [Generic packages](https://docs.gitlab.com/ee/user/packages/generic_packages/) to host your release assets. For a complete example, see the [Release assets as Generic packages](https://gitlab.com/gitlab-org/release-cli/-/tree/master/docs/examples/release-assets-as-generic-package/) project.

## `release-cli` command line
The entries under the `release` node are transformed into a `bash` command line and sent to the Docker container, which contains the [release-cli](https://gitlab.com/gitlab-org/release-cli). You can also call the `release-cli` directly from a `script` entry.

For example, if you use the YAML described previously:

```bash
release-cli create --name "Release $CI_COMMIT_SHA" --description "Created using the release-cli $EXTRA_DESCRIPTION" --tag-name "v${MAJOR}.${MINOR}.${REVISION}" --ref "$CI_COMMIT_SHA" --released-at "2020-07-15T08:00:00Z" --milestone "m1" --milestone "m2" --milestone "m3" --assets-link "{\"name\":\"asset1\",\"url\":\"https://example.com/assets/1\",\"link_type\":\"other\"}
```
