---
title: when
---

Use when to implement jobs that run in case of failure or despite the failure.

The valid values of `when` are:

1. `ON_SUCCESS` (default) - Execute job only when all jobs in earlier stages succeed, or are considered successful because they have `allowFailure = true`.
2. `ON_FAILURE` - Execute job only when at least one job in an earlier stage fails.
3. `ALWAYS` - Execute job regardless of the status of jobs in earlier stages.
4. `MANUAL` - Execute job manually.
5. `DELAYED` - Delay the execution of a job for a specified duration
6. `NEVER`:
* With job `rules`, don’t execute job.
* With `workflow:rules`, don’t run pipeline.
In the following example, the script:

1. Executes `cleanup_build_job` only when `build_job` fails.
2. Always executes `cleanup_job` as the last step in pipeline regardless of success or failure.
3. Executes `deploy_job` when you run it manually in the GitLab UI.

```kotlin
val build by stage()
val cleanupBuild by stage("cleanup_build")
val test by stage()
val deploy by stage()
val cleanup by stage()

job("build_job") {
    stage = build
    script {
        +shell("make build")
    }
}

job("cleanup_build_job") {
    stage = cleanup_build
    script {
        +shell("cleanup build when failed")
    }
    `when` =  When.ON_FAILURE
}

job("test_job") {
    stage = test
    script {
        +shell("make test")
    }
}

job("deploy_job") {
    stage = deploy
    script {
        +shell("make deploy")
    }
    `when` = When.MANUAL
}

job("cleanup_job") {
    stage = cleanup
    script {
        +shell("cleanup after jobs")
    }
    `when` = When.ALWAYS
}
```

## `when:manual`
A manual job is a type of job that is not executed automatically and must be explicitly started by a user. You might want to use manual jobs for things like deploying to production.

To make a job manual, add ``` `when` = When.MANUAL``` to its configuration.

When the pipeline starts, manual jobs display as skipped and do not run automatically. They can be started from the pipeline, job, [environment](https://docs.gitlab.com/ee/ci/environments/index.html#configure-manual-deployments), and deployment views.

Manual jobs can be either optional or blocking:

* **Optional**: Manual jobs have `allowFailure = true` set by default and are considered optional. The status of an optional manual job does not contribute to the overall pipeline status. A pipeline can succeed even if all its manual jobs fail.

* **Blocking**: To make a blocking manual job, add `allowFailure = false` to its configuration. Blocking manual jobs stop further execution of the pipeline at the stage where the job is defined. To let the pipeline continue running, click **Play** on the blocking manual job.

Merge requests in projects with [merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html) enabled can’t be merged with a blocked pipeline. Blocked pipelines show a status of **blocked**.

When you use `rules:`, `allow_failure` defaults to `false`, including for manual jobs.

To trigger a manual job, a user must have permission to merge to the assigned branch. You can use [protected branches](https://docs.gitlab.com/ee/user/project/protected_branches.html) to more strictly [protect manual deployments](https://docs.gitlab.com/ee/ci/yaml/README.html#protecting-manual-jobs) from being run by unauthorized users.

In GitLab 13.5 and later, you can use `when:manual` in the same job as [trigger](../trigger). In GitLab 13.4 and earlier, using them together causes the error `jobs:#{job-name} when should be on_success, on_failure or always`.

### Protecting manual jobs
Use [protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html) to define a list of users authorized to run a manual job. You can authorize only the users associated with a protected environment to trigger manual jobs, which can:

* More precisely limit who can deploy to an environment.
* Block a pipeline until an approved user “approves” it.
To protect a manual job:

1. Add an `environment` to the job. For example:

```kotlin
val deploy by stage()

job("deploy_prod") {
    stage = deploy
    script {
        +shell("echo \"Deploy to production server\"")
    }
    environment {
        name = "production"
        url = "https://example.com"
        `when` = When.MANUAL
        only {
            +"master"
        }
    }
}
```

2. In the [protected environments settings](https://docs.gitlab.com/ee/ci/environments/protected_environments.html#protecting-environments), select the environment (`production` in this example) and add the users, roles or groups that are authorized to trigger the manual job to the **Allowed to Deploy** list. Only those in this list can trigger this manual job, as well as GitLab administrators who are always able to use protected environments.

You can use protected environments with blocking manual jobs to have a list of users allowed to approve later pipeline stages. Add `allowFailure = false` to the protected manual job and the pipeline’s next stages only run after the manual job is triggered by authorized users.

## `when:delayed`
Introduced in GitLab 11.4.

Use ``` `when` = When.DELAYED``` to execute scripts after a waiting period, or if you want to avoid jobs immediately entering the pending state.

You can set the period with `startIn` keyword. The value of `startIn` is an elapsed time in seconds, unless a unit is provided. `startIn` must be less than or equal to one week. Examples of valid values include:

* `'5'`
* `5 seconds`
* `30 minutes`
* `1 day`
* `1 week`

When a stage includes a delayed job, the pipeline doesn’t progress until the delayed job finishes. You can use this keyword to insert delays between different stages.

The timer of a delayed job starts immediately after the previous stage completes. Similar to other types of jobs, a delayed job’s timer doesn’t start unless the previous stage passes.

The following example creates a job named `timed rollout 10%` that is executed 30 minutes after the previous stage completes:

```kotlin
val deploy by stage()

job("timed rollout 10%") {
    stage = deploy
    script {
        +shell("echo 'Rolling out 10% ...'")
    }
    `when` = When.DELAYED
    startIn = "30 minutes"
}
```

To stop the active timer of a delayed job, click the **Unschedule** button. This job can no longer be scheduled to run automatically. You can, however, execute the job manually.

To start a delayed job immediately, click the **Play** button. Soon GitLab Runner picks up and starts the job.

