---
title: only/except
---

`only` and `except` are not being actively developed. `rules` is the preferred keyword to control when to add jobs to pipelines.

You can use `only` and `except` to control when to add jobs to pipelines.

* Use `only` to define when a job runs.
* Use `except` to define when a job **does not** run.

Four keywords can be used with `only` and `except`:

* `refs`
* `variables`
* `changes`
* `kubernetes`

See [specify when jobs run with `only` and `except`](https://docs.gitlab.com/ee/ci/jobs/job_control.html#specify-when-jobs-run-with-only-and-except) for more details and examples.

## `only:refs` / `except:refs`
Use the `only:refs` and `except:refs` keywords to control when to add jobs to a pipeline based on branch names or pipeline types.

**Keyword type**: Job keyword. You can use it only as part of a job.

**Possible inputs**: An array including any number of:

* Branch names, for example `main` or `my-feature-branch`.
* [Regular expressions](https://docs.gitlab.com/ee/ci/jobs/job_control.html#only--except-regex-syntax) that match against branch names, for example `/^feature-.*/`.
* The following keywords:

| Value                     | Description |
| :------------------------ | :---------- |
| `api`	                    | For pipelines triggered by the [pipelines API](https://docs.gitlab.com/ee/api/pipelines.html#create-a-new-pipeline). |
| `branches`                | When the Git reference for a pipeline is a branch. |
| `chat`	                | For pipelines created by using a [GitLab ChatOps](https://docs.gitlab.com/ee/ci/chatops/index.html) command. |
| `external`	            | When you use CI services other than GitLab. |
| `external_pull_requests`	| When an external pull request on GitHub is created or updated (See [Pipelines for external pull requests](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/index.html#pipelines-for-external-pull-requests)). |
| `merge_requests`          | For pipelines created when a merge request is created or updated. Enables [merge request pipelines](https://docs.gitlab.com/ee/ci/merge_request_pipelines/index.html), [merged results pipelines](https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/index.html), and [merge trains](https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/index.html). |
| `pipelines`	            | For [multi-project pipelines](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html) created by [using the API with `CI_JOB_TOKEN`](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html#triggering-multi-project-pipelines-through-api), or the [`trigger`](../trigger) keyword. |
| `pushes`	                | For pipelines triggered by a `git push` event, including for branches and tags. |
| `schedules`	            | For [scheduled pipelines](https://docs.gitlab.com/ee/ci/pipelines/schedules.html). |
| `tags`	                | When the Git reference for a pipeline is a tag. |
| `triggers`	            | For pipelines created by using a [trigger token](https://docs.gitlab.com/ee/ci/triggers/README.html#trigger-token). |
| `web`	                    | For pipelines created by using **Run pipeline** button in the GitLab UI, from the project’s **CI/CD > Pipelines** section. |

**Example of `only:refs` and `except:refs`**:

```kotlin
job("job1") {
    script {
        +shell("echo")
    }
    only {
        refs {
            +"main"
            +"/^issue-.*\$/"
            +"merge_requests"
        }
    }
}

job("job2") {
    script {
        +shell("echo")
    }
    except {
        refs {
            +"main"
            +"/^stable-branch.*\$/"
            +"schedules"
        }
    }
}
```

**Additional details**:

* Scheduled pipelines run on specific branches, so jobs configured with `only: branches` run on scheduled pipelines too. Add `except: schedules` to prevent jobs with `only: branches` from running on scheduled pipelines.
* If a job does not use `only`, `except`, or `rules`, then only is set to `branches` and `tags` by default.

For example, `job1` and `job2` are equivalent:

```kotlin
job("job1") {
    script {
        +shell("echo 'test'")
    }
}

job("job2") {
    script {
        +shell("echo 'test'")
    }
    only {
        refs {
            +"branches"
            +"tags"
        }
    }
}
```

## `only:variables` / `except:variables`
Use the `only:variables` or `except:variables` keywords to control when to add jobs to a pipeline, based on the status of [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/README.html).

**Keyword type**: Job keyword. You can use it only as part of a job.

**Possible inputs**: An array of [CI/CD variable expressions](https://docs.gitlab.com/ee/ci/jobs/job_control.html#cicd-variable-expressions).

**Example of `only:variables`**:

```kotlin
job("deploy") {
    script {
        +shell("cap staging deploy")
    }
    only {
        variables {
            +"\$RELEASE == \"staging\""
            +"\$STAGING"
        }
    }
}
```

Related topics:

* [`only:variables` and `except:variables` examples](https://docs.gitlab.com/ee/ci/jobs/job_control.html#only-variables--except-variables-examples).

## `only:changes` / `except:changes`
[Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/19232) in GitLab 11.4.

Use the `changes` keyword with `only` to run a job, or with `except` to skip a job, when a Git push event modifies a file.

Use `changes` in pipelines with the following refs:

* `branches`
* `external_pull_requests`
* `merge_requests` (see additional details about [using `only:changes` with pipelines for merge requests](https://docs.gitlab.com/ee/ci/jobs/job_control.html#use-onlychanges-with-pipelines-for-merge-requests))

**Keyword type**: Job keyword. You can use it only as part of a job.

**Possible inputs**: An array including any number of:
* Paths to files.
* Wildcard paths for single directories, for example `path/to/directory/*`, or a directory and all its subdirectories, for example `path/to/directory/**/*`.
* Wildcard ([glob](https://en.wikipedia.org/wiki/Glob_(programming))) paths for all files with the same extension or multiple extensions, for example `*.md` or `path/to/directory/*.{rb,py,sh}`.
* Wildcard paths to files in the root directory, or all directories, wrapped in double quotes. For example `"*.json"` or `"**/*.json"`.

**Example of `only:changes`**:

```kotlin
job("docker build") {
    script {
        +shell("docker build -t my-image:\$CI_COMMIT_REF_SLUG .")
    }
    only {
        refs {
            +"branches"
        }
        changes {
            +"Dockerfile"
            +"docker/scripts/*"
            +"dockerfiles/**/*"
            +"more_scripts/*.{rb,py,sh}"
        }
    }
}
```

**Additional details**:
* If you use refs other than `branches`, `external_pull_requests`, or `merge_requests`, changes can’t determine if a given file is new or old and always returns `true`.
* If you use `only: changes` with other refs, jobs ignore the changes and always run.
* If you use `except: changes` with other refs, jobs ignore the changes and never run.

**Related topics**:
* [`only: changes` and `except: changes` examples](https://docs.gitlab.com/ee/ci/jobs/job_control.html#onlychanges--exceptchanges-examples).
* If you use `changes` with [only allow merge requests to be merged if the pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html#only-allow-merge-requests-to-be-merged-if-the-pipeline-succeeds), you should [also use `only:merge_requests`](https://docs.gitlab.com/ee/ci/jobs/job_control.html#use-onlychanges-with-pipelines-for-merge-requests).
* Use `changes` with [new branches or tags *without* pipelines for merge requests](https://docs.gitlab.com/ee/ci/jobs/job_control.html#use-onlychanges-without-pipelines-for-merge-requests).
* Use changes with [scheduled pipelines](https://docs.gitlab.com/ee/ci/jobs/job_control.html#use-onlychanges-with-scheduled-pipelines).

## `only:kubernetes` / `except:kubernetes`
Use `only:kubernetes` or `except:kubernetes` to control if jobs are added to the pipeline when the Kubernetes service is active in the project.

**Keyword type**: Job-specific. You can use it only as part of a job.

**Possible inputs**: The `kubernetes` strategy accepts only the `active` keyword.

**Example of `only:kubernetes`**:

```kotlin
job("deploy") {
    only {
        kubernetes = Kubernetes.ACTIVE
    }
}
```

In this example, the `deploy` job runs only when the Kubernetes service is active in the project.