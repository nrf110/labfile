---
title: extends
---

Introduced in GitLab 11.3.

Use `extends` to reuse configuration sections. It’s an alternative to YAML anchors and is a little more flexible and readable. You can use `extends` to reuse configuration from [included configuration files](#use-extends-and-include-together).

In the following example, the `rspec` job uses the configuration from the `.tests` template job. GitLab:
* Performs a reverse deep merge based on the keys.
* Merges the `.tests` content with the `rspec` job.
* Doesn’t merge the values of the keys.

```kotlin
val test by stage()

job(".tests") {
    script {
        +shell("rake test")
    }
    stage = test
    only {
        refs {
            +"branches"
        }
    }
}

job("rspec") {
    extends = ".tests"
    script {
        +shell("rake rspec")
    }
    only {
        variables {
            +"\$RSPEC"
        }
    }
}
```

The result is this rspec job:

```yaml
rspec:
    script:
        - |-
          rake rspec
    stage: |-
      test
    only:
      refs:
        - |-
          branches
      variables:
        - |-
          $RSPEC
```

`.tests` in this example is a [hidden job](https://docs.gitlab.com/ee/ci/yaml/README.html#hide-jobs), but it’s possible to extend configuration from regular jobs as well.

`extends` supports multi-level inheritance. You should avoid using more than three levels, but you can use as many as eleven. The following example has two levels of inheritance:

```kotlin
job(".tests") {
    rules {
        +rule {
            `if`("\$CI_PIPELINE_SOURCE == 'push'")
        }
    }
}

job(".rspec") {
    extends = ".tests"
    script {
        +shell("rake rspec")
    }
}

job("rspec 1") {
    variables {
        "RSPEC_SUITE" to 1
    }
    extends = ".rspec"
}

job("rspec 2") {
    variables {
        "RSPEC_SUITE" to 2
    }
    extends = ".rspec"
}

job("spinach") {
    extends = ".tests"
    script  {
        +shell("rake spinach")
    }
}
```

In GitLab 12.0 and later, it’s also possible to use multiple parents for `extends`.

## Merge details
You can use `extends` to merge hashes but not arrays. The algorithm used for merge is “closest scope wins,” so keys from the last member always override anything defined on other levels. For example:

```kotlin
job(".only-important") {
    variables {
        "URL" to "http://my-url.internal"
        "IMPORTANT_VAR" to "the details"
    }
    rules {
        +rule {
            `if`("\$CI_COMMIT_BRANCH == 'master'")
        }
        +rule {
            `if`("\$CI_COMMIT_BRANCH == 'stable'")
        }
    }
    tags {
        +"production"
    }
    script {
        +shell("echo \"Hello world!\"")
    }
}

job(".in-docker") {
    variables {
        "URL" to "http://docker-url.internal"
    }
    tags {
        +"docker"
    }
    image("alpine")
}

job("rspec") {
    variables {
        "GITLAB" to "is-awesome"
    }
    extends {
        +".only-important"
        +".in-docker"
    }
    script {
        +shell("rake rspec")
    }
}
```

The result is this rspec job:

```yaml
rspec:
    variables:
        "URL": "http://docker-url.internal"
        "IMPORTANT_VAR": "the details"
        "GITLAB": "is-awesome"
    rules:
        - if: |-
          $CI_COMMIT_BRANCH == 'master'
        - if: |-
          $CI_COMMIT_BRANCH == 'stable'

    tags:
        - |-
          docker
    image: alpine
    script:
        - |-
          rake rspec
```

In this example:

* The `variables` sections merge, but `URL: "http://docker-url.internal"` overwrites `URL: "http://my-url.internal"`.
* `tags: ['docker']` overwrites `tags: ['production']`.
* `script` does not merge, but s`cript: ['rake rspec']` overwrites script: `['echo "Hello world!"']`.

## Use `extends` and `include` together
To reuse configuration from different configuration files, combine `extends` and `include`.

In the following example, a `script` is defined in the `included.yml` file. Then, in the `Labfile.main.kts` file, `extends` refers to the contents of the `script`:

* `included.yml`:
```yaml
.template:
    script:
        - echo Hello!
```

* `Labfile.main.kts`:
```kotlin
include("included.yml")

job("useTemplate") {
    image("alpine")
    extends(".template")
}
```

