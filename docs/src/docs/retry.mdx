---
title: retry
---

Use `retry` to configure how many times a job is retried in case of a failure.

When a job fails, the job is processed again, until the limit specified by the `retry` keyword is reached.

If `retry` is set to `2`, and a job succeeds in a second run (first retry), it is not retried. The `retry` value must be a positive integer, from `0` to `2` (two retries maximum, three runs in total).

The following example retries all failure cases:

```kotlin
job("test") {
    script {
        +shell("rspec")
    }
    retry(2)
}
```

By default, a job is retried on all failure cases. To have better control over which failures to retry, `retry` can be a hash with the following keys:

* `max`: The maximum number of retries.
* `when`: The failure cases to retry.

To retry only runner system failures at maximum two times:

```kotlin
job("test") {
    script {
        +shell("rspec")
    }
    retry(2) {
        `when` {
            +Retry.When.RUNNER_SYSTEM_FAILURE
        }
    }
}
```

If there is another failure, other than a runner system failure, the job is not retried.

To retry on multiple failure cases, `when` can also be an array of failures:

```kotlin
job("test") {
    script {
        +shell("rspec")
    }
    retry(2) {
        `when`  {
            +Retry.When.RUNNER_SYSTEM_FAILURE
            +Retry.When.STUCK_OR_TIMEOUT_FAILURE
        }
    }
}
```

Possible values for `Retry.When` are:

* `ALWAYS`: Retry on any failure (default).
* `UNKNOWN_FAILURE`: Retry when the failure reason is unknown.
* `SCRIPT_FAILURE`: Retry when the script failed.
* `API_FAILURE`: Retry on API failure.
* `STUCK_OR_TIMEOUT_FAILURE`: Retry when the job got stuck or timed out.
* `RUNNER_SYSTEM_FAILURE`: Retry if there is a runner system failure (for example, job setup failed).
* `MISSING_DEPENDENCY_FAILURE`: Retry if a dependency is missing.
* `RUNNER_UNSUPPORTED`: Retry if the runner is unsupported.
* `STALE_SCHEDULED`: Retry if a delayed job could not be executed.
* `JOB_EXECUTION_TIMEOUT`: Retry if the script exceeded the maximum execution time set for the job.
* `ARCHIVED_FAILURE`: Retry if the job is archived and can’t be run.
* `UNMET_PREREQUISITES`: Retry if the job failed to complete prerequisite tasks.
* `SCHEDULER_FAILURE`: Retry if the scheduler failed to assign the job to a runner.
* `DATA_INTEGRITY_FAILURE`: Retry if there is a structural integrity problem detected.

You can specify the number of [retry attempts for certain stages of job execution](https://docs.gitlab.com/ee/ci/runners/README.html#job-stages-attempts) using variables.