---
title: resourceGroup
---

##
Sometimes running multiple jobs or pipelines at the same time in an environment can lead to errors during the deployment.

To avoid these errors, use the `resourceGroup` attribute to make sure that the runner doesn’t run certain jobs simultaneously. Resource groups behave similar to semaphores in other programming languages.

When the `resourceGroup` keyword is defined for a job in the `.gitlab-ci.yml` file, job executions are mutually exclusive across different pipelines for the same project. If multiple jobs belonging to the same resource group are enqueued simultaneously, only one of the jobs is picked by the runner. The other jobs wait until the `resourceGroup` is free.

For example:

```kotlin
job("deploy-to-production") {
    script {
        +shell("deploy")
    }
    resourceGroup = "production"
}
```

In this case, two `deploy-to-production` jobs in two separate pipelines can never run at the same time. As a result, you can ensure that concurrent deployments never happen to the production environment.

You can define multiple resource groups per environment. For example, when deploying to physical devices, you may have multiple physical devices. Each device can be deployed to, but there can be only one deployment per device at any given time.

The `resourceGroup` value can only contain letters, digits, `-`, `_`, `/`, `$`, `{`, `}`, `.`, and spaces. It can’t start or end with `/`.

For more information, see [Deployments Safety](https://docs.gitlab.com/ee/ci/environments/deployment_safety.html).

## Pipeline-level concurrency control with Cross-Project/Parent-Child pipelines

You can define `resourceGroup` for downstream pipelines that are sensitive to concurrent executions. The [`trigger` keyword](../trigger) can trigger downstream pipelines. The [`resourceGroup`](./) keyword can co-exist with it. This is useful to control the concurrency for deployment pipelines, while running non-sensitive jobs concurrently.

The following example has two pipeline configurations in a project. When a pipeline starts running, non-sensitive jobs are executed first and aren’t affected by concurrent executions in other pipelines. However, GitLab ensures that there are no other deployment pipelines running before triggering a deployment (child) pipeline. If other deployment pipelines are running, GitLab waits until those pipelines finish before running another one.

```yaml
# .gitlab-ci.yml (parent pipeline)
build:
  stage: build
  script: echo "Building..."

test:
  stage: test
  script: echo "Testing..."

labfile:
  stage: generate
  image: nrf110/kotlin:1.4-jdk11-buster
  script:
    - ./Labfile.main.kts
    - cat labfile.yml
  artifacts:
    paths:
      - labfile.yml

deploy:
  stage: deploy
  trigger:
    include: Labfile.main.kts
    strategy: depend
  resource_group: AWS-production
```

```kotlin
// deploy.gitlab-ci.yml (child pipeline)
val provision by stage()
val deploy by stage()

job("provision") {
    stage = provision
    script {
        +shell("echo \"Provisioning...\"")
    }
}

job("deployment") {
    stage = deploy
    script {
        shell("echo \"Deploying...\"")
    }
}
```

You must define [`strategy: depend`](../trigger#linking-pipelines-with-triggerstrategy) with the `trigger` keyword. This ensures that the lock isn’t released until the downstream pipeline finishes.

