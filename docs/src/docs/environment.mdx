---
title: environment
---

Use `environment` to define the [environment](https://docs.gitlab.com/ee/ci/environments/index.html) that a job deploys to. For example:

```kotlin
val deploy by stage()

job("deploy to production") {
    stage = deploy
    environment("production")
    script  {
        +shell("git push production HEAD:main")
    }
}
```

You can assign a value to the `environment` keyword by using:
* Plain text, like `production`.
* Variables, including CI/CD variables, predefined, secure, or variables defined in the `.gitlab-ci.yml` file.

You can’t use variables defined in a `script` section.

If you specify an `environment` and no environment with that name exists, an environment is created.

## `environment:name`
Set a name for an [`environment`](https://docs.gitlab.com/ee/ci/environments/index.html). For example:

```kotlin
val deploy by stage()

job("deploy to production") {
    stage = deploy
    environment("production")
    script {
        +"git push production HEAD:main"
    }
}
```

Common environment names are `qa`, `staging`, and `production`, but you can use any name you want.

You can assign a value to the `name` keyword by using:
* Plain text, like `staging`.
* Variables, including CI/CD variables, predefined, secure, or variables defined in the `.gitlab-ci.yml` file.

You can’t use variables defined in a `script` section.

The environment `name` can contain:
* Letters
* Digits
* Spaces
* `-`
* `_`
* `/`
* `$`
* `{`
* `}`

## `environment:url`
Set a URL for an [`environment`](https://docs.gitlab.com/ee/ci/environments/index.html). For example:

```kotlin
val deploy by stage()

job("deploy to production") {
    stage = deploy
    environment("production") {
        url = "https://prod.example.com"
    }

    script {
        +shell("git push production HEAD:main")
    }
}
```

After the job completes, you can access the URL by using a button in the merge request, environment, or deployment pages.

You can assign a value to the `url` keyword by using:
* Plain text, like `https://prod.example.com`.
* Variables, including CI/CD variables, predefined, secure, or variables defined in the `.gitlab-ci.yml` file.

You can’t use variables defined in a `script` section.

## `environment:on_stop`
Closing (stopping) environments can be achieved with the `on_stop` keyword defined under `environment`. It declares a different job that runs to close the environment.

Read the `environment:action` section for an example.

## `environment:action`
Use the `action` keyword to specify jobs that prepare, start, or stop environments.

| Value     | Description                                                                                                                           |
|:--------- | :------------------------------------------------------------------------------------------------------------------------------------ |
| start     |	Default value. Indicates that job starts the environment. The deployment is created after the job <starts></starts>                 |
| prepare	|   Indicates that the job is only preparing the environment. It does not trigger deployments. Read more about preparing environments.  |
| stop      |	Indicates that job stops deployment. See the example below.                                                                         |

Take for instance:

```kotlin
val deploy by stage()

job("review_app") {
    stage = deploy
    script {
        +shell("make deploy-app")
    }
    environment("review/\$CI_COMMIT_REF_NAME") {
        url = "https://\$CI_ENVIRONMENT_SLUG.example.com"
        onStop = "stop_review_app"
    }
}

job("stop_review_app") {
    stage = deploy
    `when` = When.MANUAL

    variables {
        "GIT_STRATEGY" to "none"
    }
    script {
        +shell("make delete-app")
    }

    environment("review/\$CI_COMMIT_REF_NAME") {
        action = Environment.Action.Stop
    }
}
```

In the above example, the `review_app` job deploys to the `review` environment. A new `stop_review_app` job is listed under `onStop`. After the `review_app` job is finished, it triggers the `stop_review_app` job based on what is defined under when. In this case, it is set to `manual`, so it needs a [manual action](../when/#whenmanual) from the GitLab UI to run.

Also in the example, `GIT_STRATEGY` is set to `none`. If the `stop_review_app` job is [automatically triggered](https://docs.gitlab.com/ee/ci/environments/index.html#stopping-an-environment), the runner won’t try to check out the code after the branch is deleted.

The `stop_review_app` job is **required** to have the following keywords defined:
* ``` `when` ```, defined at either:
    * [The job level](../when).
    * [In a rules clause](../rules). If you use `rules:` and `when: When.MANUAL`, you should also set [`allow_failure: true`](../allow-failure) so the pipeline can complete even if the job doesn’t run.
* `environment:name`
* `environment:action`

Additionally, both jobs should have matching [`rules`](../rules) or [`only/except`](../only-except) configuration.

In the examples above, if the configuration is not identical:

* The `stop_review_app` job might not be included in all pipelines that include the `review_app` job.
* It is not possible to trigger the `action = Environment.Action.STOP` to stop the environment automatically.

## `environment:autoStopIn`
The `auto_stop_in` keyword is for specifying the lifetime of the environment, that when expired, GitLab automatically stops them.

For example,

```kotlin
job("review_app") {
    script {
        +shell("deploy-review-app")
    }

    environment("review/\$CI_COMMIT_REF_NAME") {
        autoStopIn = "1 day"
    }
}
```

When the environment for `review_app` is created, the environment’s lifetime is set to `1 day`. Every time the review app is deployed, that lifetime is also reset to `1 day`.

For more information, see [the environments auto-stop documentation](https://docs.gitlab.com/ee/ci/environments/index.html#stop-an-environment-after-a-certain-time-period)

## `environment:kubernetes`
Use the `kubernetes` keyword to configure deployments to a [Kubernetes cluster](https://docs.gitlab.com/ee/user/project/clusters/index.html) that is associated with your project.

For example:

```kotlin
val deploy by stage()

job("deploy") {
    stage = deploy
    script {
        +shell("make deploy-app")
    }
    environment("production") {
        kubernetes {
            namespace = "production"
        }
    }
}
```

This configuration sets up the `deploy` job to deploy to the `production` environment, using the `production` [Kubernetes namespace](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/).

For more information, see [Available settings for `kubernetes`](https://docs.gitlab.com/ee/ci/environments/index.html#configure-kubernetes-deployments).

**Kubernetes configuration is not supported for Kubernetes clusters that are [managed by GitLab](https://docs.gitlab.com/ee/user/project/clusters/index.html#gitlab-managed-clusters). To follow progress on support for GitLab-managed clusters, see the [relevant issue](https://gitlab.com/gitlab-org/gitlab/-/issues/38054).**

## `environment:deployment_tier`
Use the `deployment_tier` keyword to specify the tier of the deployment environment:

```kotlin
job("deploy") {
    script {
        +shell("echo")
    }
    environment("customer-portal") {
        deploymentTier = "production"
    }
}
```

For more information, see [Deployment tier of environments](https://docs.gitlab.com/ee/ci/environments/index.html#deployment-tier-of-environments).

## Dynamic environments
Use CI/CD [variables](https://docs.gitlab.com/ee/ci/variables/index.html) to dynamically name environments.

For example:

```kotlin
val deploy by stage()

job("deploy as review app") {
    stage = deploy
    script {
        +shell("make deploy")
    }
    environment("review/\$CI_COMMIT_REF_NAME") {
        url = "https://\$CI_ENVIRONMENT_SLUG.example.com/"
    }
}
```

The `deploy as review app` job is marked as a deployment to dynamically create the `review/$CI_COMMIT_REF_NAME` environment. `$CI_COMMIT_REF_NAME` is a [CI/CD variable](https://docs.gitlab.com/ee/ci/variables/index.html) set by the runner. The `$CI_ENVIRONMENT_SLUG` variable is based on the environment name, but suitable for inclusion in URLs. If the `deploy as review app` job runs in a branch named `pow`, this environment would be accessible with a URL like `https://review-pow.example.com/`.

The common use case is to create dynamic environments for branches and use them as Review Apps. You can see an example that uses Review Apps at [https://gitlab.com/gitlab-examples/review-apps-nginx/](https://gitlab.com/gitlab-examples/review-apps-nginx/).

