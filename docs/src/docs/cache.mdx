---
title: cache
---

Use `cache` to specify a list of files and directories to cache between jobs. You can only use paths that are in the local working copy.

Caching is shared between pipelines and jobs. Caches are restored before [artifacts](../artifacts).

Learn more about caches in [Caching in GitLab CI/CD](https://docs.gitlab.com/ee/ci/caching/index.html).

## `cache:paths`
Use the `cache:paths` keyword to choose which files or directories to cache.

**Keyword type**: Job-specific. You can use it only as part of a job.

**Possible inputs**: An array of paths relative to the project directory (`$CI_PROJECT_DIR`). You can use wildcards that use [glob](https://en.wikipedia.org/wiki/Glob_(programming)) patterns:

In [GitLab Runner 13.0](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/2620) and later, [doublestar.Glob](https://pkg.go.dev/github.com/bmatcuk/doublestar@v1.2.2?tab=doc#Match).
In GitLab Runner 12.10 and earlier, [filepath.Match](https://pkg.go.dev/path/filepath#Match).

**Example of `cache:paths`:**

Cache all files in `binaries` that end in `.apk` and the `.config` file:

```kotlin
job("rspec") {
    script {
        +shell("echo \"This job uses a cache.\"")
    }
    cache {
        key("binaries-cache")
        paths {
            +"binaries/*.apk"
            +".config"
        }
    }
}
```

**Related topics:**
* See the common cache use cases for more `cache:paths` examples.

## `cache:key`
Use the `cache:key` keyword to give each cache a unique identifying key. All jobs that use the same cache key use the same cache, including in different pipelines.

If not set, the default key is `default`. All jobs with the `cache:` keyword but no `cache:key` share the `default` cache.

**Keyword type**: Job-specific. You can use it only as part of a job.

**Possible inputs**:
* A string.
* A [predefined variable](https://docs.gitlab.com/ee/ci/variables/index.html).
* A combination of both.

**Example of `cache:key`:**

```kotlin
job("cache-job") {
    script {
        +shell("echo \"This job uses a cache.\"")
    }
    cache {
        key("binaries-cache-\$CI_COMMIT_REF_SLUG")
        paths {
            +"binaries/"
        }
    }
}
```

**Additional details:**

* If you use **Windows Batch** to run your shell scripts you need to replace `$` with `%`. For example: `key("%CI_COMMIT_REF_SLUG%")`
* The `cache:key` value can’t contain:
    * The `/` character, or the equivalent URI-encoded `%2F`.
    * Only the `.` character (any number), or the equivalent URI-encoded `%2E`.
* The cache is shared between jobs, so if you’re using different paths for different jobs, you should also set a different `cache:key`. Otherwise cache content can be overwritten.

**Related topics:**

* You can specify a [fallback cache key](https://docs.gitlab.com/ee/ci/caching/index.html#use-a-fallback-cache-key) to use if the specified `cache:key` is not found.
* You can use multiple cache keys in a single job.
* See the common cache use cases for more `cache:key` examples.

## `cache:key:files`
Use the `cache:key:files` keyword to generate a new key when one or two specific files change. `cache:key:files` lets you reuse some caches, and rebuild them less often, which speeds up subsequent pipeline runs.

**Keyword type**: Job-specific. You can use it only as part of a job.

**Possible inputs**: An array of one or two file paths.

**Example of `cache:key:files`:**

```kotlin
job("cache-job") {
    script {
        +shell("echo \"This job uses a cache.\"")
    }
    cache {
        key {
            files {
                +"Gemfile.lock"
                +"package.json"
            }
        }
        paths {
            +"vendor/ruby"
            +"node_modules"
        }
    }
}
```

This example creates a cache for Ruby and Node.js dependencies. The cache is tied to the current versions of the `Gemfile.lock` and `package.json` files. When one of these files changes, a new cache key is computed and a new cache is created. Any future job runs that use the same `Gemfile.lock` and `package.json` with `cache:key:files` use the new cache, instead of rebuilding the dependencies.

**Additional details**: The cache `key` is a SHA computed from the most recent commits that changed each listed file. If neither file is changed in any commits, the fallback key is default.

## `cache:key:prefix`
Use `cache:key:prefix` to combine a prefix with the SHA computed for `cache:key:files`.

**Keyword type**: Job-specific. You can use it only as part of a job.

**Possible inputs:**
* A string.
* A [predefined variable](https://docs.gitlab.com/ee/ci/variables/index.html).
* A combination of both.

**Example of `cache:key:prefix`:**

```kotlin
job("rspec") {
    script {
        +shell("echo \"This rspec job uses a cache.\"")
    }
    cache {
        key("\$CI_JOB_NAME") {
            files {
                +"Gemfile.lock"
            }
        }
        paths {
            +"vendor/ruby"
        }
    }
}
```

For example, adding a prefix of `$CI_JOB_NAME` causes the key to look like `rspec-feef9576d21ee9b6a32e30c5c79d0a0ceb68d1e5`. If a branch changes `Gemfile.lock`, that branch has a new SHA checksum for `cache:key:files`. A new cache key is generated, and a new cache is created for that key. If `Gemfile.lock` is not found, the prefix is added to `default`, so the key in the example would be `rspec-default`.

**Additional details**: If no file in `cache:key:files` is changed in any commits, the prefix is added to the default key.

## `cache:untracked`
Use `untracked: true` to cache all files that are untracked in your Git repository:

**Keyword type**: Job-specific. You can use it only as part of a job.

**Possible inputs**: `true` or `false` (default).

**Example of `cache:untracked`:**

```kotlin
job("rspec") {
    script {
        +shell("test")
    }
    cache {
        untracked = true
    }
}
```

**Additional details:**

You can combine `cache:untracked` with `cache:paths` to cache all untracked files as well as files in the configured paths. For example:

```kotlin
job("rspec") {
    script {
        +shell("test")
    }
    cache {
        untracked = true
        paths {
            +"binaries/"
        }
    }
}
```

## `cache:unprotect`
Use `cache:unprotect` to set a cache to be shared between protected and unprotected branches.

When set to true, users without access to protected branches can read and write to cache keys used by protected branches.
**Keyword type**: Job keyword. You can use it only as part of a job or in the default section.

**Possible inputs:**

`true` or `false` (default).
**Example of `cache:unprotect`:**

```kotlin
job("rspec") {
  script {
    +shell("test")
  }
  cache {
    unprotect = true
  }
}
```

## `cache:when`

Use `cache:when` to define when to save the cache, based on the status of the job.

**Keyword type**: Job-specific. You can use it only as part of a job.

**Possible inputs:**

* `Cache.When.ON_SUCCESS` (default): Save the cache only when the job succeeds.
* `Cache.When.ON_FAILURE`: Save the cache only when the job fails.
* `Cache.When.ALWAYS`: Always save the cache.

**Example of `cache:when`:**

```kotlin
job("rspec") {
    script {
        +shell("rspec")
    }
    cache {
        paths {
            +"rspec/"
        }
        `when` = Cache.When.ALWAYS
    }
}
```

This example stores the cache whether or not the job fails or succeeds.

## `cache:policy`
To change the upload and download behavior of a cache, use the `cache:policy` keyword. By default, the job downloads the cache when the job starts, and uploads changes to the cache when the job ends. This is the `pull-push` policy (default).

To set a job to only download the cache when the job starts, but never upload changes when the job finishes, use `Cache.Policy.PULL`.

To set a job to only upload a cache when the job finishes, but never download the cache when the job starts, use `Cache.Policy.PUSH`.

Use the `PULL` policy when you have many jobs executing in parallel that use the same cache. This policy speeds up job execution and reduces load on the cache server. You can use a job with the `PUSH` policy to build the cache.

**Keyword type**: Job-specific. You can use it only as part of a job.

**Possible inputs:**
* `Cache.When.PULL`
* `Cache.When.PUSH`
* `null` (default, pull-push)

**Example of cache:policy:**

```kotlin
val build by stage()
val test by stage()

job("prepare-dependencies-job") {
    stage = build
    cache {
        key("gems")
        paths {
            +"vendor/bundle"
        }
        policy = Cache.Policy.PUSH
    }
    script {
        +shell("echo \"This job only downloads dependencies and builds the cache.\"")
        +shell("echo \"Downloading dependencies...\"")
    }
}

job("faster-test-job") {
    stage = test
    cache {
        key("gems")
        paths {
            +"vendor/bundle"
        }
        policy = Cache.Policy.PULL
    }
    script {
        +shell("echo \"This job script uses the cache, but does not update it.\"")
        +shell("echo \"Running tests...\"")
    }
}
```

## `cache:fallback_keys`

Use `cache:fallback_keys` to specify a list of keys to try to restore cache from if there is no cache found for the cache:key. Caches are retrieved in the order specified in the fallback_keys section.

**Keyword type**: Job keyword. You can use it only as part of a job or in the default section.

**Possible inputs:**

An array of cache keys
**Example of `cache:fallback_keys`:**

```kotlin
job("rspec") {
  script {
    +shell("rspec")
  }
  cache {
    key("gems-$CI_COMMIT_REF_SLUG")
    paths {
      +"rspec/"
    }
    fallback_keys {
      +"gems"
    }
    when = When.ALWAYS
  }
}
```

