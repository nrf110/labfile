---
title: rules
---

Use `rules` to include or exclude jobs in pipelines.

Rules are evaluated in order until the first match. When a match is found, the job is either included or excluded from the pipeline, depending on the configuration.

`rules` replaces [only/except](../only-except) and they can’t be used together in the same job. If you configure one job to use both keywords, the GitLab returns a `key may not be used with rules` error.

`rules` accepts an array of rules defined with:

* `if`
* `changes`
* `exists`
* `allowFailure`
* `variables`
* `when`

You can combine multiple keywords together for [complex rules](https://docs.gitlab.com/ee/ci/jobs/job_control.html#complex-rules).

The job is added to the pipeline:

If an `if`, `changes`, or `exists` rule matches and also has ``` `when` = When.ON_SUCCESS``` (default), ``` `when` = When.DELAYED```, or ``` `when` = When.ALWAYS```.
If a rule is reached that is only ``` `when` = When.ON_SUCCESS```, ``` `when` = When.DELAYED```, or ``` `when` = When.ALWAYS```.
The job is not added to the pipeline:

* If no rules match.
* If a rule matches and has ``` `when` = When.NEVER```.

## `rules:if`
Use `rules:if` clauses to specify when to add a job to a pipeline:

* If an `if` statement is true, add the job to the pipeline.
* If an `if` statement is true, but it’s combined with ``` `when` = When.NEVER```, do not add the job to the pipeline.
* If no `if` statements are true, do not add the job to the pipeline.

`if`: clauses are evaluated based on the values of [predefined CI/CD variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) or [custom CI/CD variables](https://docs.gitlab.com/ee/ci/variables/README.html#custom-cicd-variables).

**Keyword type**: Job-specific and pipeline-specific. You can use it as part of a job to configure the job behavior, or with [workflow](../global-keywords#workflow) to configure the pipeline behavior.

**Possible inputs**: [A CI/CD variable expression](https://docs.gitlab.com/ee/ci/jobs/job_control.html#cicd-variable-expressions).

**Example of `rules:if`**:

```kotlin
job {
    script {
        +shell("echo \"Hello, Rules!\"")
    }
    rules {
        +rule {
            `if`("\$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^feature/ && \$CI_MERGE_REQUEST_TARGET_BRANCH_NAME != \$CI_DEFAULT_BRANCH")
            `when` = When.NEVER
        }
        +rule {
            `if`("\$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^feature/")
            `when` = When.MANUAL
            allowFailure = true
        }
        +rule {
            `if`("\$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME")
        }
    }
}
```

**Additional details**:

* If a rule matches and has no `when` defined, the rule uses the `when` defined for the job, which defaults to `When.ON_SUCCESS` if not defined.
* You can define `when` once per rule, or once at the job-level, which applies to all rules. You can’t mix `when` at the job-level with `when` in rules.
* Unlike variables in [script](https://docs.gitlab.com/ee/ci/variables/README.html#use-cicd-variables-in-job-scripts) sections, variables in rules expressions are always formatted as `\$VARIABLE`.

**Related topics**:

* [Common if expressions for rules](https://docs.gitlab.com/ee/ci/jobs/job_control.html#common-if-clauses-for-rules).
* [Avoid duplicate pipelines](https://docs.gitlab.com/ee/ci/jobs/job_control.html#avoid-duplicate-pipelines).

## `rules:changes`
Use `rules:changes` to specify when to add a job to a pipeline by checking for changes to specific files.

You should use `rules: changes` only with **branch pipelines** or **merge request pipelines**. You can use `rules: changes` with other pipeline types, but `rules: changes` always evaluates to `true` when there is no Git `push` event. Tag pipelines, scheduled pipelines, and so on do **not** have a Git `push` event associated with them. A `rules: changes` job is **always** added to those pipelines if there is no `if:` that limits the job to branch or merge request pipelines.

**Keyword type**: Job keyword. You can use it only as part of a job.

**Possible inputs**: An array of file paths. In GitLab 13.6 and later, [file paths can include variables](https://docs.gitlab.com/ee/ci/jobs/job_control.html#variables-in-ruleschanges).

**Example of `rules:changes`**:

```kotlin
job("docker build") {
    script {
        +shell("docker build -t my-image:\$CI_COMMIT_REF_SLUG .")
    }
    rules {
        +rule {
            `if`("\$CI_PIPELINE_SOURCE == \"merge_request_event\"")
            changes {
                +"Dockerfile"
            }
            `when` = When.MANUAL
            allowFailure = true
        }
    }
}
```

* If the pipeline is a merge request pipeline, check `Dockerfile` for changes.
* If `Dockerfile` has changed, add the job to the pipeline as a manual job, and the pipeline continues running even if the job is not triggered (`allowFailure = true`).
* If `Dockerfile` has not changed, do not add job to any pipeline (same as `when = When.NEVER`).

**Additional details**:

* `rules: changes` works the same way as [`only: changes` and `except: changes`](../only-except/#onlychanges--exceptchanges).
* You can use `when: never` to implement a rule similar to `except:changes`.

## `rules:exists`
[`Introduced`](https://gitlab.com/gitlab-org/gitlab/-/issues/24021) in GitLab 12.4.

Use `exists` to run a job when certain files exist in the repository.

**Keyword type**: Job keyword. You can use it only as part of a job.

**Possible inputs**: An array of file paths. Paths are relative to the project directory (`$CI_PROJECT_DIR`) and can’t directly link outside it. File paths can use glob patterns.

**Example of `rules:exists`**:

```kotlin
job("job") {
    script {
        +shell("docker build -t my-image:\$CI_COMMIT_REF_SLUG .")
    }
    rules {
        +rule {
            exists {
                +"Dockerfile"
            }
        }
    }
}
```

`job` runs if a `Dockerfile` exists anywhere in the repository.

**Additional details**:
* Glob patterns are interpreted with Ruby [`File.fnmatch`](https://docs.ruby-lang.org/en/2.7.0/File.html#method-c-fnmatch) with the flags `File::FNM_PATHNAME | File::FNM_DOTMATCH | File::FNM_EXTGLOB`.
* For performance reasons, GitLab matches a maximum of 10,000 `exists` patterns or file paths. After the 10,000th check, rules with patterned globs always match. In other words, the `exists` rule always assumes a match in projects with more than 10,000 files.

## `rules:allow_failure`
[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/30235) in GitLab 12.8.

Use [`allow_failure: true`](https://docs.gitlab.com/ee/ci/yaml/README.html#allow_failure) in `rules:` to allow a job to fail without stopping the pipeline.

You can also use `allow_failure: true` with a manual job. The pipeline continues running without waiting for the result of the manual job. `allow_failure: false` combined with `when: manual` in rules causes the pipeline to wait for the manual job to run before continuing.

**Keyword type**: Job keyword. You can use it only as part of a job.

**Possible inputs**: `true` or `false`. Defaults to `false` if not defined.

**Example of `rules:allow_failure`**:

```kotlin
job("job") {
    script {
        +shell("echo \"Hello, Rules!\"")
    }
    rules {
        +rule {
            `if`("\$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == \$CI_DEFAULT_BRANCH")
            `when` = When.MANUAL
            allowFailure = true
        }
    }
}
```
If the rule matches, then the job is a manual job with `allow_failure: true`.

**Additional details**:
* The rule-level `rules:allow_failure` overrides the job-level [`allow_failure`](../allow-failure), and only applies when the specific rule triggers the job.

## `rules:needs`

Use `needs` in rules to update a job’s [`needs`](../needs) for specific conditions. When a condition matches a rule, the job’s `needs` configuration is completely replaced with the `needs` in the rule.

**Keyword type**: Job-specific. You can use it only as part of a job.

**Possible inputs**:

- An array of job names as strings.
- A hash with a job name, optionally with additional attributes.
- An empty array (), to set the job needs to none when the specific condition is met.

**Example of `rules:needs`**:

```kotlin
val build by stage()
val test by stage()

job("build-dev") {
  stage = build
  rules {
    +rule {
        `if`("\$CI_COMMIT_BRANCH != \$CI_DEFAULT_BRANCH")

    }
  }
  script {
      +shell("echo \"Feature branch, so building dev version...\"")
  }
}

job("build-prod") {
  stage = build
  rules {
    +rule {
        `if`("\$CI_COMMIT_BRANCH == \$CI_DEFAULT_BRANCH")
    }
  }
  script {
      +shell("echo \"Default branch, so building prod version...\"")
  }
}

job("specs") {
  stage = test
  needs  {
      +"build-dev"
  }
  rules {
    +rule {
        `if`("\$CI_COMMIT_REF_NAME == \$CI_DEFAULT_BRANCH")
        needs {
            +"build-prod"
        }
    }
    +rule {
        `when` = When.ON_SUCCESS // Run the job in other cases
    }
  }
  script {
    +shell("echo \"Running dev specs by default, or prod specs when default branch...\"")
  }
}
```

In this example:

- If the pipeline runs on a branch that is not the default branch, the `specs` job needs the `build-dev` job (default behavior).
- If the pipeline runs on the default branch, and therefore the rule matches the condition, the `specs` job needs the `build-prod` job instead.

**Additional details**:

- `needs` in rules override any `needs` defined at the job-level. When overridden, the behavior is same as [ob-level `needs`](../needs).
- `needs` in rules can accept [`artifacts`]() and [`optional`]().

## `rules:variables`
Version history
Use [`variables`](../variables) in `rules:` to define variables for specific conditions.

**Keyword type**: Job-specific. You can use it only as part of a job.

**Possible inputs**: A hash of variables in the format `"VARIABLE-NAME" to "value"`.

**Example of `rules:variables`**:

```kotlin
job("job") {
    variables {
        "DEPLOY_VARIABLE" to "default-deploy"
    }
    rules {
        +rule {
            `if`("\$CI_COMMIT_REF_NAME == \$CI_DEFAULT_BRANCH")
            variables { // Override DEPLOY_VARIABLE defined
                "DEPLOY_VARIABLE" to "deploy-production"  // at the job level.
            }
        }
        +rule {
            `if`("\$CI_COMMIT_REF_NAME =~ /feature/")
            variables {
                "IS_A_FEATURE" to "true" // Define a new variable.
            }
        }
    }
    script {
        +shell("echo \"Run script with \$DEPLOY_VARIABLE as an argument\"")
        +shell("echo \"Run another script if \$IS_A_FEATURE exists\"")
    }
}
```