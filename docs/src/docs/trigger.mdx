---
title: trigger
---

Use `trigger` to declare that a job is a “trigger job” which starts a downstream pipeline that is either:

- [A multi-project pipeline](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#multi-project-pipelines).
- [A child pipeline](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#parent-child-pipelines).

Trigger jobs can use only a limited set of GitLab CI/CD configuration keywords. The keywords available for use in trigger jobs are:

- [allow_failure](../allow-failure).
- [extends](../extends).
- [needs](../needs), but not [needs:project](../needs).
- [only](../only-except) and [except](../only-except).
- [rules](../rules).
- [stage](../stage).
- [trigger](../trigger).
- [variables](../variables).
- [when](../when) (only with a value of `on_success`, `on_failure`, or `always`).
- [resource_group](../resource-group).
- [environment](../environment).

Additional details:

- You [cannot use the API to start `when:manual` trigger jobs](https://gitlab.com/gitlab-org/gitlab/-/issues/284086).
- You can use [`when:manual`](../when) in the same job as `trigger`.
- You cannot [manually specify CI/CD variables](https://docs.gitlab.com/ee/ci/jobs/index.html#specifying-variables-when-running-manual-jobs) before running a manual trigger job.
- [Manual pipeline variables and scheduled pipeline variables](https://docs.gitlab.com/ee/ci/variables/index.html#override-a-defined-cicd-variable) are not passed to downstream pipelines by default. Use [`trigger:forward`](#triggerforward) to forward these variables to downstream pipelines.
- [Job-level persisted variables](https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html#persisted-variables) are not available in trigger jobs.
- Environment variables [defined in the runner’s `config.toml`](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runners-section) are not available to trigger jobs and are not passed to downstream pipelines.

**Related topics**:

- [Multi-project pipeline configuration examples](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#trigger-a-downstream-pipeline-from-a-job-in-the-gitlab-ciyml-file).
- To run a pipeline for a specific branch, tag, or commit, you can use a [trigger token](https://docs.gitlab.com/ee/ci/triggers/index.html) to authenticate with the [pipeline triggers API](https://docs.gitlab.com/ee/api/pipeline_triggers.html). The trigger token is different than the `trigger` keyword.

## `trigger:include`
Use `trigger:include` to declare that a job is a “trigger job” which starts a [child pipeline](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#parent-child-pipelines).

Use `trigger:include:artifact` to trigger a [dynamic child pipeline](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#dynamic-child-pipelines).

**Keyword type**: Job keyword. You can use it only as part of a job.

**Possible inputs**:

- The path to the child pipeline’s configuration file.

**Example of `trigger:include`**:

```kotlin
job("trigger-child-pipeline") {
  trigger {
    include {
        +"path/to/child-pipeline.gitlab-ci.yml"
    }
  }
}
```

**Related topics**:

- [Child pipeline configuration examples](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#trigger-a-downstream-pipeline-from-a-job-in-the-gitlab-ciyml-file).

## `trigger:project`
Use `trigger:project` to declare that a job is a “trigger job” which starts a multi-project pipeline.

By default, the multi-project pipeline triggers for the default branch. Use `trigger:branch` to specify a different branch.

**Keyword type**: Job keyword. You can use it only as part of a job.

**Possible inputs**:

- The path to the downstream project. CI/CD variables [are supported](https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html#gitlab-ciyml-file) in GitLab 15.3 and later, but not [job-level persisted variables](https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html#persisted-variables).

**Example of `trigger:project`**:

```kotlin
job("trigger-multi-project-pipeline") {
  trigger {
    project = "my-group/my-project"
  }
}
```

**Example of `trigger:project` for a different branch**:

```kotlin
job("trigger-multi-project-pipeline") {
  trigger {
    project = "my-group/my-project"
    branch = "development"
  }
}

**Related topics**:

- [Multi-project pipeline configuration examples](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#trigger-a-downstream-pipeline-from-a-job-in-the-gitlab-ciyml-file).
- To run a pipeline for a specific branch, tag, or commit, you can also use a [trigger token](https://docs.gitlab.com/ee/ci/triggers/index.html) to authenticate with the [pipeline triggers API](https://docs.gitlab.com/ee/api/pipeline_triggers.html). The trigger token is different than the `trigger` keyword.

## `trigger:strategy`
Use `trigger:strategy` to force the `trigger` job to wait for the downstream pipeline to complete before it is marked as success.

This behavior is different than the default, which is for the trigger job to be marked as **success** as soon as the downstream pipeline is created.

This setting makes your pipeline execution linear rather than parallel.

**Example of `trigger:strategy`**:

```kotlin
job("trigger_job") {
  trigger {
    include {
      +"path/to/child-pipeline.yml"
    }
    strategy = Trigger.Strategy.DEPEND
  }
}
```

In this example, jobs from subsequent stages wait for the triggered pipeline to successfully complete before starting.

**Additional details**:

- [Optional manual jobs](https://docs.gitlab.com/ee/ci/jobs/job_control.html#types-of-manual-jobs) in the downstream pipeline do not affect the status of the downstream pipeline or the upstream trigger job. The downstream pipeline can complete successfully without running any optional manual jobs.
- [Blocking manual jobs](https://docs.gitlab.com/ee/ci/jobs/job_control.html#types-of-manual-jobs) in the downstream pipeline must run before the trigger job is marked as successful or failed. The trigger job shows **pending** if the downstream pipeline status is **waiting for manual action** due to manual jobs. By default, jobs in later stages do not start until the trigger job completes.
- If the downstream pipeline has a failed job, but the job uses [`allow_failure: true`](../allow-failure), the downstream pipeline is considered successful and the trigger job shows **success**.

## `trigger:forward`

Use `trigger:forward` to specify what to forward to the downstream pipeline. You can control what is forwarded to both [parent-child pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#parent-child-pipelines) and [multi-project pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#multi-project-pipelines).

**Possible inputs**:

- `yaml_variables`: `true` (default), or `false`. When `true`, variables defined in the trigger job are passed to downstream pipelines.
- `pipeline_variables`: `true` or `false` (default). When `true`, [manual pipeline variables](https://docs.gitlab.com/ee/ci/variables/index.html#override-a-defined-cicd-variable) and [scheduled pipeline variables](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#add-a-pipeline-schedule) are passed to downstream pipelines.

**Example of `trigger:forward`**:

[Run this pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/index.html#run-a-pipeline-manually), with the CI/CD variable `MYVAR = my value`:

```kotlin
pipeline {
  variables { // default variables for each job
    "VAR" to "value"
  }

  /**
   * Default behavior:
   * - VAR is passed to the child
   * - MYVAR is not passed to the child
   */
  job("child1") {
    trigger {
      include {
        +".child-pipeline.yml"
      }
    }
  }

  /**
   * Forward pipeline variables:
   * - VAR is passed to the child
   * - MYVAR is passed to the child
   */
  job("child2") {
    trigger {
      include {
        +".child-pipeline.yml"
      }
      forward {
        pipelineVariables = true
      }
    }
  }

  /**
   * Do not forward YAML variables:
   * - VAR is not passed to the child
   * - MYVAR is not passed to the child
   */
  job("child3") {
    trigger {
      include {
        +".child-pipeline.yml"
      }
      forward {
        yamlVariables = false
      }
    }
  }
}
```