---
title: Pipeline Keywords
---

## stage
Use stage to define stages that contain groups of jobs. stages are defined globally for the pipeline. Use stage in a job to define which stage the job is part of.

The order of the stages items defines the execution order for jobs:

Jobs in the same stage run in parallel.
Jobs in the next stage run after the jobs from the previous stage complete successfully.
For example:

```kotlin
val build by stage()
val test by stage()
val deploy by stage()
```

All jobs in build execute in parallel.
If all jobs in build succeed, the test jobs execute in parallel.
If all jobs in test succeed, the deploy jobs execute in parallel.
If all jobs in deploy succeed, the pipeline is marked as passed.
If any job fails, the pipeline is marked as failed and jobs in later stages do not start. Jobs in the current stage are not stopped and continue to run.

If no stages are defined in the Labfile.main.kts, then build, test and deploy are the default pipeline stages.

If a job does not specify a stage, the job is assigned the test stage.

To make a job start earlier and ignore the stage order, use the needs keyword.

## workflow
Use `workflow` to determine whether or not a pipeline is created. Define this keyword at the top level, with a single `rules` keyword that is similar to `rules` [defined in jobs](/docs/jobs).

You can use the `workflow:rules` templates to import a preconfigured `workflow: rules` entry.

`workflow:rules` accepts these config blocks:

* `if`: Check this rule to determine when to run a pipeline.
* `when`: Specify what to do when the `if` rule evaluates to true.
** To run a pipeline, set to always.
** To prevent pipelines from running, set to never.
* `variables`: If not defined, uses the variables defined elsewhere.

When no rules evaluate to true, the pipeline does not run.

Some example `if` clauses for `workflow: rules`:

| Example rules                                                 | Details                                                   |
| :------------------------------------------------------------ | :-------------------------------------------------------- |
| `` `if`("\$CI_PIPELINE_SOURCE == \"merge_request_event\"") `` | Control when merge request pipelines run.                 |
| `` `if`("\$CI_PIPELINE_SOURCE == \"push\"") ``                | Control when both branch pipelines and tag pipelines run. |
| `` `if`("\"$CI_COMMIT_TAG\"") ``                              | Control when tag pipelines run.                           |
| `` `if`("\"$CI_COMMIT_BRANCH\"") ``                           | Control when branch pipelines run.                        |
See the common if clauses for rules for more examples.

In the following example, pipelines run for all push events (changes to branches and new tags). Pipelines for push events with -draft in the commit message don’t run, because they are set to when: never. Pipelines for schedules or merge requests don’t run either, because no rules evaluate to true for them:

```kotlin
workflow {
    rules {
        +rule {
            `if`("\$CI_COMMIT_MESSAGE =~ /-draft$/")
            `when` = When.NEVER
        }
        +rule {
            `if`("\$CI_PIPELINE_SOURCE == 'push'")
        }
    }
}
```

This example has strict rules, and pipelines do not run in any other case.

Alternatively, all of the rules can be when: never, with a final when: always rule. Pipelines that match the when: never rules do not run. All other pipeline types run:

```kotlin
workflow {
    rules {
        +rule {
            `if`("\$CI_PIPELINE_SOURCE == 'schedule'")
            `when` = When.NEVER
        }
        +rule {
            `if`("\$CI_PIPELINE_SOURCE == 'push'")
            `when` = When.NEVER
        }
        +rule {
            `when` = When.ALWAYS
        }
    }
}
```
This example prevents pipelines for schedules or push (branches and tags) pipelines. The final when: always rule runs all other pipeline types, including merge request pipelines.

If your rules match both branch pipelines and merge request pipelines, duplicate pipelines can occur.

### `workflow:rules:variables`
You can use [variables](/docs/variables) in `workflow:rules:` to define variables for specific pipeline conditions.

For example:

```kotlin
variables {
    "DEPLOY_VARIABLE" to "default-deploy"
}

workflow {
    rules {
        +rule {
            `if`("\$CI_COMMIT_REF_NAME =~ /master/")
            variables {
                "DEPLOY_VARIABLE" to "deploy-production"  // Override globally-defined DEPLOY_VARIABLE
            }
        }
        +rule {
            `if`("\$CI_COMMIT_REF_NAME =~ /feature/")
            variables {
                "IS_A_FEATURE" to true // Define a new variable.
            }
        }
        +rule {
            `when` = When.ALWAYS // Run the pipeline in other cases
        }
    }
}

job("job1") {
    variables {
        "DEPLOY_VARIABLE" to "job1-default-deploy"
    }
    rules {
        +rule {
            `if`("\$CI_COMMIT_REF_NAME =~ /master/")
            variables { // Override DEPLOY_VARIABLE defined
                "DEPLOY_VARIABLE" to "job1-deploy-production"  // at the job level.
            }
        }
        +rule {
            `when` = When.ON_SUCCESS // Run the job in other cases
        }
    }
    script {
        +shell("echo \"Run script with \$DEPLOY_VARIABLE as an argument\"")
        +shell("echo \"Run another script if $IS_A_FEATURE exists\"")
    }
}

job("job2") {
    script {
        +shell("echo \"Run script with $DEPLOY_VARIABLE as an argument\"")
        +shell("echo \"Run another script if $IS_A_FEATURE exists\"")
    }
}
```

When the branch is `master`:

* job1’s `DEPLOY_VARIABLE` is `job1-deploy-production`.
* job2’s `DEPLOY_VARIABLE` is `deploy-production`.

When the branch is `feature`:

* job1’s `DEPLOY_VARIABLE` is `job1-default-deploy`, and `IS_A_FEATURE` is `true`.
* job2’s `DEPLOY_VARIABLE` is `default-deploy`, and `IS_A_FEATURE` is `true`.

When the branch is something else:

* job1’s `DEPLOY_VARIABLE` is `job1-default-deploy`.
* job2’s `DEPLOY_VARIABLE` is `default-deploy`.

### `workflow:rules` templates
GitLab provides templates that set up `workflow: rules` for common scenarios. These templates help prevent duplicate pipelines.

The Branch-Pipelines template makes your pipelines run for branches and tags.

Branch pipeline status is displayed in merge requests that use the branch as a source. However, this pipeline type does not support any features offered by merge request pipelines, like pipelines for merge results or merge trains. This template intentionally avoids those features.

To include it:

```kotlin
include {
  +template("Workflows/Branch-Pipelines.gitlab-ci.yml")
}
```

The MergeRequest-Pipelines template makes your pipelines run for the default branch, tags, and all types of merge request pipelines. Use this template if you use any of the the pipelines for merge requests features.

To include it:

```kotlin
include {
  +template("Workflows/MergeRequest-Pipelines.gitlab-ci.yml")
}
```

### Switch between branch pipelines and merge request pipelines

To make the pipeline switch from branch pipelines to merge request pipelines after a merge request is created, add a `workflow: rules` section to your `.gitlab-ci.yml` file.

If you use both pipeline types at the same time, duplicate pipelines might run at the same time. To prevent [duplicate pipelines](/docs/rules#prevent-duplicate-pipelines), use the [`CI_OPEN_MERGE_REQUESTS` variable](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).

The following example is for a project that runs branch and merge request pipelines only, but does not run pipelines for any other case. It runs:

* Branch pipelines when a merge request is not open for the branch.
* Merge request pipelines when a merge request is open for the branch.

```kotlin
workflow {
    rules {
        +rule {
            `if`("\$CI_PIPELINE_SOURCE == 'merge_request_event'")
        }
        +rule {
            `if`("\$CI_COMMIT_BRANCH && \$CI_OPEN_MERGE_REQUESTS")
            `when` = When.NEVER
        }
        +rule {
            `if`("\$CI_COMMIT_BRANCH")
        }
    }
}
```

If the pipeline is triggered by:

* A merge request, run a merge request pipeline. For example, a merge request pipeline can be triggered by a push to a branch with an associated open merge request.
* A change to a branch, but a merge request is open for that branch, do not run a branch pipeline.
* A change to a branch, but without any open merge requests, run a branch pipeline.

You can also add a rule to an existing `workflow` section to switch from branch pipelines to merge request pipelines when a merge request is created.

Add this rule to the top of the `workflow` section, followed by the other rules that were already present:

```kotlin
workflow {
    rules {
        +rule {
            `if`("\$CI_COMMIT_BRANCH && \$CI_OPEN_MERGE_REQUESTS && \$CI_PIPELINE_SOURCE == 'push'")
            `when` = When.NEVER
        }
        +rule {
            //... Previously defined workflow rules here
        }
    }
}
```

[Triggered pipelines](https://docs.gitlab.com/ee/ci/triggers/README.html) that run on a branch have a `$CI_COMMIT_BRANCH` set and could be blocked by a similar rule. Triggered pipelines have a pipeline source of `trigger` or `pipeline`, so `&& $CI_PIPELINE_SOURCE == "push"` ensures the rule does not block triggered pipelines.

## `include`

Use `include` to include external YAML files in your CI/CD configuration. You can break down one long `gitlab-ci.yml` file into multiple files to increase readability, or reduce duplication of the same configuration in multiple places.

You can also store template files in a central repository and `include` them in projects.

`include` requires the external YAML file to have the extensions `.yml` or `.yaml`, otherwise the external file is not included.

`include` supports the following inclusion methods:


| Keyword  | Method                                                         |
| :------- | :------------------------------------------------------------- |
| local    | Include a file from the local project repository.              |
| file	   | Include a file from a different project repository.            |
| remote   | Include a file from a remote URL. Must be publicly accessible. |
| template | Include templates that are provided by GitLab.                 |

When the pipeline starts, the `.gitlab-ci.yml` file configuration included by all methods is evaluated. The configuration is a snapshot in time and persists in the database. GitLab does not reflect any changes to the referenced `.gitlab-ci.yml` file configuration until the next pipeline starts.

The `include` files are:

Deep merged with those in the `.gitlab-ci.yml` file.
Always evaluated first and merged with the content of the `.gitlab-ci.yml` file, regardless of the position of the `include` keyword.

### Variables with `include`
You can [use some predefined variables in `include` sections](https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html#gitlab-ciyml-file) in your `.gitlab-ci.yml` file:

```kotlin
include {
    +project("\$CI_PROJECT_PATH") {
        file {
            +".compliance-gitlab-ci.yml"
        }
    }
}
```

For an example of how you can include these predefined variables, and the variables’ impact on CI/CD jobs, see this [CI/CD variable demo](https://youtu.be/4XR8gw3Pkos).

### `include:local`
Use `include:local` to include a file that is in the same repository as the `.gitlab-ci.yml` file. Use a full path relative to the root directory (`/`).

If you use `include:local`, make sure that both the `.gitlab-ci.yml` file and the local file are on the same branch.

You can’t include local files through Git submodules paths.

All [nested includes](https://docs.gitlab.com/ee/ci/yaml/README.html#nested-includes) are executed in the scope of the same project, so it’s possible to use local, project, remote, or template includes.

Example:

```kotlin
include {
    +local("/templates/.gitlab-ci-template.yml")
}
```

Use local includes instead of symbolic links.

### `include:local` with wildcard file paths
You can use wildcard paths (`*`) with `include:local`.

Example:

```kotlin
include {
    +local("configs/*.yml")
}
```

When the pipeline runs, it adds all `.yml` files in the `configs` folder into the pipeline configuration.

### `include:file`
To include files from another private project on the same GitLab instance, use `include:file`. You can use `include:file` in combination with `include:project` only. Use a full path, relative to the root directory (/).

For example:

```kotlin
include {
    +project("my-group/my-project") {
        file {
            +"templates/.gitlab-ci-template.yml"
        }
    }
}
```

You can also specify a `ref`. If you do not specify a value, the `ref` defaults to the `HEAD` of the project:

```kotlin
include {
    +project("my-group/my-project") {
        ref = "master"
        file {
            +"/templates/.gitlab-ci-template.yml"
        }
    }
    +project("my-group/my-project") {
        ref = "v1.0.0"
        file {
            +"/templates/.gitlab-ci-template.yml"
        }
    }
    +project("my-group/my-project") {
        ref = "b47f14b552955ca2786bc9542ae66fee5b" // Git SHA
        file {
            +"/templates/.gitlab-ci-template.yml"
        }
    }
}
```

All [nested includes](https://docs.gitlab.com/ee/ci/yaml/README.html#nested-includes) are executed in the scope of the target project. You can use local (relative to target project), project, remote, or template includes.

**Multiple files from a project**

You can include multiple files from the same project:

```kotlin
include {
    +project("my-group/my-project") {
        ref = "master"
        file {
            +"/templates/.builds.yml"
            +"/templates/.tests.yml"
        }
    }
}
```

### `include:remote`
Use `include:remote` with a full URL to include a file from a different location. The remote file must be publicly accessible by an HTTP/HTTPS GET request, because authentication in the remote URL is not supported. For example:

```kotlin
include {
    +remote("https://gitlab.com/example-project/-/raw/master/.gitlab-ci.yml")
}
```

All [nested includes](https://docs.gitlab.com/ee/ci/yaml/README.html#nested-includes) execute without context as a public user, so you can only `include` public projects or templates.

### `include:template`

Use `include:template` to include `.gitlab-ci.yml` templates that are [shipped with GitLab](https://gitlab.com/gitlab-org/gitlab/tree/master/lib/gitlab/ci/templates).

For example:

```kotlin
// File sourced from the GitLab template collection
include {
    +template("Auto-DevOps.gitlab-ci.yml")
}
```

Multiple `include:template` files:

```kotlin
include {
    +template("Android-Fastlane.gitlab-ci.yml")
    +template("Auto-DevOps.gitlab-ci.yml")
}
```

All [nested includes](https://docs.gitlab.com/ee/ci/yaml/README.html#nested-includes) are executed only with the permission of the user, so it’s possible to use project, remote or template includes.
