---
title: parallel
---

Use `parallel` to configure how many instances of a job to run in parallel. The value can be from 2 to 50.

The `parallel` keyword creates N instances of the same job that run in parallel. They are named sequentially from `job_name 1/N` to `job_name N/N`:

```kotlin
job("test") {
    script {
        +shell("rspec")
    }
    parallel(5)
}
```

Every parallel job has a `CI_NODE_INDEX` and `CI_NODE_TOTAL` [predefined CI/CD variable](https://docs.gitlab.com/ee/ci/variables/README.html#predefined-cicd-variables) set.

Different languages and test suites have different methods to enable parallelization. For example, use [Semaphore Test Boosters](https://github.com/renderedtext/test-boosters) and RSpec to run Ruby tests in parallel:

```ruby
# Gemfile
source 'https://rubygems.org'

gem 'rspec'
gem 'semaphore_test_boosters'
```

```kotlin
job("test") {
    parallel(3)
    script {
        +shell("bundle")
        +shell("bundle exec rspec_booster --job \$CI_NODE_INDEX/\$CI_NODE_TOTAL")
    }
}
```

You can then navigate to the **Jobs** tab of a new pipeline build and see your RSpec job split into three separate jobs.

## `parallel:matrix`

Use `matrix` to run a job multiple times in parallel in a single pipeline, but with different variable values for each instance of the job. There can be from 2 to 50 jobs.

Jobs can only run in parallel if there are multiple runners, or a single runner is [configured to run multiple jobs concurrently](https://docs.gitlab.com/ee/ci/yaml/README.html#use-your-own-runners).

Every job gets the same `CI_NODE_TOTAL` [CI/CD variable](https://docs.gitlab.com/ee/ci/variables/README.html#predefined-cicd-variables) value, and a unique `CI_NODE_INDEX` value.

```kotlin
val deploy by stage()

job("deploystacks") {
    stage = deploy
    script {
        +shell("bin/deploy")
    }
    parallel {
        matrix {
            +mapOf(
                "PROVIDER" to listOf("aws"),
                "STACK" to listOf(
                    "monitoring",
                    "app1",
                    "app2"
                )
              )
            +mapOf(
                "PROVIDER" to listOf("ovh"),
                "STACK" to listOf(
                    "monitoring",
                    "backup",
                    "app"
                )
            )
            +mapOf(
                "PROVIDER" to listOf("gcp", "vultr"),
                "STACK" to listOf("data", "processing")
            )
        }
    }
}
```

The following example generates 10 parallel `deploystacks` jobs, each with different values for `PROVIDER` and `STACK`:

```
deploystacks: [aws, monitoring]
deploystacks: [aws, app1]
deploystacks: [aws, app2]
deploystacks: [ovh, monitoring]
deploystacks: [ovh, backup]
deploystacks: [ovh, app]
deploystacks: [gcp, data]
deploystacks: [gcp, processing]
deploystacks: [vultr, data]
deploystacks: [vultr, processing]
```

## One-dimensional `matrix` jobs
You can also have one-dimensional matrices with a single job:

```kotlin
val deploy by stage()

job("deploystacks") {
    stage = deploy
    script {
        +shell("bin/deploy")
    }
    parallel {
        matrix {
            +mapOf("PROVIDER" to listOf("aws", "ovh", "gcp", "vultr"))
        }
    }
}
```

## Parallel `matrix` trigger jobs

Use `matrix` to run a [trigger](../trigger) job multiple times in parallel in a single pipeline, but with different variable values for each instance of the job.

```kotlin
val deploy by stage()

job("deploystacks") {
    stage = deploy
    trigger {
        child {
            include {
                local("path/to/child-pipeline.yml")
            }
        }
    }
    parallel {
        matrix {
            +mapOf(
                "PROVIDER" to listOf("aws"),
                "STACK" to listOf("monitoring", "app1")
            )
            +mapOf(
                "PROVIDER" to listOf("ovh"),
                "STACK" to listOf("monitoring", "backup")
            )
            +mapOf(
                "PROVIDER" to listOf("gcp", "vultr"),
                "STACK" to listOf("data")
            )
        }
    }
}
```

This example generates 6 parallel `deploystacks` trigger jobs, each with different values for `PROVIDER` and `STACK`, and they create 6 different child pipelines with those variables.

```
deploystacks: [aws, monitoring]
deploystacks: [aws, app1]
deploystacks: [ovh, monitoring]
deploystacks: [ovh, backup]
deploystacks: [gcp, data]
deploystacks: [vultr, data]
```

