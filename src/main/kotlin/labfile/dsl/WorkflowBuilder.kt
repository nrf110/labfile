package labfile.dsl

import labfile.ast.Workflow

class WorkflowBuilder(private val rules: HasRules = HasRules()) : IHasRules by rules {
    fun build(): Workflow = Workflow(rules = this.rules.value ?: emptyList())
}
