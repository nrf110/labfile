package labfile.dsl

import labfile.ast.PullPolicy
import labfile.ast.Service

class ServiceBuilder(private val name: String) {
    private var _command: List<String>? = null
    private var _entrypoint: List<String>? = null
    private var _pullPolicy: List<PullPolicy>? = null

    var alias: String? = null

    fun command(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _command = it }

    fun entrypoint(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _entrypoint = it }

    fun pullPolicy(init: ListBuilder<PullPolicy>.() -> Unit): List<PullPolicy> =
        ListBuilder.use(init) { _pullPolicy = it }

    fun build(): Service =
        Service(
            name = this.name,
            alias = this.alias,
            command = this._command,
            entrypoint = this._entrypoint,
            pullPolicy = this._pullPolicy,
        )
}

class ServicesBuilder {
    private var value: MutableList<Service> = mutableListOf()

    operator fun Service.unaryPlus() {
        value.add(this)
    }

    operator fun String.unaryPlus() {
        value.add(Service(name = this))
    }

    fun service(name: String, init: ServiceBuilder.() -> Unit): Service =
        ServiceBuilder(name).apply(init).build()

    fun service(name: String): Service = service(name) {}

    fun build(): List<Service> = value
}

interface IHasServices {
    fun services(init: ServicesBuilder.() -> Unit): List<Service>
}

class HasServices : IHasServices {
    var value: List<Service>? = null

    override fun services(init: ServicesBuilder.() -> Unit): List<Service> =
        ServicesBuilder().apply(init).build().also { value = it }
}
