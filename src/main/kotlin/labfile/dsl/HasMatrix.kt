package labfile.dsl

import labfile.ast.Matrix

interface IHasMatrix {
    fun matrix(init: ListBuilder<Map<String, List<String>>>.() -> Unit): Matrix
}

open class HasMatrix : IHasMatrix {
    var value: Matrix? = null

    override fun matrix(init: ListBuilder<Map<String, List<String>>>.() -> Unit): Matrix {
        ListBuilder.use(init) { value = Matrix(it) }
        return value ?: Matrix()
    }
}
