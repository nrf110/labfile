package labfile.dsl

import labfile.ast.Kubernetes
import labfile.ast.LegacyControl

class LegacyControlBuilder {
    private var _changes: List<String>? = null
    private var _refs: List<String>? = null
    private var _variables: List<String>? = null
    var kubernetes: Kubernetes? = null

    fun changes(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _changes = it }

    fun refs(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _refs = it }

    fun variables(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _variables = it }

    fun build(): LegacyControl =
        LegacyControl(
            refs = _refs,
            variables = _variables,
            changes = _changes,
            kubernetes = kubernetes,
        )
}

interface IHasOnly {
    fun only(init: LegacyControlBuilder.() -> Unit): LegacyControl
}

class HasOnly : IHasOnly {
    var value: LegacyControl? = null

    override fun only(init: LegacyControlBuilder.() -> Unit): LegacyControl =
        LegacyControlBuilder().apply(init).build().also { value = it }
}

interface IHasExcept {
    fun except(init: LegacyControlBuilder.() -> Unit): LegacyControl
}

class HasExcept : IHasExcept {
    var value: LegacyControl? = null

    override fun except(init: LegacyControlBuilder.() -> Unit): LegacyControl =
        LegacyControlBuilder().apply(init).build().also { value = it }
}
