package labfile.dsl

import labfile.ast.IdToken

class IdTokenBuilder {
    private var aud: List<String>? = null

    fun aud(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder<String>()
            .apply(init)
            .build()
            .ifEmpty { error("aud must have at least one entry") }
            .also { aud = it }

    fun build(): IdToken = aud?.let(::IdToken) ?: error("aud is required for id tokens")
}

class IdTokensBuilder {
    private var idTokens: MutableMap<String, IdToken> = mutableMapOf()

    infix fun String.to(token: IdToken): Pair<String, IdToken> =
        Pair(this, token).also { idTokens[it.first] = it.second }

    fun idToken(init: IdTokenBuilder.() -> Unit): IdToken = IdTokenBuilder().apply(init).build()

    fun build(): Map<String, IdToken> = idTokens
}

interface IHasIdTokens {
    fun idTokens(init: IdTokensBuilder.() -> Unit): Map<String, IdToken>
}

class HasIdTokens : IHasIdTokens {
    var value: Map<String, IdToken>? = null

    override fun idTokens(init: IdTokensBuilder.() -> Unit): Map<String, IdToken> =
        IdTokensBuilder().apply(init).build().also { value = it }
}
