package labfile.dsl

import labfile.ast.Include
import labfile.ast.Trigger

class ForwardBuilder {
    var pipelineVariables: Boolean? = null
    var yamlVariables: Boolean? = null

    fun build(): Trigger.Forward = Trigger.Forward(pipelineVariables, yamlVariables)
}

class TriggerBuilder {
    private var _forward: Trigger.Forward? = null

    var branch: String? = null
    var include: List<Include>? = null
    var project: String? = null
    var strategy: Trigger.Strategy? = null

    fun forward(init: ForwardBuilder.() -> Unit): Trigger.Forward =
        ForwardBuilder().apply(init).build().also { _forward = it }

    fun build(): Trigger =
        Trigger(
            branch = branch,
            forward = _forward,
            include = include,
            project = project,
            strategy = strategy,
        )
}

interface IHasTrigger {
    fun trigger(init: TriggerBuilder.() -> Unit): Trigger
}

class HasTrigger : IHasTrigger {
    var value: Trigger? = null

    override fun trigger(init: TriggerBuilder.() -> Unit): Trigger =
        TriggerBuilder().apply(init).build().also { value = it }
}
