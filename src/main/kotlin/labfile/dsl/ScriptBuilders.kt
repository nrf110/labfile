package labfile.dsl

import java.lang.IllegalStateException
import labfile.ast.Either
import labfile.ast.ReferenceTag
import labfile.ast.Script
import org.intellij.lang.annotations.Language

open class ScriptProvider {
    var value: List<Either<Script, ReferenceTag>>? = null

    protected fun list(init: ScriptListBuilder.() -> Unit): List<Either<Script, ReferenceTag>> =
        ScriptListBuilder()
            .apply(init)
            .build()
            .ifEmpty { throw IllegalStateException("script block must have at least one line") }
            .also { value = it }
}

class ScriptListBuilder : ListBuilder<Either<Script, ReferenceTag>>() {
    fun shell(@Language("bash") content: String): Either<Script, ReferenceTag> =
        Either.left(Script.Shell(content))

    fun reference(block: String, field: String): Either<Script, ReferenceTag> =
        Either.right(ReferenceTag(block, field))
}

interface IHasAfterScript {
    fun afterScript(init: ScriptListBuilder.() -> Unit): List<Either<Script, ReferenceTag>>
}

class HasAfterScript : ScriptProvider(), IHasAfterScript {
    override fun afterScript(
        init: ScriptListBuilder.() -> Unit
    ): List<Either<Script, ReferenceTag>> = list(init)
}

interface IHasBeforeScript {
    fun beforeScript(init: ScriptListBuilder.() -> Unit): List<Either<Script, ReferenceTag>>
}

class HasBeforeScript : ScriptProvider(), IHasBeforeScript {
    override fun beforeScript(
        init: ScriptListBuilder.() -> Unit
    ): List<Either<Script, ReferenceTag>> = list(init)
}

interface IHasScript {
    fun script(init: ScriptListBuilder.() -> Unit): List<Either<Script, ReferenceTag>>
}

class HasScript : ScriptProvider(), IHasScript {
    override fun script(init: ScriptListBuilder.() -> Unit): List<Either<Script, ReferenceTag>> =
        list(init)
}
