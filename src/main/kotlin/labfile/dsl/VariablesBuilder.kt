package labfile.dsl

import labfile.ast.Variable

class VariableBuilder {
    var description: String? = null
    var value: String? = null

    fun build(): Variable = Variable(description = description, value = value)
}

class VariablesBuilder {
    private var _variables: MutableMap<String, Variable> = mutableMapOf()

    infix fun String.to(v: String): Pair<String, Variable> = this to Variable(value = v)

    infix fun String.to(v: Boolean): Pair<String, Variable> = this to v.toString()

    infix fun String.to(v: Number): Pair<String, Variable> = this to v.toString()

    infix fun String.to(v: Variable): Pair<String, Variable> =
        Pair(this, v).also { _variables[it.first] = it.second }

    fun variable(init: VariableBuilder.() -> Unit): Variable = VariableBuilder().apply(init).build()

    fun build(): Map<String, Variable> = _variables
}

interface IHasVariables {
    fun variables(init: VariablesBuilder.() -> Unit): Map<String, Variable>
}

class HasVariables : IHasVariables {
    var value: Map<String, Variable>? = null

    override fun variables(init: VariablesBuilder.() -> Unit): Map<String, Variable> =
        VariablesBuilder().apply(init).build().also { value = it }
}
