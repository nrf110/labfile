package labfile.dsl

import labfile.ast.Retry

class RetryBuilder(private val max: Int) {
    private var _when: List<Retry.When>? = null

    @Suppress("FunctionNaming")
    fun `when`(init: ListBuilder<Retry.When>.() -> Unit): List<Retry.When> =
        ListBuilder.use(init) { _when = it }

    fun build(): Retry =
        Retry(
            max = this.max,
            `when` = this._when,
        )
}

interface IHasRetry {
    fun retry(max: Int, init: RetryBuilder.() -> Unit): Retry

    fun retry(max: Int): Retry = retry(max) {}
}

class HasRetry : IHasRetry {
    var value: Retry? = null

    override fun retry(max: Int, init: RetryBuilder.() -> Unit): Retry =
        RetryBuilder(max).apply(init).build().also { value = it }
}
