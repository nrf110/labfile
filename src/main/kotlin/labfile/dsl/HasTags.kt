package labfile.dsl

interface IHasTags {
    fun tags(init: ListBuilder<String>.() -> Unit): List<String>
}

class HasTags : IHasTags {
    var value: List<String>? = null

    override fun tags(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { value = it }
}
