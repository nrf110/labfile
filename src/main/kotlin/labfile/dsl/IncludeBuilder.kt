package labfile.dsl

import labfile.ast.Include

class ProjectIncludeBuilder(val name: String) {
    private var file: List<String>? = null
    var ref: String? = null

    fun file(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { file = it }

    fun build(): Include.Project =
        Include.Project(file = requireNotNull(this.file), project = name, ref = this.ref)
}

class IncludeBuilder {
    private var value: MutableList<Include> = mutableListOf()

    fun artifact(path: String, job: String): Include = Include.Artifact(path, job)

    fun project(name: String, init: ProjectIncludeBuilder.() -> Unit): Include =
        ProjectIncludeBuilder(name).apply(init).build()

    fun local(path: String): Include = Include.Local(path)

    fun remote(url: String): Include = Include.Remote(url)

    fun template(path: String): Include = Include.Template(path)

    operator fun Include.unaryPlus() {
        value.add(this)
    }

    fun build(): List<Include> = value
}

interface IHasInclude {
    fun include(init: IncludeBuilder.() -> Unit): List<Include>
}

class HasInclude : IHasInclude {
    var value: List<Include>? = null

    override fun include(init: IncludeBuilder.() -> Unit): List<Include> =
        IncludeBuilder().apply(init).build().also { value = it }
}
