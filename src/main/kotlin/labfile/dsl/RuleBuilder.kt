package labfile.dsl

import labfile.ast.Either
import labfile.ast.ReferenceTag
import labfile.ast.Rule
import labfile.ast.When

class RuleBuilder(
    private val allowFailure: HasAllowFailure = HasAllowFailure(),
    private val variables: HasVariables = HasVariables(),
    private val needs: HasNeeds = HasNeeds()
) : IHasAllowFailure by allowFailure, IHasVariables by variables, IHasNeeds by needs {

    private var _if: String? = null
    private var _changes: List<String>? = null
    private var _exists: List<String>? = null

    var startIn: String? = null
    @Suppress("VariableNaming") var `when`: When? = null

    fun changes(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _changes = it }

    fun exists(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _exists = it }

    @Suppress("FunctionNaming")
    fun `if`(condition: String) {
        _if = condition
    }

    fun build(): Rule =
        Rule(
            allowFailure = this.allowFailure.value,
            changes = this._changes,
            exists = this._exists,
            `if` = this._if,
            needs = this.needs.value,
            startIn = this.startIn,
            variables = this.variables.value,
            `when` = this.`when`,
        )
}

class RulesBuilder {
    private var value: MutableList<Either<Rule, ReferenceTag>> = mutableListOf()

    fun rule(init: RuleBuilder.() -> Unit): Rule = RuleBuilder().apply(init).build()

    fun reference(block: String, property: String): ReferenceTag = ReferenceTag(block, property)

    operator fun Rule.unaryPlus() {
        value.add(Either.left(this))
    }

    operator fun ReferenceTag.unaryPlus() {
        value.add(Either.right(this))
    }

    fun build(): List<Either<Rule, ReferenceTag>> = value
}

interface IHasRules {
    fun rules(init: RulesBuilder.() -> Unit): List<Either<Rule, ReferenceTag>>
}

class HasRules : IHasRules {
    var value: List<Either<Rule, ReferenceTag>>? = null

    override fun rules(init: RulesBuilder.() -> Unit): List<Either<Rule, ReferenceTag>> =
        RulesBuilder().apply(init).build().also { value = it }
}
