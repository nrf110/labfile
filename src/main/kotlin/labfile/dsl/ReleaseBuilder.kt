package labfile.dsl

import labfile.ast.Assets
import labfile.ast.Link
import labfile.ast.Release

class LinkBuilder {
    var filepath: String? = null
    var linkType: String? = null
    var name: String? = null
    var url: String? = null

    fun build(): Link =
        Link(
            filepath = filepath,
            linkType = linkType,
            name = name,
            url = url,
        )
}

class AssetsBuilder {
    private var _links: List<Link> = emptyList()

    fun link(init: LinkBuilder.() -> Unit): Link = LinkBuilder().apply(init).build()

    fun links(init: ListBuilder<Link>.() -> Unit): List<Link> =
        ListBuilder.use(init) { _links = it }

    fun build(): Assets = Assets(links = _links)
}

class ReleaseBuilder(private val tagName: String) {
    private var _assets: Assets? = null
    private var _milestones: List<String>? = null

    var name: String? = null
    var description: String? = null
    var ref: String? = null
    var releasedAt: String? = null
    var tagMessage: String? = null

    fun assets(init: AssetsBuilder.() -> Unit): Assets =
        AssetsBuilder().apply(init).build().also { _assets = it }

    fun milestones(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _milestones = it }

    fun build(): Release =
        Release(
            assets = this._assets,
            description = this.description,
            milestones = this._milestones,
            name = this.name,
            ref = this.ref,
            releasedAt = this.releasedAt,
            tagMessage = this.tagMessage,
            tagName = this.tagName,
        )
}

interface IHasRelease {
    fun release(tagName: String, init: ReleaseBuilder.() -> Unit): Release

    fun release(tagName: String): Release = release(tagName) {}
}

class HasRelease : IHasRelease {
    var value: Release? = null

    override fun release(tagName: String, init: ReleaseBuilder.() -> Unit): Release =
        ReleaseBuilder(tagName).apply(init).build().also { value = it }
}
