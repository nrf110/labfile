package labfile.dsl

import labfile.ast.Environment

class EnvironmentBuilder(private val name: String) {
    private var _kubernetes: Environment.Kubernetes? = null

    var action: Environment.Action? = null
    var autoStopIn: String? = null
    var deploymentTier: Environment.DeploymentTier? = null
    var onStop: String? = null
    var url: String? = null

    fun kubernetes(init: KubernetesBuilder.() -> Unit): Environment.Kubernetes =
        KubernetesBuilder().apply(init).build().also { _kubernetes = it }

    fun build(): Environment =
        Environment(
            action = this.action,
            autoStopIn = this.autoStopIn,
            deploymentTier = this.deploymentTier,
            kubernetes = this._kubernetes,
            name = this.name,
            onStop = this.onStop,
            url = this.url,
        )

    class KubernetesBuilder {
        var namespace: String? = null

        fun build(): Environment.Kubernetes =
            Environment.Kubernetes(
                namespace = namespace,
            )
    }
}

interface IHasEnvironment {
    fun environment(name: String, init: EnvironmentBuilder.() -> Unit): Environment

    fun environment(name: String): Environment = environment(name) {}
}

class HasEnvironment : IHasEnvironment {
    var value: Environment? = null

    override fun environment(name: String, init: EnvironmentBuilder.() -> Unit): Environment =
        EnvironmentBuilder(name).apply(init).build().also { value = it }
}
