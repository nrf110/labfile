package labfile.dsl

import labfile.ast.Image
import labfile.ast.PullPolicy

class ImageBuilder(private val name: String) {
    private var _entrypoint: List<String>? = null
    private var _pullPolicy: List<PullPolicy>? = null

    fun entrypoint(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _entrypoint = it }

    fun pullPolicy(init: ListBuilder<PullPolicy>.() -> Unit): List<PullPolicy> =
        ListBuilder.use(init) { _pullPolicy = it }

    fun build(): Image =
        Image(
            name = this.name,
            entrypoint = this._entrypoint,
            pullPolicy = this._pullPolicy,
        )
}

interface IHasImage {
    fun image(name: String, init: ImageBuilder.() -> Unit): Image

    fun image(name: String): Image = image(name) {}
}

class HasImage : IHasImage {
    var value: Image? = null

    override fun image(name: String, init: ImageBuilder.() -> Unit): Image =
        ImageBuilder(name).apply(init).build().also { value = it }
}
