package labfile.dsl

import java.io.FileWriter
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty
import labfile.ast.GlobalDefaults
import labfile.ast.Job
import labfile.ast.Pipeline
import labfile.ast.Workflow

class PipelineBuilder(
    private val variables: HasVariables = HasVariables(),
    private val include: HasInclude = HasInclude()
) : IHasVariables by variables, IHasInclude by include {
    private var _default: GlobalDefaults? = null
    private val _jobs: MutableMap<String, Job> = mutableMapOf()
    private var _stages: MutableSet<String>? = null
    private var _workflow: Workflow? = null

    fun default(init: GlobalDefaultsBuilder.() -> Unit): GlobalDefaults =
        GlobalDefaultsBuilder().apply(init).build().also { _default = it }

    fun job(name: String, init: JobBuilder.() -> Unit): Job =
        JobBuilder(name).apply(init).build().also { _jobs[name] = it }

    fun pages(init: JobBuilder.() -> Unit): Job =
        JobBuilder(PAGES).apply(init).build().also { _jobs[PAGES] = it }

    fun stage(name: String? = null) =
        object : ReadOnlyProperty<Nothing?, String> {
            override fun getValue(thisRef: Nothing?, property: KProperty<*>): String {
                _stages = _stages ?: mutableSetOf()
                return (name ?: property.name).also { _stages!! += it }
            }
        }

    fun workflow(init: WorkflowBuilder.() -> Unit): Workflow =
        WorkflowBuilder().apply(init).build().also { _workflow = it }

    fun build(): Pipeline =
        Pipeline(
            default = this._default,
            include = this.include.value,
            jobs = this._jobs,
            stages = this._stages,
            variables = this.variables.value,
            workflow = this._workflow,
        )

    companion object {
        private const val PAGES = "pages"
    }
}

fun pipeline(init: PipelineBuilder.() -> Unit): Pipeline =
    pipeline(System.getenv("GITLAB_YAML_OUTPUT") ?: "./labfile.yml", init)

fun pipeline(outputFile: String, init: PipelineBuilder.() -> Unit): Pipeline =
    PipelineBuilder().apply(init).build().also {
        FileWriter(outputFile, Charsets.UTF_8).use { writer ->
            writer.write(it.toYaml().serialize())
        }
    }
