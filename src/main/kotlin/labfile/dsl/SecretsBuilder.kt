package labfile.dsl

import labfile.ast.Either
import labfile.ast.Secret
import labfile.ast.Secrets
import labfile.ast.VaultConfig
import labfile.ast.VaultEngine

class VaultEngineBuilder {
    var name: String? = null
    var path: String? = null

    fun build(): VaultEngine =
        VaultEngine(
            name = name,
            path = path,
        )
}

class VaultSecretBuilder {
    private var _engine: VaultEngine? = null
    var field: String? = null
    var path: String? = null

    fun engine(init: VaultEngineBuilder.() -> Unit): VaultEngine =
        VaultEngineBuilder().apply(init).build().also { _engine = it }

    fun build(): Secret.VaultSecret =
        Secret.VaultSecret(
            Either.right(
                VaultConfig(
                    engine = _engine,
                    field = field,
                    path = path,
                )
            )
        )
}

class SecretsBuilder {
    var values = mutableMapOf<String, Secret>()

    infix fun String.to(value: Secret): Pair<String, Secret> =
        Pair(this, value).also { values[it.first] = it.second }

    fun vault(path: String): Secret.VaultSecret = Secret.VaultSecret(Either.left(path))

    fun vault(init: VaultSecretBuilder.() -> Unit): Secret.VaultSecret =
        VaultSecretBuilder().apply(init).build()

    fun build(): Secrets = Secrets(values)
}

interface IHasSecrets {
    fun secrets(init: SecretsBuilder.() -> Unit): Secrets
}

class HasSecrets : IHasSecrets {
    var value: Secrets? = null

    override fun secrets(init: SecretsBuilder.() -> Unit): Secrets =
        SecretsBuilder().apply(init).build().also { value = it }
}
