package labfile.dsl

import labfile.ast.Job
import labfile.ast.When

class JobBuilder(
    private val name: String,
    private val afterScript: HasAfterScript = HasAfterScript(),
    private val allowFailure: HasAllowFailure = HasAllowFailure(),
    private val artifacts: HasArtifacts = HasArtifacts(),
    private val beforeScript: HasBeforeScript = HasBeforeScript(),
    private val cache: HasCache = HasCache(),
    private val dastConfiguration: HasDastConfiguration = HasDastConfiguration(),
    private val dependencies: HasDependencies = HasDependencies(),
    private val environment: HasEnvironment = HasEnvironment(),
    private val except: HasExcept = HasExcept(),
    private val hooks: HasHooks = HasHooks(),
    private val idTokens: HasIdTokens = HasIdTokens(),
    private val image: HasImage = HasImage(),
    private val include: HasInclude = HasInclude(),
    private val inherit: HasInherit = HasInherit(),
    private val needs: HasNeeds = HasNeeds(),
    private val only: HasOnly = HasOnly(),
    private val parallel: HasParallel = HasParallel(),
    private val release: HasRelease = HasRelease(),
    private val retry: HasRetry = HasRetry(),
    private val rules: HasRules = HasRules(),
    private val script: HasScript = HasScript(),
    private val secrets: HasSecrets = HasSecrets(),
    private val services: HasServices = HasServices(),
    private val tags: HasTags = HasTags(),
    private val trigger: HasTrigger = HasTrigger(),
    private val variables: HasVariables = HasVariables(),
) :
    IHasAfterScript by afterScript,
    IHasAllowFailure by allowFailure,
    IHasArtifacts by artifacts,
    IHasBeforeScript by beforeScript,
    IHasCache by cache,
    IHasDastConfiguration by dastConfiguration,
    IHasDependencies by dependencies,
    IHasEnvironment by environment,
    IHasExcept by except,
    IHasHooks by hooks,
    IHasIdTokens by idTokens,
    IHasImage by image,
    IHasInclude by include,
    IHasInherit by inherit,
    IHasNeeds by needs,
    IHasOnly by only,
    IHasParallel by parallel,
    IHasRelease by release,
    IHasRetry by retry,
    IHasRules by rules,
    IHasScript by script,
    IHasSecrets by secrets,
    IHasTags by tags,
    IHasTrigger by trigger,
    IHasVariables by variables {
    var coverage: String? = null
    var extends: String? = null
    var interruptible: Boolean? = null
    var publish: String? = null
    var resourceGroup: String? = null
    var stage: String? = null
    var startIn: String? = null
    var timeout: String? = null
    @Suppress("VariableNaming") var `when`: When? = null

    fun build(): Job {
        return Job(
            name = this.name,
            afterScript = this.afterScript.value,
            allowFailure = this.allowFailure.value,
            artifacts = this.artifacts.value,
            beforeScript = this.beforeScript.value,
            cache = this.cache.value,
            coverage = this.coverage,
            dastConfiguration = this.dastConfiguration.value,
            dependencies = this.dependencies.value,
            environment = this.environment.value,
            except = this.except.value,
            extends = this.extends,
            hooks = this.hooks.value,
            idTokens = this.idTokens.value,
            image = this.image.value,
            include = this.include.value,
            inherit = this.inherit.value,
            interruptible = this.interruptible,
            needs = this.needs.value,
            only = this.only.value,
            parallel = this.parallel.value,
            publish = this.publish,
            release = this.release.value,
            resourceGroup = this.resourceGroup,
            retry = this.retry.value,
            rules = this.rules.value,
            script = this.script.value,
            secrets = this.secrets.value,
            services = this.services.value,
            stage = this.stage,
            startIn = this.startIn,
            tags = this.tags.value,
            timeout = this.timeout,
            trigger = this.trigger.value,
            `when` = this.`when`,
            variables = this.variables.value,
        )
    }
}
