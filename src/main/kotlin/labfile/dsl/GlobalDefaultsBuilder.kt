package labfile.dsl

import labfile.ast.GlobalDefaults

class GlobalDefaultsBuilder(
    private val afterScript: HasAfterScript = HasAfterScript(),
    private val artifacts: HasArtifacts = HasArtifacts(),
    private val beforeScript: HasBeforeScript = HasBeforeScript(),
    private val cache: HasCache = HasCache(),
    private val image: HasImage = HasImage(),
    private val retry: HasRetry = HasRetry(),
    private val services: HasServices = HasServices(),
    private val tags: HasTags = HasTags(),
) :
    IHasArtifacts by artifacts,
    IHasBeforeScript by beforeScript,
    IHasAfterScript by afterScript,
    IHasImage by image,
    IHasServices by services,
    IHasTags by tags,
    IHasCache by cache,
    IHasRetry by retry {
    var interruptible: Boolean? = null
    var timeout: String? = null

    fun build(): GlobalDefaults =
        GlobalDefaults(
            afterScript = this.afterScript.value,
            artifacts = this.artifacts.value,
            beforeScript = this.beforeScript.value,
            cache = this.cache.value,
            image = this.image.value,
            interruptible = this.interruptible,
            retry = this.retry.value,
            services = this.services.value,
            tags = this.tags.value,
            timeout = this.timeout,
        )
}
