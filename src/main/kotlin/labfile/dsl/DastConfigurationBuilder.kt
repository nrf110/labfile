package labfile.dsl

import labfile.ast.DastConfiguration

class DastConfigurationBuilder {
    var scannerProfile: String? = null
    var siteProfile: String? = null

    fun build(): DastConfiguration =
        DastConfiguration(
            scannerProfile = scannerProfile,
            siteProfile = siteProfile,
        )
}

interface IHasDastConfiguration {
    fun dastConfiguration(init: DastConfigurationBuilder.() -> Unit): DastConfiguration
}

class HasDastConfiguration : IHasDastConfiguration {
    var value: DastConfiguration? = null

    override fun dastConfiguration(init: DastConfigurationBuilder.() -> Unit): DastConfiguration =
        DastConfigurationBuilder().apply(init).build().also { value = it }
}
