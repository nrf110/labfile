package labfile.dsl

import labfile.ast.Either
import labfile.ast.Hooks
import labfile.ast.ReferenceTag
import labfile.ast.Script

class HooksBuilder {
    private var value: List<Either<Script, ReferenceTag>>? = null

    fun preGetSourcesScript(
        init: ScriptListBuilder.() -> Unit
    ): List<Either<Script, ReferenceTag>> =
        ScriptListBuilder()
            .apply(init)
            .build()
            .ifEmpty { error("script block must have at least one line") }
            .also { value = it }

    fun build(): Hooks =
        value?.let(::Hooks) ?: error("preGetSourcesScript must be set in hooks clause")
}

interface IHasHooks {
    fun hooks(init: HooksBuilder.() -> Unit): Hooks
}

class HasHooks : IHasHooks {
    var value: Hooks? = null

    override fun hooks(init: HooksBuilder.() -> Unit): Hooks {
        return HooksBuilder().apply(init).build().also { value = it }
    }
}
