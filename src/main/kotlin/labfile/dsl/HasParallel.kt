package labfile.dsl

import labfile.ast.Either
import labfile.ast.Matrix
import labfile.ast.Parallel

class ParallelBuilder : HasMatrix() {
    fun build(): Parallel = Either.right(value ?: Matrix())
}

interface IHasParallel {
    fun parallel(count: Int): Parallel

    fun parallel(init: ParallelBuilder.() -> Unit): Parallel
}

class HasParallel : IHasParallel {
    var value: Parallel? = null

    override fun parallel(count: Int): Parallel =
        Either.left<Int, Matrix>(count).also { value = it }

    override fun parallel(init: ParallelBuilder.() -> Unit): Parallel =
        ParallelBuilder().apply(init).build().also { value = it }
}
