package labfile.dsl

import labfile.ast.Cache
import labfile.ast.CacheKeyFiles
import labfile.ast.Either

class CacheKeyFilesBuilder(private val prefix: String? = null) {
    private var _files: List<String> = emptyList()

    fun files(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _files = it }

    fun build(): CacheKeyFiles {
        require(_files.size in 1..2) { "Cache key must consist of either 1 or 2 files" }
        return CacheKeyFiles(files = this._files, prefix = this.prefix)
    }
}

class CacheBuilder {
    private var _key: Either<String, CacheKeyFiles>? = null
    private var _paths: List<String>? = null

    var policy: Cache.Policy? = null
    var unprotect: Boolean? = null
    var untracked: Boolean? = null
    @Suppress("VariableNaming") var `when`: Cache.When? = null

    fun key(name: String): String = name.also { _key = Either.Left(it) }

    fun key(prefix: String, init: CacheKeyFilesBuilder.() -> Unit): CacheKeyFiles =
        CacheKeyFilesBuilder(prefix).apply(init).build().also { _key = Either.Right(it) }

    fun key(init: CacheKeyFilesBuilder.() -> Unit): CacheKeyFiles =
        CacheKeyFilesBuilder().apply(init).build().also { _key = Either.Right(it) }

    fun paths(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _paths = it }

    fun build(): Cache =
        Cache(
            paths = this._paths,
            key = this._key,
            policy = this.policy,
            unprotect = this.unprotect,
            untracked = this.untracked,
            `when` = this.`when`,
        )
}

interface IHasCache {
    fun cache(init: CacheBuilder.() -> Unit): Cache
}

class HasCache : IHasCache {
    var value: Cache? = null

    override fun cache(init: CacheBuilder.() -> Unit): Cache =
        CacheBuilder().apply(init).build().also { value = it }
}
