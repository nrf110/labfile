package labfile.dsl

import labfile.ast.Either
import labfile.ast.Inherit

class InheritBuilder {
    private var _default: Either<Boolean, List<Inherit.Default>>? = null
    private var _variables: Either<Boolean, List<String>>? = null

    fun default(allow: Boolean): Either<Boolean, List<Inherit.Default>> =
        Either.left<Boolean, List<Inherit.Default>>(allow).also { _default = it }

    fun default(
        init: ListBuilder<Inherit.Default>.() -> Unit
    ): Either<Boolean, List<Inherit.Default>> =
        Either.right<Boolean, List<Inherit.Default>>(
                ListBuilder<Inherit.Default>().apply(init).build()
            )
            .also { _default = it }

    fun variables(allow: Boolean): Either<Boolean, List<String>> =
        Either.left<Boolean, List<String>>(allow).also { _variables = it }

    fun variables(init: ListBuilder<String>.() -> Unit): Either<Boolean, List<String>> =
        Either.right<Boolean, List<String>>(ListBuilder<String>().apply(init).build()).also {
            _variables = it
        }

    fun build(): Inherit =
        Inherit(
            default = _default,
            variables = _variables,
        )
}

interface IHasInherit {
    fun inherit(init: InheritBuilder.() -> Unit): Inherit
}

class HasInherit : IHasInherit {
    var value: Inherit? = null

    override fun inherit(init: InheritBuilder.() -> Unit): Inherit =
        InheritBuilder().apply(init).build().also { value = it }
}
