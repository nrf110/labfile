package labfile.dsl

interface IHasDependencies {
    fun dependencies(init: ListBuilder<String>.() -> Unit): List<String>
}

class HasDependencies : IHasDependencies {
    var value: List<String>? = null

    override fun dependencies(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { value = it }
}
