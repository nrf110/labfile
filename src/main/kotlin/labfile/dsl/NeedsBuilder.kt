package labfile.dsl

import labfile.ast.Matrix
import labfile.ast.Need

class NeedBuilder {
    var artifacts: Boolean? = null
    var job: String? = null
    var optional: Boolean? = null
    var parallel: Matrix? = null
    var pipeline: String? = null
    var project: String? = null
    var ref: String? = null

    fun build(): Need =
        Need(
            artifacts = artifacts,
            job = job,
            optional = optional,
            pipeline = pipeline,
            project = project,
            ref = ref,
        )
}

class NeedsBuilder {
    private var value: MutableList<Need> = mutableListOf()

    operator fun String.unaryPlus() {
        value.add(Need(job = this))
    }

    operator fun Need.unaryPlus() {
        value.add(this)
    }

    fun need(init: NeedBuilder.() -> Unit): Need = NeedBuilder().apply(init).build()

    fun build(): List<Need> = value
}

interface IHasNeeds {
    fun needs(init: NeedsBuilder.() -> Unit): List<Need>
}

class HasNeeds : IHasNeeds {
    var value: List<Need>? = null

    override fun needs(init: NeedsBuilder.() -> Unit): List<Need> =
        NeedsBuilder().apply(init).build().also { value = it }
}
