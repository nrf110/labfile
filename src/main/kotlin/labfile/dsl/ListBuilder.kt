package labfile.dsl

open class ListBuilder<T> {
    private val value: MutableList<T> = mutableListOf()

    operator fun T.unaryPlus() {
        value.add(this)
    }

    fun build(): List<T> = value

    companion object {
        fun <R> use(init: ListBuilder<R>.() -> Unit, effect: (List<R>) -> Unit): List<R> =
            ListBuilder<R>().apply(init).build().also(effect)
    }
}
