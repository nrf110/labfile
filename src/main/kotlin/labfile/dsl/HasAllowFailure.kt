package labfile.dsl

import labfile.ast.AllowFailure
import labfile.ast.Either
import labfile.ast.ExitCodes

class AllowFailureBuilder {
    var value: List<Int>? = null

    fun exitCodes(init: ListBuilder<Int>.() -> Unit): List<Int> =
        ListBuilder.use(init) { value = it }

    fun build(): ExitCodes {
        requireNotNull(value)
        return ExitCodes(exitCodes = value!!)
    }
}

interface IHasAllowFailure {
    fun allowFailure(flag: Boolean): AllowFailure

    fun allowFailure(init: AllowFailureBuilder.() -> Unit): AllowFailure
}

class HasAllowFailure : IHasAllowFailure {
    var value: AllowFailure? = null

    override fun allowFailure(flag: Boolean): AllowFailure =
        Either.Left<Boolean, ExitCodes>(flag).also { value = it }

    override fun allowFailure(init: AllowFailureBuilder.() -> Unit): AllowFailure =
        Either.Right<Boolean, ExitCodes>(AllowFailureBuilder().apply(init).build()).also {
            value = it
        }
}
