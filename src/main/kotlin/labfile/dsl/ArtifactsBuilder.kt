package labfile.dsl

import labfile.ast.Artifacts
import labfile.ast.CoverageFormat
import labfile.ast.CoverageReport
import labfile.ast.Reports
import labfile.ast.When

class CoverageReportBuilder {
    var coverageFormat: CoverageFormat? = null
    var path: String? = null

    fun build(): CoverageReport {
        requireNotNull(coverageFormat)
        requireNotNull(path)

        return CoverageReport(coverageFormat = coverageFormat!!, path = path!!)
    }
}

class ReportsBuilder {
    private var _junit: List<String>? = null
    private var coverageReport: CoverageReport? = null

    var apiFuzzing: String? = null
    var codequality: String? = null
    var containerScanning: String? = null
    var coverageFuzzing: String? = null
    var dast: String? = null
    var dependencyScanning: String? = null
    var dotenv: String? = null
    var licenseScanning: String? = null
    var loadPerformance: String? = null
    var metrics: String? = null
    var performance: String? = null
    var requirements: String? = null
    var sast: String? = null
    var secretDetection: String? = null
    var terraform: String? = null

    fun junit(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _junit = it }

    fun coverageReport(init: CoverageReportBuilder.() -> Unit): CoverageReport =
        CoverageReportBuilder().apply(init).build().also { coverageReport = it }

    fun build(): Reports =
        Reports(
            apiFuzzing = this.apiFuzzing,
            codequality = this.codequality,
            containerScanning = this.containerScanning,
            coverageFuzzing = this.coverageFuzzing,
            coverageReport = this.coverageReport,
            dast = this.dast,
            dependencyScanning = this.dependencyScanning,
            dotenv = this.dotenv,
            junit = this._junit,
            licenseScanning = this.licenseScanning,
            loadPerformance = this.loadPerformance,
            metrics = this.metrics,
            performance = this.performance,
            requirements = this.requirements,
            sast = this.sast,
            secretDetection = this.secretDetection,
            terraform = this.terraform,
        )
}

interface IHasArtifacts {
    fun artifacts(init: ArtifactsBuilder.() -> Unit): Artifacts
}

class HasArtifacts : IHasArtifacts {
    var value: Artifacts? = null

    override fun artifacts(init: ArtifactsBuilder.() -> Unit): Artifacts =
        ArtifactsBuilder().apply(init).build().also { value = it }
}

class ArtifactsBuilder {
    private var _exclude: List<String>? = null
    private var _paths: List<String>? = null
    private var _report: Reports? = null

    var expireIn: String? = null
    var exposeAs: String? = null
    var name: String? = null
    @Suppress("VariableNaming") var `public`: Boolean? = null
    var untracked: Boolean? = null
    @Suppress("VariableNaming") var `when`: When? = null

    fun exclude(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _exclude = it }

    fun paths(init: ListBuilder<String>.() -> Unit): List<String> =
        ListBuilder.use(init) { _paths = it }

    fun reports(init: ReportsBuilder.() -> Unit): Reports =
        ReportsBuilder().apply(init).build().also { _report = it }

    fun build(): Artifacts =
        Artifacts(
            exclude = this._exclude,
            expireIn = this.expireIn,
            exposeAs = this.exposeAs,
            name = this.name,
            paths = this._paths,
            `public` = this.`public`,
            reports = this._report,
            untracked = this.untracked,
            `when` = this.`when`,
        )
}
