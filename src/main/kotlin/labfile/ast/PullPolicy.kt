package labfile.ast

enum class PullPolicy {
    ALWAYS,
    `IF-NOT-PRESENT`,
    NEVER,
}
