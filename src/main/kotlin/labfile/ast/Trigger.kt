package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class Trigger(
    val branch: String? = null,
    val forward: Forward? = null,
    val include: List<Include>? = null,
    val project: String? = null,
    val strategy: Strategy? = null,
) : YamlNode {
    enum class Strategy {
        DEPEND,
    }

    data class Forward(
        val pipelineVariables: Boolean? = null,
        val yamlVariables: Boolean? = null,
    ) : YamlNode {
        override fun toYaml(): Yaml = buildYaml {
            put("pipeline_variables", pipelineVariables?.toYaml())
            put("yaml_variables", yamlVariables?.toYaml())
        }
    }

    override fun toYaml(): Yaml = buildYaml {
        put("branch", branch?.toYaml())
        put("forward", forward?.toYaml())
        put("include", include?.toYaml())
        put("project", project?.toYaml())
        put("strategy", strategy?.toYaml())
    }
}
