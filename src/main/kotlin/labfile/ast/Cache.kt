package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class CacheKeyFiles(
    val files: List<String>,
    val prefix: String? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("files", files.toYaml())
        put("prefix", prefix?.toYaml())
    }
}

data class Cache(
    val fallbackKeys: List<String>? = null,
    val key: Either<String, CacheKeyFiles>? = null,
    val paths: List<String>? = null,
    val policy: Policy? = null,
    val unprotect: Boolean? = null,
    val untracked: Boolean? = null,
    val `when`: When? = null,
) : YamlNode {

    enum class When {
        /** (default): Save the cache only when the job succeeds. */
        ON_SUCCESS,

        /** Save the cache only when the job fails. */
        ON_FAILURE,

        /** Always save the cache. */
        ALWAYS,
    }

    enum class Policy {
        PULL,
        PUSH,
    }

    override fun toYaml(): Yaml = buildYaml {
        put("fallback_keys", fallbackKeys?.toYaml())
        put("key", key?.fold(String::toYaml, CacheKeyFiles::toYaml))
        put("paths", paths?.toYaml())
        put("policy", policy?.toYaml())
        put("unprotect", unprotect?.toYaml())
        put("untracked", untracked?.toYaml())
        put("when", `when`?.toYaml())
    }
}
