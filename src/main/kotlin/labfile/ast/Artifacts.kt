package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

enum class CoverageFormat {
    CLOVER,
    COBERTURA,
    HTML,
    `JSON-SUMMARY`,
    JSON,
    LCOV,
    LCOVONLY,
    NONE,
    TEAMCITY,
    `TEXT-LCOV`,
    `TEXT-SUMMARY`,
    TEXT,
}

data class CoverageReport(val coverageFormat: CoverageFormat, val path: String) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("coverage_format", coverageFormat.toYaml())
        put("path", path.toYaml())
    }
}

data class Reports(
    val apiFuzzing: String? = null,
    val codequality: String? = null,
    val containerScanning: String? = null,
    val coverageFuzzing: String? = null,
    val coverageReport: CoverageReport? = null,
    val dast: String? = null,
    val dependencyScanning: String? = null,
    val dotenv: String? = null,
    val junit: List<String>? = null,
    val licenseScanning: String? = null,
    val loadPerformance: String? = null,
    val metrics: String? = null,
    val performance: String? = null,
    val requirements: String? = null,
    val sast: String? = null,
    val secretDetection: String? = null,
    val terraform: String? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("api_fuzzing", apiFuzzing?.toYaml())
        put("codequality", codequality?.toYaml())
        put("container_scanning", containerScanning?.toYaml())
        put("coverage_fuzzing", coverageFuzzing?.toYaml())
        put("coverage_report", coverageReport?.toYaml())
        put("dast", dast?.toYaml())
        put("dependency_scanning", dependencyScanning?.toYaml())
        put("dotenv", dotenv?.toYaml())
        put("junit", junit?.toYaml())
        put("license_scanning", licenseScanning?.toYaml())
        put("load_performance", loadPerformance?.toYaml())
        put("metrics", metrics?.toYaml())
        put("performance", performance?.toYaml())
        put("requirements", requirements?.toYaml())
        put("sast", sast?.toYaml())
        put("secret_detection", secretDetection?.toYaml())
        put("terraform", terraform?.toYaml())
    }
}

data class Artifacts(
    val exclude: List<String>? = null,
    val expireIn: String? = null,
    val exposeAs: String? = null,
    val name: String? = null,
    val paths: List<String>? = null,
    val `public`: Boolean? = null,
    val reports: Reports? = null,
    val untracked: Boolean? = null,
    val `when`: When? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("exclude", exclude?.toYaml())
        put("expire_in", expireIn?.toYaml())
        put("expose_as", exposeAs?.toYaml())
        put("name", name?.toYaml())
        put("paths", paths?.toYaml())
        put("public", public?.toYaml())
        put("reports", reports?.toYaml())
        put("untracked", untracked?.toYaml())
        put("when", `when`?.toYaml())
    }
}
