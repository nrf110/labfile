package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

typealias Parallel = Either<Int, Matrix>

data class Matrix(
    val matrix: List<Map<String, List<String>>>? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put(
            "matrix",
            matrix
                ?.map {
                    it.asSequence()
                        .associate { (key, value) -> key.toYaml() to value.toYaml() }
                        .toYaml()
                }
                ?.toYaml()
        )
    }
}
