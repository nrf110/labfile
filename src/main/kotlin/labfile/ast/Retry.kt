package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class Retry(
    val max: Int? = null,
    val `when`: List<When>? = null,
) : YamlNode {
    enum class When {
        /** Retry on any failure (default). */
        ALWAYS,

        /** Retry when the failure reason is unknown. */
        UNKNOWN_FAILURE,

        /** Retry when the script failed. */
        SCRIPT_FAILURE,

        /** Retry on API failure. */
        API_FAILURE,

        /** Retry when the job got stuck or timed out. */
        STUCK_OR_TIMEOUT_FAILURE,

        /** Retry if there is a runner system failure (for example, job setup failed). */
        RUNNER_SYSTEM_FAILURE,

        /** Retry if a dependency is missing. */
        MISSING_DEPENDENCY_FAILURE,

        /** Retry if the runner is unsupported. */
        RUNNER_UNSUPPORTED,

        /** Retry if a delayed job could not be executed. */
        STALE_SCHEDULE,

        /** Retry if the script exceeded the maximum execution time set for the job. */
        JOB_EXECUTION_TIMEOUT,

        /** Retry if the job is archived and can’t be run. */
        ARCHIVED_FAILURE,

        /** Retry if the job failed to complete prerequisite tasks. */
        UNMET_PREREQUISITES,

        /** Retry if the scheduler failed to assign the job to a runner. */
        SCHEDULER_FAILURE,

        /** Retry if there is a structural integrity problem detected. */
        DATA_INTEGRITY_FAILURE,
    }

    override fun toYaml(): Yaml = buildYaml {
        put("max", max?.toYaml())
        put("when", `when`?.toYaml())
    }
}
