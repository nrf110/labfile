package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class Environment(
    val action: Action? = null,
    val autoStopIn: String? = null,
    val deploymentTier: DeploymentTier? = null,
    val kubernetes: Kubernetes? = null,
    val name: String,
    val onStop: String? = null,
    val url: String? = null,
) : YamlNode {

    enum class Action {
        /**
         * Default value. Indicates that job starts the environment. The deployment is created after
         * the job starts.
         */
        START,

        /** Indicates that job is only preparing the environment. Does not affect deployments. */
        STOP,

        /** Indicates that job stops deployment. */
        PREPARE
    }

    enum class DeploymentTier {
        PRODUCTION,
        STAGING,
        TESTING,
        DEVELOPMENT,
        OTHER
    }

    data class Kubernetes(
        val namespace: String? = null,
    ) : YamlNode {
        override fun toYaml(): Yaml = buildYaml { put("namespace", namespace?.toYaml()) }
    }

    override fun toYaml(): Yaml = buildYaml {
        put("action", action?.toYaml())
        put("auto_stop_in", autoStopIn?.toYaml())
        put("deployment_tier", deploymentTier?.toYaml())
        put("kubernetes", kubernetes?.toYaml())
        put("name", name.toYaml())
        put("on_stop", onStop?.toYaml())
        put("url", url?.toYaml())
    }
}
