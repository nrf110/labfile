package labfile.ast

enum class When {
    /**
     * Execute job only when all jobs in earlier stages succeed, or are considered successful
     * because they have `allowFailure: true`.
     */
    ON_SUCCESS,

    /** Execute job only when at least one job in an earlier stage failss */
    ON_FAILURE,

    /** Execute job regardless of the status of jobs in earlier stages. */
    ALWAYS,

    /** Execute job manually. */
    MANUAL,

    /** Delay the execution of a job for a specified duration. */
    DELAYED,

    /** With `rules`, don't run job. With `workflow:rules`, don't run pipeline. */
    NEVER,
}
