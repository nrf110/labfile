package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.toYaml

sealed class Script : YamlNode {
    abstract val content: String

    data class Shell(override val content: String) : Script() {
        override fun toYaml(): Yaml = content.toYaml()
    }
}
