package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class Link(
    val filepath: String? = null,
    val linkType: String? = null,
    val name: String? = null,
    val url: String? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("filepath", filepath?.toYaml())
        put("link_type", linkType?.toYaml())
        put("name", name?.toYaml())
        put("url", url?.toYaml())
    }
}

data class Assets(val links: List<Link>? = null) : YamlNode {
    override fun toYaml(): Yaml = buildYaml { put("links", links?.toYaml()) }
}

data class Release(
    val assets: Assets? = null,
    val description: String? = null,
    val milestones: List<String>? = null,
    val name: String? = null,
    val ref: String? = null,
    val releasedAt: String? = null,
    val tagMessage: String? = null,
    val tagName: String? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("assets", assets?.toYaml())
        put("description", description?.toYaml())
        put("milestones", milestones?.toYaml())
        put("name", name?.toYaml())
        put("ref", ref?.toYaml())
        put("released_at", releasedAt?.toYaml())
        put("tag_message", tagMessage?.toYaml())
        put("tag_name", tagName?.toYaml())
    }
}
