package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

private val numeric = "^\\d+(\\.\\d+)?$".toRegex()

data class Variable(val description: String? = null, val value: String? = null) : YamlNode {
    init {
        requireNotNull(value ?: description) { "Variable must have either value or description" }
    }

    override fun toYaml(): Yaml {
        val formattedValue =
            value
                ?.let { if (it in listOf("true", "false") || numeric.matches(it)) "'$it'" else it }
                ?.toYaml()

        return description?.let { desc ->
            buildYaml {
                put("description", desc.toYaml())
                put("value", formattedValue)
            }
        }
            ?: formattedValue?.toYaml() ?: Yaml.NullNode
    }
}
