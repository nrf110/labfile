package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.toYaml

data class Hooks(val preGetSourcesScript: List<Either<Script, ReferenceTag>>) : YamlNode {
    override fun toYaml(): Yaml =
        buildYaml {
                put(
                    "pre_get_sources_script".toYaml(),
                    preGetSourcesScript
                        .map { it.fold(Script::toYaml, ReferenceTag::toYaml) }
                        .toYaml()
                )
            }
            .toYaml()
}
