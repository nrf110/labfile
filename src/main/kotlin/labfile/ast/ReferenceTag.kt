package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode

data class ReferenceTag(val block: String, val field: String) : YamlNode {
    override fun toYaml(): Yaml = Yaml.ScalarNode.StringNode("!reference [$block, $field]")
}
