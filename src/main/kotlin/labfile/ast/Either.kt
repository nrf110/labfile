package labfile.ast

sealed class Either<L : Any, R : Any> {
    data class Left<L : Any, R : Any>(val value: L) : Either<L, R>() {
        override fun left() = value

        override fun right() = error("Called right() on Either.Left")
    }

    data class Right<L : Any, R : Any>(val value: R) : Either<L, R>() {
        override fun left() = error("Called left() on Either.Right")

        override fun right() = value
    }

    fun <T> fold(mapLeft: (L) -> T, mapRight: (R) -> T): T =
        when (this) {
            is Left -> mapLeft(value)
            is Right -> mapRight(value)
        }

    abstract fun left(): L

    abstract fun right(): R

    companion object {
        fun <L : Any, R : Any> left(value: L): Left<L, R> = Left(value)

        fun <L : Any, R : Any> right(value: R): Right<L, R> = Right(value)
    }
}
