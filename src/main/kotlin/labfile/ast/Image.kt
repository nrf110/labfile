package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class Image(
    val entrypoint: List<String>? = null,
    val name: String? = null,
    val pullPolicy: List<PullPolicy>? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("entrypoint", entrypoint?.toYaml())
        put("name", name?.toYaml())
        put("pull_policy", pullPolicy?.toYaml())
    }
}
