package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class DastConfiguration(
    val scannerProfile: String? = null,
    val siteProfile: String? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("scanner_profile", scannerProfile?.toYaml())
        put("site_profile", siteProfile?.toYaml())
    }
}
