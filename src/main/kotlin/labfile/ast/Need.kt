package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class Need(
    val artifacts: Boolean? = null,
    val job: String? = null,
    val optional: Boolean? = null,
    val parallel: Matrix? = null,
    val pipeline: String? = null,
    val project: String? = null,
    val ref: String? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("artifacts", artifacts?.toYaml())
        put("job", job?.toYaml())
        put("optional", optional?.toYaml())
        put("parallel", parallel?.toYaml())
        put("pipeline", pipeline?.toYaml())
        put("project", project?.toYaml())
        put("ref", ref?.toYaml())
    }
}
