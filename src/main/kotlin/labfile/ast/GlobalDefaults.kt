package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class GlobalDefaults(
    val afterScript: List<Either<Script, ReferenceTag>>? = null,
    val artifacts: Artifacts? = null,
    val beforeScript: List<Either<Script, ReferenceTag>>? = null,
    val cache: Cache? = null,
    val image: Image? = null,
    val interruptible: Boolean? = null,
    val retry: Retry? = null,
    val services: List<Service>? = null,
    val tags: List<String>? = null,
    val timeout: String? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put(
            "after_script",
            afterScript?.map { it.fold(Script::toYaml, ReferenceTag::toYaml) }?.toYaml()
        )
        put("artifacts", artifacts?.toYaml())
        put(
            "before_script",
            beforeScript?.map { it.fold(Script::toYaml, ReferenceTag::toYaml) }?.toYaml()
        )
        put("cache", cache?.toYaml())
        put("image", image?.toYaml())
        put("interruptible", interruptible?.toYaml())
        put("retry", retry?.toYaml())
        put("services", services?.toYaml())
        put("tags", tags?.toYaml())
        put("timeout", timeout?.toYaml())
    }
}
