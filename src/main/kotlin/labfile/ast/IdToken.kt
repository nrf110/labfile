package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.toYaml

data class IdToken(
    val aud: List<String> = emptyList(),
) : YamlNode {
    override fun toYaml(): Yaml = mapOf("aud".toYaml() to aud.toYaml()).toYaml()
}
