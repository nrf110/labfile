package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class Service(
    val alias: String? = null,
    val command: List<String>? = null,
    val entrypoint: List<String>? = null,
    val name: String? = null,
    val pullPolicy: List<PullPolicy>? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("alias", alias?.toYaml())
        put("command", command?.toYaml())
        put("entrypoint", entrypoint?.toYaml())
        put("name", name?.toYaml())
        put("pull_policy", pullPolicy?.toYaml())
    }
}
