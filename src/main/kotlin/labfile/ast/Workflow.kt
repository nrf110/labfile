package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.toYaml

data class Workflow(val rules: List<Either<Rule, ReferenceTag>>) : YamlNode {
    override fun toYaml(): Yaml =
        mapOf(
                "rules".toYaml() to
                    rules.map { it.fold(Rule::toYaml, ReferenceTag::toYaml) }.toYaml(),
            )
            .toYaml()
}
