package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.toYaml

typealias AllowFailure = Either<Boolean, ExitCodes>

data class ExitCodes(val exitCodes: List<Int>) : YamlNode {
    override fun toYaml(): Yaml = mapOf("exit_codes".toYaml() to exitCodes.toYaml()).toYaml()
}
