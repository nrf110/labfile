package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class Rule(
    val allowFailure: AllowFailure? = null,
    val changes: List<String>? = null,
    val exists: List<String>? = null,
    val `if`: String? = null,
    val needs: List<Need>? = null,
    val startIn: String? = null,
    val variables: Map<String, Variable>? = null,
    val `when`: When? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("allow_failure", allowFailure?.fold(Boolean::toYaml, ExitCodes::toYaml))
        put("changes", changes?.toYaml())
        put("exists", exists?.toYaml())
        put("if", `if`?.toYaml())
        put("needs", needs?.toYaml())
        put("start_in", startIn?.toYaml())
        put(
            "variables",
            variables
                ?.asSequence()
                ?.associate { (key, value) -> key.toYaml() to value.toYaml() }
                ?.toYaml()
        )
        put("when", `when`?.toYaml())
    }
}
