package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class Job(
    /**
     * `name` is here to make it easier for jobs depend on each via `needs`. It should not be
     * included in toYaml() *
     */
    val name: String,
    val afterScript: List<Either<Script, ReferenceTag>>? = null,
    val allowFailure: AllowFailure? = null,
    val artifacts: Artifacts? = null,
    val beforeScript: List<Either<Script, ReferenceTag>>? = null,
    val cache: Cache? = null,
    val coverage: String? = null,
    val dastConfiguration: DastConfiguration? = null,
    val dependencies: List<String>? = null,
    val environment: Environment? = null,
    val except: LegacyControl? = null,
    val extends: String? = null,
    val hooks: Hooks? = null,
    val idTokens: Map<String, IdToken>? = null,
    val image: Image? = null,
    val include: List<Include>? = null,
    val inherit: Inherit? = null,
    val interruptible: Boolean? = null,
    val needs: List<Need>? = null,
    val only: LegacyControl? = null,
    val parallel: Parallel? = null,
    val publish: String? = null,
    val release: Release? = null,
    val resourceGroup: String? = null,
    val retry: Retry? = null,
    val rules: List<Either<Rule, ReferenceTag>>? = null,
    val script: List<Either<Script, ReferenceTag>>? = null,
    val secrets: Secrets? = null,
    val services: List<Service>? = null,
    val stage: String? = null,
    val startIn: String? = null,
    val tags: List<String>? = null,
    val timeout: String? = null,
    val trigger: Trigger? = null,
    val variables: Map<String, Variable>? = null,
    val `when`: When? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put(
            "after_script",
            afterScript?.map { it.fold(Script::toYaml, ReferenceTag::toYaml) }?.toYaml()
        )
        put("allow_failure", allowFailure?.fold(Boolean::toYaml, ExitCodes::toYaml))
        put("artifacts", artifacts?.toYaml())
        put(
            "before_script",
            beforeScript?.map { it.fold(Script::toYaml, ReferenceTag::toYaml) }?.toYaml()
        )
        put("cache", cache?.toYaml())
        put("coverage", coverage?.toYaml())
        put("dast_configuration", dastConfiguration?.toYaml())
        put("dependencies", dependencies?.toYaml())
        put("environment", environment?.toYaml())
        put("except", except?.toYaml())
        put("extends", extends?.toYaml())
        put("hooks", hooks?.toYaml())
        put(
            "id_tokens",
            idTokens
                ?.asSequence()
                ?.associate { (key, value) -> key.toYaml() to value.toYaml() }
                ?.toYaml()
        )
        put("image", image?.toYaml())
        put("include", include?.toYaml())
        put("inherit", inherit?.toYaml())
        put("interruptible", interruptible?.toYaml())
        put("needs", needs?.toYaml())
        put("only", only?.toYaml())
        put("parallel", parallel?.fold(Int::toYaml, Matrix::toYaml))
        put("publish", publish?.toYaml())
        put("release", release?.toYaml())
        put("resource_group", resourceGroup?.toYaml())
        put("retry", retry?.toYaml())
        put("rules", rules?.map { it.fold(Rule::toYaml, ReferenceTag::toYaml) }?.toYaml())
        put("script", script?.map { it.fold(Script::toYaml, ReferenceTag::toYaml) }?.toYaml())
        put("secrets", secrets?.toYaml())
        put("services", services?.toYaml())
        put("stage", stage?.toYaml())
        put("start_in", startIn?.toYaml())
        put("tags", tags?.toYaml())
        put("timeout", timeout?.toYaml())
        put("trigger", trigger?.toYaml())
        put(
            "variables",
            variables
                ?.asSequence()
                ?.associate { (key, value) -> key.toYaml() to value.toYaml() }
                ?.toYaml()
        )
        put("when", `when`?.toYaml())
    }
}
