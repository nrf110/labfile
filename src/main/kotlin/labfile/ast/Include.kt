package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

sealed class Include : YamlNode {
    data class Local(val path: String) : Include() {

        override fun toYaml(): Yaml = mapOf("local".toYaml() to path.toYaml()).toYaml()
    }

    data class Project(val file: List<String>, val project: String, val ref: String? = null) :
        Include() {
        override fun toYaml(): Yaml = buildYaml {
            put("project", project.toYaml())
            put("ref", ref?.toYaml())
            put("file", file.toYaml())
        }
    }

    data class Remote(val url: String) : Include() {

        override fun toYaml(): Yaml = mapOf("remote".toYaml() to url.toYaml()).toYaml()
    }

    data class Template(val path: String) : Include() {

        override fun toYaml(): Yaml = mapOf("template".toYaml() to path.toYaml()).toYaml()
    }

    data class Artifact(val artifact: String, val job: String) : Include() {
        override fun toYaml(): Yaml =
            mapOf(
                    "artifact".toYaml() to artifact.toYaml(),
                    "job".toYaml() to job.toYaml(),
                )
                .toYaml()
    }
}
