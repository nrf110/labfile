package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class Pipeline(
    val default: GlobalDefaults? = null,
    val include: List<Include>? = null,
    val jobs: Map<String, Job>? = null,
    val stages: Set<String>? = null,
    val variables: Map<String, Variable>? = null,
    val workflow: Workflow? = null
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("default", default?.toYaml())
        put("include", include?.toYaml())
        put("stages", stages?.toYaml())
        put(
            "variables",
            variables
                ?.asSequence()
                ?.associate { (key, value) -> key.toYaml() to value.toYaml() }
                ?.toYaml()
        )
        put("workflow", workflow?.toYaml())
        jobs?.forEach { (name, job) -> put(name, job.toYaml()) }
    }
}
