package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class Inherit(
    val default: Either<Boolean, List<Default>>? = null,
    val variables: Either<Boolean, List<String>>? = null,
) : YamlNode {
    enum class Default {
        IMAGE,
        SERVICE,
        BEFORE_SCRIPT,
        AFTER_SCRIPT,
        TAGS,
        CACHE,
        ARTIFACTS,
        RETRY,
        TIMEOUT,
        INTERRUPTIBLE,
    }

    override fun toYaml(): Yaml = buildYaml {
        put("default", default?.fold(Boolean::toYaml, List<Default>::toYaml))
        put("variables", variables?.fold(Boolean::toYaml, List<String>::toYaml))
    }
}
