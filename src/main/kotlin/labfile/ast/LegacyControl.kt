package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class LegacyControl(
    val changes: List<String>? = null,
    val kubernetes: Kubernetes? = null,
    val refs: List<String>? = null,
    val variables: List<String>? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("changes", changes?.toYaml())
        put("kubernetes", kubernetes?.toYaml())
        put("refs", refs?.toYaml())
        put("variables", variables?.toYaml())
    }
}
