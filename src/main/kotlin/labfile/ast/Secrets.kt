package labfile.ast

import labfile.marshallers.Yaml
import labfile.marshallers.YamlNode
import labfile.marshallers.buildYaml
import labfile.marshallers.put
import labfile.marshallers.toYaml

data class VaultEngine(
    val name: String? = null,
    val path: String? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("name", name?.toYaml())
        put("path", path?.toYaml())
    }
}

data class VaultConfig(
    val engine: VaultEngine? = null,
    val field: String? = null,
    val path: String? = null,
) : YamlNode {
    override fun toYaml(): Yaml = buildYaml {
        put("engine", engine?.toYaml())
        put("field", field?.toYaml())
        put("path", path?.toYaml())
    }
}

sealed class Secret : YamlNode {
    data class VaultSecret(val value: Either<String, VaultConfig>) : Secret() {
        override fun toYaml(): Yaml =
            mapOf("vault".toYaml() to value.fold(String::toYaml, VaultConfig::toYaml)).toYaml()
    }
}

class Secrets(val map: MutableMap<String, Secret> = mutableMapOf()) : YamlNode {
    override fun toYaml(): Yaml =
        map.asSequence().associate { (key, value) -> key.toYaml() to value.toYaml() }.toYaml()
}
