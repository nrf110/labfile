@file:Suppress("TooManyFunctions")

package labfile.marshallers

fun buildYaml(init: MutableMap<Yaml, Yaml>.() -> Unit): Yaml = buildMap(init).toYaml()

fun MutableMap<Yaml, Yaml>.put(key: String, value: Yaml?): Yaml? =
    value?.let { put(key.toYaml(), it) }

fun String.toYaml(): Yaml = Yaml.ScalarNode.StringNode(this)

fun Number.toYaml(): Yaml = Yaml.ScalarNode.NumberNode(this)

fun Boolean.toYaml(): Yaml = Yaml.ScalarNode.BooleanNode(this)

inline fun <reified E : Enum<E>> Enum<E>.toYaml(): Yaml = this.name.lowercase().toYaml()

@JvmName("toListYaml")
fun List<Yaml>.toYaml(): Yaml =
    if (this.isEmpty()) Yaml.NullNode
    else
        Yaml.CollectionNode.ListNode(
            map { yaml ->
                when (yaml) {
                    is Yaml.CollectionNode.MapNode -> yaml.copy(isListElement = true)
                    else -> yaml
                }
            }
        )

@JvmName("toListYamlNode") fun List<YamlNode>.toYaml(): Yaml = map { it.toYaml() }.toYaml()

@JvmName("toSetString") fun Set<String>.toYaml(): Yaml = map { it.toYaml() }.toYaml()

@JvmName("toSetYaml") fun Set<Yaml>.toYaml(): Yaml = toList().toYaml()

@JvmName("toSetYamlNode") fun Set<YamlNode>.toYaml(): Yaml = toList().toYaml()

@JvmName("toListString") fun List<String>.toYaml(): Yaml = map { it.toYaml() }.toYaml()

@JvmName("toListNumber") fun List<Number>.toYaml(): Yaml = map { it.toYaml() }.toYaml()

@JvmName("toListEnum")
inline fun <reified E : Enum<E>> List<Enum<E>>.toYaml(): Yaml = map { it.toYaml() }.toYaml()

@JvmName("toMapString")
fun Map<String, String>.toYaml(): Yaml =
    asSequence().associate { (key, value) -> key.toYaml() to value.toYaml() }.toYaml()

@JvmName("toMapYamlNode")
fun Map<YamlNode, YamlNode>.toYaml(): Yaml =
    Yaml.CollectionNode.MapNode(this.asSequence().associate { (k, v) -> k.toYaml() to v.toYaml() })

@JvmName("toMapYaml") fun Map<Yaml, Yaml>.toYaml(): Yaml = let { Yaml.CollectionNode.MapNode(it) }
