package labfile.marshallers

interface YamlNode {
    fun toYaml(): Yaml
}

const val YAML_INDENT = 2

sealed class Yaml : YamlNode {
    abstract fun serialize(indentation: Int = 0): String

    override fun toYaml(): Yaml = this

    object NullNode : Yaml() {
        override fun serialize(indentation: Int): String = "null"
    }

    sealed class ScalarNode : Yaml() {

        data class StringNode(val value: String) : ScalarNode() {
            override fun serialize(indentation: Int): String {
                val lines = value.lines()
                return when {
                    lines.size == 1 && ":" in value -> "'$value'"
                    lines.size == 1 -> value
                    else ->
                        buildString {
                            appendLine("|-")
                            lines.forEachIndexed { idx, line ->
                                indent(indentation)
                                append(line)
                                if (idx < lines.size - 1) {
                                    appendLine()
                                }
                            }
                        }
                }
            }
        }

        data class NumberNode(val value: Number) : ScalarNode() {
            override fun serialize(indentation: Int): String = value.toString()
        }

        data class BooleanNode(val value: Boolean) : ScalarNode() {
            override fun serialize(indentation: Int): String = value.toString()
        }
    }

    sealed class CollectionNode : Yaml() {
        data class ListNode(val elements: List<Yaml>) : CollectionNode() {
            override fun serialize(indentation: Int): String = buildString {
                elements
                    .filterNot { it is NullNode }
                    .ifEmpty {
                        return "[]"
                    }
                    .forEachIndexed { idx, element ->
                        indent(indentation)
                        append("- ")
                        append(element.serialize(indentation + YAML_INDENT))
                        if (idx < elements.size - 1) {
                            appendLine()
                        }
                    }
            }
        }

        data class MapNode(val elements: Map<Yaml, Yaml>, val isListElement: Boolean = false) :
            CollectionNode() {

            override fun serialize(indentation: Int): String = buildString {
                elements.asSequence().forEachIndexed { idx, (key, value) ->
                    if (key !is ScalarNode) {
                        throw UnsupportedOperationException("Key $key is not a scalar value")
                    }

                    if (value !is NullNode) {
                        if (!isListElement || idx > 0) {
                            indent(indentation)
                        }
                        append(key.serialize(indentation))
                        when (value) {
                            is CollectionNode -> {
                                append(":")
                                appendLine()
                                append(value.serialize(indentation + YAML_INDENT))
                            }
                            is ScalarNode -> {
                                append(": ")
                                append(value.serialize(indentation + YAML_INDENT))
                            }
                            else -> {}
                        }
                        if (idx < elements.size - 1) {
                            appendLine()
                        }
                    }
                }
            }
        }
    }
}

internal fun StringBuilder.indent(indentation: Int) {
    repeat(indentation) { append(" ") }
}
