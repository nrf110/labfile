package labfile

import labfile.ast.Pipeline
import labfile.ast.When
import labfile.dsl.pipeline

val builder: Pipeline = pipeline {
    val build by stage()

    job("build") {
        stage = build

        script {
            shell(
                """
            | echo 'this is a test'
            """
                    .trimMargin()
            )
        }

        script {
            +shell("echo 'line 1'")
            +shell("echo \${ENV_VAR}")
            +shell("exit 0")
        }

        parallel { matrix { +mapOf("VAR_1" to listOf("3"), "VAR_2" to listOf("a", "b")) } }

        artifacts {
            public = true
            `when` = When.ALWAYS

            paths { +"/b/c" }

            exclude { +"/d/e" }
        }

        release("") {
            assets {
                links {
                    +link {
                        name = ""
                        url = ""
                    }
                }
            }
        }
    }
}
