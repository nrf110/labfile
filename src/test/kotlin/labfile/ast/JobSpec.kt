package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class JobSpec : ShouldSpec({ include(toYaml(JobMocks.job)) })

object JobMocks {
    val job by lazy {
        val subject =
            Job(
                name = "testjob",
                afterScript = listOf(Either.left(Script.Shell("echo 'ending'"))),
                allowFailure = AllowFailure.left(true),
                artifacts = ArtifactMocks.artifacts.subject,
                beforeScript =
                    listOf(
                        Either.left(
                            Script.Shell(
                                """
                |echo 'first line'
                |echo 'second line'
                |echo '${'$'}VARIABLE
            """
                                    .trimMargin()
                            )
                        )
                    ),
                cache = CacheMocks.cache.subject,
                coverage = "Coverage: ",
                dastConfiguration =
                    DastConfiguration(
                        scannerProfile = "smoke test",
                        siteProfile = "my company",
                    ),
                dependencies = listOf("checkout"),
                environment = EnvironmentMocks.environment.subject,
                extends = "base",
                hooks =
                    Hooks(
                        preGetSourcesScript =
                            listOf(
                                Either.left(Script.Shell("echo '1'")),
                                Either.left(Script.Shell("echo '2'")),
                            )
                    ),
                idTokens =
                    mapOf(
                        "GCP" to
                            IdToken(
                                aud =
                                    listOf(
                                        "https://google.com",
                                    )
                            ),
                        "AWS" to IdToken(aud = listOf("https://aws.com"))
                    ),
                image = ImageMocks.image.subject,
                include = listOf(IncludeMocks.artifact.subject, IncludeMocks.file.subject),
                inherit = InheritMocks.inheritRight.subject,
                interruptible = true,
                needs = listOf(NeedMocks.need.subject),
                parallel = Parallel.right(ParallelMocks.matrix.subject),
                publish = "dist",
                release = ReleaseMocks.release.subject,
                resourceGroup = "nodejs",
                retry = RetryMocks.retry.subject,
                rules =
                    listOf(
                        Either.left(RuleMocks.rule.subject),
                        Either.right(ReferenceMocks.reference.subject),
                    ),
                script = listOf(Either.left(Script.Shell("echo 'doing stuff'"))),
                services = listOf(ServiceMocks.service.subject),
                stage = "build",
                startIn = "2 hours",
                tags = listOf("foo:bar"),
                timeout = "30 seconds",
                trigger = TriggerMocks.child.subject,
                variables = mapOf("VERSION" to Variable(value = "1.0.0")),
            )

        val releaseDate = subject.release!!.releasedAt

        TestData(
            subject,
            """
                |after_script:
                |  - echo 'ending'
                |allow_failure: true
                |artifacts:
                |  exclude:
                |    - c
                |    - d
                |  expire_in: 3 hours
                |  expose_as: archive.tar
                |  name: archive
                |  paths:
                |    - a
                |    - b
                |  public: true
                |  reports:
                |    api_fuzzing: api-fuzz.txt
                |    codequality: cq.txt
                |    container_scanning: containers.txt
                |    coverage_fuzzing: cov-fuzz.txt
                |    coverage_report:
                |      coverage_format: text-lcov
                |      path: test.lcov
                |    dast: dast
                |    dependency_scanning: deps.txt
                |    dotenv: .env
                |    junit:
                |      - a
                |      - b
                |    license_scanning: license.txt
                |    load_performance: lp.xml
                |    metrics: metrics.txt
                |    performance: perf.txt
                |    requirements: reqs.txt
                |    sast: sast.xml
                |    secret_detection: secrets.json
                |    terraform: main.tf
                |  untracked: false
                |  when: manual
                |before_script:
                |  - |-
                |    echo 'first line'
                |    echo 'second line'
                |    echo '${'$'}VARIABLE
                |cache:
                |  key:
                |    files:
                |      - a.txt
                |      - b.txt
                |    prefix: /test
                |  paths:
                |    - /c
                |    - /d
                |  policy: pull
                |  unprotect: false
                |  untracked: true
                |  when: on_failure
                |coverage: 'Coverage: '
                |dast_configuration:
                |  scanner_profile: smoke test
                |  site_profile: my company
                |dependencies:
                |  - checkout
                |environment:
                |  action: start
                |  auto_stop_in: 3 days
                |  name: dev
                |  on_stop: teardown
                |  url: 'https://example.com'
                |extends: base
                |hooks:
                |  pre_get_sources_script:
                |    - echo '1'
                |    - echo '2'
                |id_tokens:
                |  GCP:
                |    aud:
                |      - 'https://google.com'
                |  AWS:
                |    aud:
                |      - 'https://aws.com'
                |image:
                |  entrypoint:
                |    - /bin/bash
                |    - -c
                |    - echo 'hello'
                |  name: redis
                |  pull_policy:
                |    - if-not-present
                |include:
                |  - artifact: /a/b.yml
                |    job: build
                |  - project: test
                |    ref: develop
                |    file:
                |      - /a/b
                |      - /c/d
                |inherit:
                |  default:
                |    - after_script
                |    - artifacts
                |  variables:
                |    - NODE_ENV
                |    - API_KEY
                |    - API_TOKEN
                |interruptible: true
                |needs:
                |  - artifacts: true
                |    job: build
                |    parallel:
                |      matrix:
                |        - PROVIDER:
                |            - aws
                |          STACK:
                |            - monitoring
                |            - app1
                |            - app2
                |        - PROVIDER:
                |            - ovh
                |          STACK:
                |            - monitoring
                |            - backup
                |            - app
                |        - PROVIDER:
                |            - gcp
                |            - vultr
                |          STACK:
                |            - data
                |            - processing
                |    pipeline: mypipeline
                |    project: myproject
                |    ref: develop
                |parallel:
                |  matrix:
                |    - PROVIDER:
                |        - aws
                |      STACK:
                |        - monitoring
                |        - app1
                |        - app2
                |    - PROVIDER:
                |        - ovh
                |      STACK:
                |        - monitoring
                |        - backup
                |        - app
                |    - PROVIDER:
                |        - gcp
                |        - vultr
                |      STACK:
                |        - data
                |        - processing
                |publish: dist
                |release:
                |  assets:
                |    links:
                |      - filepath: test.jpg
                |        link_type: other
                |        name: test
                |        url: 'https://test.com/'
                |  description: a long description
                |  milestones:
                |    - a
                |    - b
                |    - c
                |  name: my name
                |  ref: the ref
                |  released_at: '$releaseDate'
                |  tag_message: a tag message
                |  tag_name: tag name
                |resource_group: nodejs
                |retry:
                |  max: 3
                |  when:
                |    - api_failure
                |    - archived_failure
                |rules:
                |  - allow_failure:
                |      exit_codes:
                |        - 1
                |        - 2
                |        - 3
                |    changes:
                |      - public/favicon.png
                |    exists:
                |      - /some/path
                |    if: ${'$'}ENV_VAR == "some value"
                |    needs:
                |      - job: job1
                |    start_in: 3 hours
                |    variables:
                |      VERSION: 1.0.0
                |    when: on_success
                |  - !reference [.test, property]
                |script:
                |  - echo 'doing stuff'
                |services:
                |  - alias: theredis
                |    command:
                |      - a
                |      - b
                |    entrypoint:
                |      - c
                |      - d
                |    name: redis
                |    pull_policy:
                |      - if-not-present
                |stage: build
                |start_in: 2 hours
                |tags:
                |  - 'foo:bar'
                |timeout: 30 seconds
                |trigger:
                |  forward:
                |    pipeline_variables: true
                |    yaml_variables: false
                |  include:
                |    - artifact: /a/b.yml
                |      job: build
                |  strategy: depend
                |variables:
                |  VERSION: 1.0.0
            """
                .trimMargin()
        )
    }
}
