package labfile.ast

import io.kotest.assertions.throwables.shouldThrowExactly
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.shouldBe

class VariableSpec :
    ShouldSpec({
        context("Variable") {
            should("throw an exception if neither value nor description are provided") {
                val ex = shouldThrowExactly<IllegalArgumentException> { Variable() }
                ex.message.shouldBe("Variable must have either value or description")
            }
        }

        include(toYaml("Variable with only description", VariableMocks.justDescription))
        include(toYaml("Variable with only string value", VariableMocks.justValue))
        include(toYaml("Variable with multiline string value", VariableMocks.multilineValue))
        include(toYaml("Variable with only integer value", VariableMocks.integerValue))
        include(toYaml("Variable with only decimal value", VariableMocks.decimalValue))
        include(toYaml("Variable with only boolean value", VariableMocks.booleanValue))
        include(toYaml("Full Variable", VariableMocks.full))
    })

object VariableMocks {
    val justValue by lazy {
        TestData(
            subject = Variable(value = "val"),
            expected =
                """
                |val
            """
                    .trimMargin()
        )
    }

    val multilineValue by lazy {
        TestData(
            subject = Variable(value = "row1\nrow2"),
            expected =
                """
                ||-
                |row1
                |row2
                """
                    .trimMargin()
        )
    }

    val integerValue by lazy {
        TestData(
            subject = Variable(value = "394"),
            expected =
                """
                |'394'
                """
                    .trimMargin()
        )
    }

    val decimalValue by lazy {
        TestData(
            subject = Variable(value = "394.12"),
            expected =
                """
                |'394.12'
                """
                    .trimMargin()
        )
    }

    val booleanValue by lazy {
        TestData(
            subject = Variable(value = "true"),
            expected =
                """
                |'true'
                """
                    .trimMargin()
        )
    }

    val justDescription by lazy {
        TestData(
            subject = Variable(description = "desc"),
            expected =
                """
                |description: desc
            """
                    .trimMargin()
        )
    }
    val full by lazy {
        TestData(
            subject = Variable(description = "full desc", value = "full val"),
            expected =
                """
                |description: full desc
                |value: full val
            """
                    .trimMargin()
        )
    }
}
