package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class InheritSpec :
    ShouldSpec({
        include(toYaml("Inherit (right)", InheritMocks.inheritRight))
        include(toYaml("Inherit (left)", InheritMocks.inheritLeft))
    })

object InheritMocks {
    val inheritRight by lazy {
        val subject =
            Inherit(
                default =
                    Either.right(listOf(Inherit.Default.AFTER_SCRIPT, Inherit.Default.ARTIFACTS)),
                variables = Either.right(listOf("NODE_ENV", "API_KEY", "API_TOKEN")),
            )

        TestData(
            subject,
            """
                |default:
                |  - after_script
                |  - artifacts
                |variables:
                |  - NODE_ENV
                |  - API_KEY
                |  - API_TOKEN
            """
                .trimMargin()
        )
    }

    val inheritLeft by lazy {
        val subject = Inherit(default = Either.left(false), variables = Either.left(true))

        TestData(
            subject,
            """
                |default: false
                |variables: true
            """
                .trimMargin()
        )
    }
}
