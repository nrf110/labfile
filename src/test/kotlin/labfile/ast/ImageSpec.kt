package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class ImageSpec : ShouldSpec({ include(toYaml(ImageMocks.image)) })

object ImageMocks {
    val image by lazy {
        val subject =
            Image(
                name = "redis",
                entrypoint = listOf("/bin/bash", "-c", "echo 'hello'"),
                pullPolicy = listOf(PullPolicy.`IF-NOT-PRESENT`),
            )

        TestData(
            subject,
            """
                |entrypoint:
                |  - /bin/bash
                |  - -c
                |  - echo 'hello'
                |name: redis
                |pull_policy:
                |  - if-not-present
            """
                .trimMargin()
        )
    }
}
