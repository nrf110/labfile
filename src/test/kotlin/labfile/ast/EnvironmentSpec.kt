package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class EnvironmentSpec : ShouldSpec({ include(toYaml(EnvironmentMocks.environment)) })

object EnvironmentMocks {
    val environment by lazy {
        val subject =
            Environment(
                name = "dev",
                action = Environment.Action.START,
                autoStopIn = "3 days",
                onStop = "teardown",
                url = "https://example.com",
            )

        TestData(
            subject,
            """
                |action: start
                |auto_stop_in: 3 days
                |name: dev
                |on_stop: teardown
                |url: 'https://example.com'
            """
                .trimMargin()
        )
    }
}
