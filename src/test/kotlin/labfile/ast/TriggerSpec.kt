package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class TriggerSpec :
    ShouldSpec({
        include(toYaml(TriggerMocks.multiproject))
        include(toYaml(TriggerMocks.child))
    })

object TriggerMocks {
    val multiproject by lazy {
        val subject =
            Trigger(
                branch = "develop",
                forward =
                    Trigger.Forward(
                        pipelineVariables = true,
                        yamlVariables = false,
                    ),
                project = "myproject",
                strategy = Trigger.Strategy.DEPEND,
            )

        TestData(
            subject,
            """
                |branch: develop
                |forward:
                |  pipeline_variables: true
                |  yaml_variables: false
                |project: myproject
                |strategy: depend
            """
                .trimMargin()
        )
    }

    val child by lazy {
        val subject =
            Trigger(
                forward =
                    Trigger.Forward(
                        pipelineVariables = true,
                        yamlVariables = false,
                    ),
                include = listOf(IncludeMocks.artifact.subject),
                strategy = Trigger.Strategy.DEPEND,
            )

        TestData(
            subject,
            """
                |forward:
                |  pipeline_variables: true
                |  yaml_variables: false
                |include:
                |  - artifact: /a/b.yml
                |    job: build
                |strategy: depend
            """
                .trimMargin()
        )
    }
}
