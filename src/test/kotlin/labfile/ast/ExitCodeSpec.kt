package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class ExitCodesSpec : ShouldSpec({ include(toYaml(ExitCodeMocks.exitCodes)) })

object ExitCodeMocks {
    val exitCodes by lazy {
        val subject = ExitCodes(exitCodes = listOf(1, 2, 3))

        TestData(
            subject,
            """
                |exit_codes:
                |  - 1
                |  - 2
                |  - 3
            """
                .trimMargin()
        )
    }
}
