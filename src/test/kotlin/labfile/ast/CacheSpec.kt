package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class CacheSpec :
    ShouldSpec({
        include(toYaml(CacheMocks.cacheKeyFiles))
        include(toYaml(CacheMocks.cache))
    })

object CacheMocks {
    val cacheKeyFiles by lazy {
        val subject =
            CacheKeyFiles(
                prefix = "/test",
                files = listOf("a.txt", "b.txt"),
            )

        TestData(
            subject,
            """               
                |files:
                |  - a.txt
                |  - b.txt
                |prefix: /test
            """
                .trimMargin()
        )
    }

    val cache by lazy {
        val subject =
            Cache(
                paths = listOf("/c", "/d"),
                key = Either.right(cacheKeyFiles.subject),
                unprotect = false,
                untracked = true,
                `when` = Cache.When.ON_FAILURE,
                policy = Cache.Policy.PULL,
            )

        TestData(
            subject,
            """
                |key:
                |  files:
                |    - a.txt
                |    - b.txt
                |  prefix: /test
                |paths:
                |  - /c
                |  - /d
                |policy: pull
                |unprotect: false
                |untracked: true
                |when: on_failure
            """
                .trimMargin()
        )
    }
}
