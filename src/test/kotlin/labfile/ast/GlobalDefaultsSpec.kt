package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class GlobalDefaultsSpec : ShouldSpec({ include(toYaml(GlobalDefaultsMocks.default)) })

object GlobalDefaultsMocks {
    val default by lazy {
        val subject =
            GlobalDefaults(
                image = ImageMocks.image.subject,
                services = listOf(ServiceMocks.service.subject),
                beforeScript = ScriptMocks.before,
                afterScript = ScriptMocks.after,
                tags = listOf("env:dev", "service:users"),
                cache = CacheMocks.cache.subject,
                artifacts = ArtifactMocks.artifacts.subject,
                retry = RetryMocks.retry.subject,
                timeout = "45 seconds",
                interruptible = true,
            )

        TestData(
            subject,
            """
                |after_script:
                |  - echo 'ending'
                |  - !reference [.after, last]
                |artifacts:
                |  exclude:
                |    - c
                |    - d
                |  expire_in: 3 hours
                |  expose_as: archive.tar
                |  name: archive
                |  paths:
                |    - a
                |    - b
                |  public: true
                |  reports:
                |    api_fuzzing: api-fuzz.txt
                |    codequality: cq.txt
                |    container_scanning: containers.txt
                |    coverage_fuzzing: cov-fuzz.txt
                |    coverage_report:
                |      coverage_format: text-lcov
                |      path: test.lcov
                |    dast: dast
                |    dependency_scanning: deps.txt
                |    dotenv: .env
                |    junit:
                |      - a
                |      - b
                |    license_scanning: license.txt
                |    load_performance: lp.xml
                |    metrics: metrics.txt
                |    performance: perf.txt
                |    requirements: reqs.txt
                |    sast: sast.xml
                |    secret_detection: secrets.json
                |    terraform: main.tf
                |  untracked: false
                |  when: manual
                |before_script:
                |  - echo 'starting'
                |  - !reference [.before, first]
                |cache:
                |  key:
                |    files:
                |      - a.txt
                |      - b.txt
                |    prefix: /test
                |  paths:
                |    - /c
                |    - /d
                |  policy: pull
                |  unprotect: false
                |  untracked: true
                |  when: on_failure
                |image:
                |  entrypoint:
                |    - /bin/bash
                |    - -c
                |    - echo 'hello'
                |  name: redis
                |  pull_policy:
                |    - if-not-present
                |interruptible: true
                |retry:
                |  max: 3
                |  when:
                |    - api_failure
                |    - archived_failure
                |services:
                |  - alias: theredis
                |    command:
                |      - a
                |      - b
                |    entrypoint:
                |      - c
                |      - d
                |    name: redis
                |    pull_policy:
                |      - if-not-present
                |tags:
                |  - 'env:dev'
                |  - 'service:users'
                |timeout: 45 seconds
            """
                .trimMargin()
        )
    }
}
