package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class NeedSpec : ShouldSpec({ include(toYaml(NeedMocks.need)) })

object NeedMocks {
    val need by lazy {
        val subject =
            Need(
                job = "build",
                project = "myproject",
                ref = "develop",
                parallel =
                    Matrix(
                        listOf(
                            mapOf(
                                "PROVIDER" to listOf("aws"),
                                "STACK" to listOf("monitoring", "app1", "app2")
                            ),
                            mapOf(
                                "PROVIDER" to listOf("ovh"),
                                "STACK" to listOf("monitoring", "backup", "app")
                            ),
                            mapOf(
                                "PROVIDER" to listOf("gcp", "vultr"),
                                "STACK" to listOf("data", "processing")
                            )
                        )
                    ),
                pipeline = "mypipeline",
                artifacts = true,
            )

        TestData(
            subject,
            """
                |artifacts: true
                |job: build
                |parallel:
                |  matrix:
                |    - PROVIDER:
                |        - aws
                |      STACK:
                |        - monitoring
                |        - app1
                |        - app2
                |    - PROVIDER:
                |        - ovh
                |      STACK:
                |        - monitoring
                |        - backup
                |        - app
                |    - PROVIDER:
                |        - gcp
                |        - vultr
                |      STACK:
                |        - data
                |        - processing
                |pipeline: mypipeline
                |project: myproject
                |ref: develop
            """
                .trimMargin()
        )
    }
}
