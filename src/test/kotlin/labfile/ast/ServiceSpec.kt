package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class ServiceSpec : ShouldSpec({ include(toYaml(ServiceMocks.service)) })

object ServiceMocks {
    val service by lazy {
        val subject =
            Service(
                name = "redis",
                alias = "theredis",
                command = listOf("a", "b"),
                entrypoint = listOf("c", "d"),
                pullPolicy = listOf(PullPolicy.`IF-NOT-PRESENT`),
            )

        TestData(
            subject,
            """
                |alias: theredis
                |command:
                |  - a
                |  - b
                |entrypoint:
                |  - c
                |  - d
                |name: redis
                |pull_policy:
                |  - if-not-present
            """
                .trimMargin()
        )
    }
}
