package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class IncludeSpec :
    ShouldSpec({
        include(toYaml(IncludeMocks.local))
        include(toYaml(IncludeMocks.file))
        include(toYaml(IncludeMocks.remote))
        include(toYaml(IncludeMocks.template))
        include(toYaml(IncludeMocks.artifact))
    })

object IncludeMocks {
    val local by lazy {
        TestData(
            Include.Local("a/b"),
            """
                |local: a/b
            """
                .trimMargin()
        )
    }

    val file by lazy {
        val paths = listOf("/a/b", "/c/d")
        val project = "test"
        val ref = "develop"

        TestData(
            Include.Project(paths, project, ref),
            """               
                |project: test
                |ref: develop
                |file:
                |  - /a/b
                |  - /c/d
            """
                .trimMargin()
        )
    }

    val remote by lazy {
        TestData(
            Include.Remote("https://test.com"),
            """
                |remote: 'https://test.com'
            """
                .trimMargin()
        )
    }

    val template by lazy {
        TestData(
            Include.Template("/a/b.yml"),
            """
                |template: /a/b.yml
            """
                .trimMargin()
        )
    }

    val artifact by lazy {
        TestData(
            Include.Artifact("/a/b.yml", "build"),
            """
                |artifact: /a/b.yml
                |job: build
            """
                .trimMargin()
        )
    }
}
