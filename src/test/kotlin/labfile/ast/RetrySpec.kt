package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class RetrySpec : ShouldSpec({ include(toYaml(RetryMocks.retry)) })

object RetryMocks {
    val retry by lazy {
        val subject =
            Retry(
                max = 3,
                `when` = listOf(Retry.When.API_FAILURE, Retry.When.ARCHIVED_FAILURE),
            )

        TestData(
            subject,
            """
                |max: 3
                |when:
                |  - api_failure
                |  - archived_failure
            """
                .trimMargin()
        )
    }
}
