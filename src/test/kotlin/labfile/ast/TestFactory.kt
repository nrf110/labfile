@file:Suppress("MatchingDeclarationName")

package labfile.ast

import io.kotest.core.spec.style.shouldSpec
import io.kotest.matchers.shouldBe
import labfile.marshallers.YamlNode

data class TestData<T>(val subject: T, val expected: String)

inline fun <reified T : YamlNode> toYaml(data: TestData<T>) =
    toYaml(data.subject::class.java.name, data)

inline fun <reified T : YamlNode> toYaml(name: String, data: TestData<T>) = shouldSpec {
    context("${name}.toYaml") {
        should("should serialize correctly") {
            (data.subject as YamlNode).toYaml().serialize().shouldBe(data.expected)
        }
    }
}
