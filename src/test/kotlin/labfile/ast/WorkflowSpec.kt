package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class WorkflowSpec : ShouldSpec({ include(toYaml(WorkflowMocks.workflow)) })

object WorkflowMocks {
    val workflow by lazy {
        TestData(
            Workflow(
                rules =
                    listOf(
                        Either.left(RuleMocks.rule.subject),
                        Either.right(ReferenceMocks.reference.subject),
                    )
            ),
            """
                |rules:
                |  - allow_failure:
                |      exit_codes:
                |        - 1
                |        - 2
                |        - 3
                |    changes:
                |      - public/favicon.png
                |    exists:
                |      - /some/path
                |    if: ${'$'}ENV_VAR == "some value"
                |    needs:
                |      - job: job1
                |    start_in: 3 hours
                |    variables:
                |      VERSION: 1.0.0
                |    when: on_success
                |  - !reference [.test, property]
            """
                .trimMargin()
        )
    }
}
