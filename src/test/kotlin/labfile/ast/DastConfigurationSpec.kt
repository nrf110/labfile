package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class DastConfigurationSpec : ShouldSpec({ include(toYaml(DastConfigurationMocks.dast)) })

object DastConfigurationMocks {
    val dast by lazy {
        val subject = DastConfiguration(scannerProfile = "smoke test", siteProfile = "my company")

        TestData(
            subject = subject,
            expected =
                """
                |scanner_profile: smoke test
                |site_profile: my company
            """
                    .trimMargin()
                    .trimIndent()
        )
    }
}
