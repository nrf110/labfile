package labfile.ast

object ScriptMocks {
    val before: List<Either<Script, ReferenceTag>> =
        listOf(
            Either.left(Script.Shell("echo 'starting'")),
            Either.right(ReferenceTag(".before", "first")),
        )

    val script: List<Either<Script, ReferenceTag>> =
        listOf(
            Either.left(Script.Shell("echo 'doing stuff'")),
            Either.right(ReferenceTag(".script", "middle")),
        )

    val after: List<Either<Script, ReferenceTag>> =
        listOf(
            Either.left(Script.Shell("echo 'ending'")),
            Either.right(ReferenceTag(".after", "last")),
        )
}
