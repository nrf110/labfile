package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class ArtifactsSpec :
    ShouldSpec({
        include(toYaml(ArtifactMocks.reports))
        include(toYaml(ArtifactMocks.artifacts))
    })

object ArtifactMocks {
    val reports by lazy {
        val subject =
            Reports(
                apiFuzzing = "api-fuzz.txt",
                codequality = "cq.txt",
                containerScanning = "containers.txt",
                coverageFuzzing = "cov-fuzz.txt",
                coverageReport =
                    CoverageReport(
                        coverageFormat = CoverageFormat.`TEXT-LCOV`,
                        path = "test.lcov",
                    ),
                dast = "dast",
                dependencyScanning = "deps.txt",
                dotenv = ".env",
                junit = listOf("a", "b"),
                licenseScanning = "license.txt",
                loadPerformance = "lp.xml",
                metrics = "metrics.txt",
                performance = "perf.txt",
                requirements = "reqs.txt",
                sast = "sast.xml",
                secretDetection = "secrets.json",
                terraform = "main.tf",
            )

        TestData(
            subject,
            """
                |api_fuzzing: api-fuzz.txt
                |codequality: cq.txt
                |container_scanning: containers.txt
                |coverage_fuzzing: cov-fuzz.txt
                |coverage_report:
                |  coverage_format: text-lcov
                |  path: test.lcov
                |dast: dast
                |dependency_scanning: deps.txt
                |dotenv: .env
                |junit:
                |  - a
                |  - b
                |license_scanning: license.txt
                |load_performance: lp.xml
                |metrics: metrics.txt
                |performance: perf.txt
                |requirements: reqs.txt
                |sast: sast.xml
                |secret_detection: secrets.json
                |terraform: main.tf
            """
                .trimMargin()
        )
    }

    val artifacts by lazy {
        val subject =
            Artifacts(
                paths = listOf("a", "b"),
                exclude = listOf("c", "d"),
                `public` = true,
                exposeAs = "archive.tar",
                name = "archive",
                untracked = false,
                `when` = When.MANUAL,
                expireIn = "3 hours",
                reports = ArtifactMocks.reports.subject,
            )

        TestData(
            subject,
            """
                |exclude:
                |  - c
                |  - d
                |expire_in: 3 hours
                |expose_as: archive.tar
                |name: archive
                |paths:
                |  - a
                |  - b
                |public: true
                |reports:
                |  api_fuzzing: api-fuzz.txt
                |  codequality: cq.txt
                |  container_scanning: containers.txt
                |  coverage_fuzzing: cov-fuzz.txt
                |  coverage_report:
                |    coverage_format: text-lcov
                |    path: test.lcov
                |  dast: dast
                |  dependency_scanning: deps.txt
                |  dotenv: .env
                |  junit:
                |    - a
                |    - b
                |  license_scanning: license.txt
                |  load_performance: lp.xml
                |  metrics: metrics.txt
                |  performance: perf.txt
                |  requirements: reqs.txt
                |  sast: sast.xml
                |  secret_detection: secrets.json
                |  terraform: main.tf
                |untracked: false
                |when: manual
            """
                .trimMargin()
        )
    }
}
