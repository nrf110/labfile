package labfile.ast

import io.kotest.core.spec.style.ShouldSpec
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class ReleaseSpec : ShouldSpec({ include(toYaml(ReleaseMocks.release)) })

object ReleaseMocks {
    val release by lazy {
        val releaseDate = ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_INSTANT)
        val release =
            Release(
                assets =
                    Assets(
                        links =
                            listOf(
                                Link(
                                    name = "test",
                                    filepath = "test.jpg",
                                    linkType = "other",
                                    url = "https://test.com/"
                                )
                            )
                    ),
                description = "a long description",
                milestones = listOf("a", "b", "c"),
                name = "my name",
                ref = "the ref",
                releasedAt = releaseDate,
                tagMessage = "a tag message",
                tagName = "tag name",
            )

        TestData(
            release,
            """
                |assets:
                |  links:
                |    - filepath: test.jpg
                |      link_type: other
                |      name: test
                |      url: 'https://test.com/'
                |description: a long description
                |milestones:
                |  - a
                |  - b
                |  - c
                |name: my name
                |ref: the ref
                |released_at: '$releaseDate'
                |tag_message: a tag message
                |tag_name: tag name
            """
                .trimMargin()
        )
    }
}
