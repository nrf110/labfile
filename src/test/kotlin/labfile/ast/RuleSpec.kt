package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class RuleSpec : ShouldSpec({ include(toYaml(RuleMocks.rule)) })

object RuleMocks {
    val rule by lazy {
        val subject =
            Rule(
                `if` = "\$ENV_VAR == \"some value\"",
                changes = listOf("public/favicon.png"),
                exists = listOf("/some/path"),
                `when` = When.ON_SUCCESS,
                startIn = "3 hours",
                allowFailure = AllowFailure.right(ExitCodeMocks.exitCodes.subject),
                variables = mapOf("VERSION" to Variable(value = "1.0.0")),
                needs = listOf(Need(job = "job1"))
            )

        TestData(
            subject,
            """                
                |allow_failure:
                |  exit_codes:
                |    - 1
                |    - 2
                |    - 3
                |changes:
                |  - public/favicon.png
                |exists:
                |  - /some/path
                |if: ${'$'}ENV_VAR == "some value"
                |needs:
                |  - job: job1
                |start_in: 3 hours
                |variables:
                |  VERSION: 1.0.0
                |when: on_success
            """
                .trimMargin()
        )
    }
}
