package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class ParallelSpec : ShouldSpec({ include(toYaml(ParallelMocks.matrix)) })

object ParallelMocks {
    val matrix by lazy {
        TestData(
            Matrix(
                matrix =
                    listOf(
                        mapOf(
                            "PROVIDER" to listOf("aws"),
                            "STACK" to listOf("monitoring", "app1", "app2")
                        ),
                        mapOf(
                            "PROVIDER" to listOf("ovh"),
                            "STACK" to listOf("monitoring", "backup", "app")
                        ),
                        mapOf(
                            "PROVIDER" to listOf("gcp", "vultr"),
                            "STACK" to listOf("data", "processing")
                        )
                    )
            ),
            """
                |matrix:
                |  - PROVIDER:
                |      - aws
                |    STACK:
                |      - monitoring
                |      - app1
                |      - app2
                |  - PROVIDER:
                |      - ovh
                |    STACK:
                |      - monitoring
                |      - backup
                |      - app
                |  - PROVIDER:
                |      - gcp
                |      - vultr
                |    STACK:
                |      - data
                |      - processing
            """
                .trimMargin()
        )
    }
}
