package labfile.ast

import io.kotest.core.spec.style.ShouldSpec

class ReferenceTagSpec : ShouldSpec({ include(toYaml(ReferenceMocks.reference)) })

object ReferenceMocks {
    val reference by lazy {
        val subject = ReferenceTag(".test", "property")

        TestData(subject, "!reference [.test, property]")
    }
}
