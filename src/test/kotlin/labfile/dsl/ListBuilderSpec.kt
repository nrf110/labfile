package labfile.dsl

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainExactly

class ListBuilderSpec :
    ShouldSpec({
        context("unaryPlus") {
            should("add an element to the list") {
                val lb =
                    ListBuilder<String>().apply {
                        +"abba"
                        +"dabba"
                    }

                val result = lb.build()

                result.shouldContainExactly("abba", "dabba")
            }
        }
    })
