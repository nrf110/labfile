package labfile.dsl

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainExactly

class PipelineBuilderSpec :
    ShouldSpec({
        context("PipelineBuilder.stage") {
            should("render a list of stages") {
                val result = pipeline {
                    val publish: String by stage()
                    val deploy: String by stage()

                    publish
                    deploy
                }

                result.stages.shouldContainExactly("publish", "deploy")
            }
        }
    })
