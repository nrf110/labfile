package labfile.dsl

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe

class ServicesBuilderSpec :
    ShouldSpec({
        context("ServicesBuilder") {
            should("build a list of services") {
                val sb =
                    ServicesBuilder().apply {
                        +"postgres"
                        +service("kafka")
                        +service("redis") {
                            entrypoint {
                                +"redis"
                                +"-p"
                                +"1234"
                            }
                        }
                    }

                val result = sb.build()
                result.shouldHaveSize(3)
                result[0].name!!.shouldBe("postgres")
                result[1].name!!.shouldBe("kafka")
                result[2].name!!.shouldBe("redis")
                result[2].entrypoint!!.shouldContainExactly("redis", "-p", "1234")
            }
        }
    })
