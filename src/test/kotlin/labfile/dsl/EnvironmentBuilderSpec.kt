package labfile.dsl

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.shouldBe
import labfile.ast.Environment

class EnvironmentBuilderSpec :
    ShouldSpec({
        context("EnvironmentBuilder.build") {
            should("return an Environment with only a name") {
                val result = EnvironmentBuilder("test").build()
                result.name.shouldBe("test")
                result.url.shouldBeNull()
                result.onStop.shouldBeNull()
                result.autoStopIn.shouldBeNull()
                result.action.shouldBeNull()
            }

            should("return a fully populated Environment") {
                val eb =
                    EnvironmentBuilder("test").apply {
                        url = "https://google.com"
                        onStop = "stopit"
                        autoStopIn = "1 hour"
                        action = Environment.Action.PREPARE
                    }

                val result = eb.build()
                result.name.shouldBe("test")
                result.url.shouldBe("https://google.com")
                result.onStop.shouldBe("stopit")
                result.autoStopIn.shouldBe("1 hour")
                result.action.shouldBe(Environment.Action.PREPARE)
            }
        }
    })
