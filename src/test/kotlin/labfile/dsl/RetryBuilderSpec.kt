package labfile.dsl

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.shouldBe
import labfile.ast.Retry.When

class RetryBuilderSpec :
    ShouldSpec({
        context("when") {
            should("return a Retry with only max") {
                val result = RetryBuilder(3).build()
                result.max.shouldBe(3)
                result.`when`.shouldBeNull()
            }

            should("set a list of when conditions") {
                val rb =
                    RetryBuilder(3).apply {
                        `when` {
                            +When.API_FAILURE
                            +When.STUCK_OR_TIMEOUT_FAILURE
                        }
                    }

                val result = rb.build()
                result.max.shouldBe(3)
                result.`when`.shouldContainExactly(When.API_FAILURE, When.STUCK_OR_TIMEOUT_FAILURE)
            }
        }
    })
