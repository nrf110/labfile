package labfile.dsl

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.maps.shouldContainExactly
import labfile.ast.Variable

class VariablesBuilderSpec :
    ShouldSpec({
        context("VariablesBuilder") {
            should("build a list of variables") {
                val builder =
                    VariablesBuilder().apply {
                        "STRING" to "value"
                        "BOOL" to true
                        "NUMBER" to 32.1
                        "JUST_VALUE" to variable { value = "Just a value" }
                        "JUST_DESCRIPTION" to variable { description = "Manual Input Required" }
                        "FULL_VAR" to
                            variable {
                                description = "a helpful hint"
                                value = "my value"
                            }
                    }

                val result = builder.build()
                result.shouldContainExactly(
                    mapOf(
                        "STRING" to Variable(value = "value"),
                        "BOOL" to Variable(value = "true"),
                        "NUMBER" to Variable(value = "32.1"),
                        "JUST_VALUE" to Variable(value = "Just a value"),
                        "JUST_DESCRIPTION" to
                            Variable(value = null, description = "Manual Input Required"),
                        "FULL_VAR" to Variable(description = "a helpful hint", value = "my value")
                    )
                )
            }
        }
    })
