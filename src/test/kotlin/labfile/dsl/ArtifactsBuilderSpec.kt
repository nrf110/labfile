package labfile.dsl

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import labfile.ast.CoverageFormat
import labfile.ast.CoverageReport

class ArtifactsBuilderSpec :
    ShouldSpec({
        context("ArtifactsBuilder.paths") {
            val ab =
                ArtifactsBuilder().apply {
                    paths {
                        +"/a.txt"
                        +"/b.txt"
                    }
                }

            val result = ab.build()
            result.paths!!.shouldContainExactly("/a.txt", "/b.txt")
        }

        context("ArtifactsBuilder.exclude") {
            val ab =
                ArtifactsBuilder().apply {
                    exclude {
                        +"/a.txt"
                        +"/b.txt"
                    }
                }

            val result = ab.build()
            result.exclude!!.shouldContainExactly("/a.txt", "/b.txt")
        }

        context("ArtifactsBuilder.reports") {
            val ab =
                ArtifactsBuilder().apply {
                    reports {
                        apiFuzzing = "apifuzzing"
                        codequality = "codequality"
                        containerScanning = "containerscanning"
                        coverageFuzzing = "coveragefuzzing"
                        coverageReport {
                            coverageFormat = CoverageFormat.COBERTURA
                            path = "cobertura.xml"
                        }
                        dast = "dast"
                        dependencyScanning = "dependencyscanning"
                        dotenv = "test.env"
                        junit { +"junit" }
                        licenseScanning = "licensescanning"
                        loadPerformance = "loadperformance"
                        metrics = "metrics"
                        performance = "performance"
                        requirements = "requirements"
                        sast = "sast"
                        secretDetection = "secretdetection"
                        terraform = "terraform"
                    }
                }

            with(ab.build().reports!!) {
                apiFuzzing.shouldBe("apifuzzing")
                codequality.shouldBe("codequality")
                containerScanning.shouldBe("containerscanning")
                coverageFuzzing.shouldBe("coveragefuzzing")
                coverageReport.shouldBe(
                    CoverageReport(
                        coverageFormat = CoverageFormat.COBERTURA,
                        path = "cobertura.xml"
                    )
                )
                dast.shouldBe("dast")
                dependencyScanning.shouldBe("dependencyscanning")
                dotenv.shouldBe("test.env")
                junit!!.shouldContainExactly("junit")
                licenseScanning.shouldBe("licensescanning")
                loadPerformance.shouldBe("loadperformance")
                metrics.shouldBe("metrics")
                performance.shouldBe("performance")
                requirements.shouldBe("requirements")
                sast.shouldBe("sast")
                secretDetection.shouldBe("secretdetection")
                terraform.shouldBe("terraform")
            }
        }
    })
