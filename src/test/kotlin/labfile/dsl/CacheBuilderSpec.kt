package labfile.dsl

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.shouldBe

class CacheBuilderSpec :
    ShouldSpec({
        context("CacheBuilder.key") {
            should("build a cache with a name key") {
                val cb = CacheBuilder().apply { key("test") }

                val result = cb.build()
                result.key!!.left().shouldBe("test")
            }

            should("build a cache with a file key") {
                val cb =
                    CacheBuilder().apply {
                        key {
                            files {
                                +"a.txt"
                                +"b.txt"
                            }
                        }
                    }

                val result = cb.build()
                val key = result.key!!.right()
                key.prefix.shouldBeNull()
                key.files.shouldContainExactly("a.txt", "b.txt")
            }
        }

        context("CacheBuilder.paths") {
            val cb =
                CacheBuilder().apply {
                    paths {
                        +"/a.txt"
                        +"/b.txt"
                    }
                }

            val result = cb.build()
            result.paths!!.shouldContainExactly("/a.txt", "/b.txt")
        }
    })
