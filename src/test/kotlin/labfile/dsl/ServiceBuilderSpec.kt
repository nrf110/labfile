package labfile.dsl

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe

class ServiceBuilderSpec :
    ShouldSpec({
        context("ServiceBuilder.entrypoint") {
            should("build a list of entrypoint arguments") {
                val sb =
                    ServiceBuilder("redis").apply {
                        entrypoint {
                            +"redis"
                            +"-p"
                            +"1234"
                        }
                    }

                val result = sb.build()
                result.name!!.shouldBe("redis")
                result.entrypoint.shouldContainExactly("redis", "-p", "1234")
            }
        }

        context("ServiceBuilder.command") {
            should("build a list of entrypoint arguments") {
                val sb =
                    ServiceBuilder("redis").apply {
                        command {
                            +"redis"
                            +"-p"
                            +"1234"
                        }
                    }

                val result = sb.build()
                result.name!!.shouldBe("redis")
                result.command.shouldContainExactly("redis", "-p", "1234")
            }
        }
    })
