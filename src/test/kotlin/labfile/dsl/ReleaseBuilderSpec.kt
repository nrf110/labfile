package labfile.dsl

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainExactly

class ReleaseBuilderSpec :
    ShouldSpec({
        context("ReleaseBuilder.milestones") {
            should("build a list of milestones") {
                val rb =
                    ReleaseBuilder("mytag").apply {
                        tagMessage = "This is a message"
                        milestones {
                            +"v1"
                            +"v2"
                        }
                    }

                val result = rb.build()

                result.milestones.shouldContainExactly("v1", "v2")
            }
        }
    })
