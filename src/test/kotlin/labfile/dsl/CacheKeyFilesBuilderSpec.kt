package labfile.dsl

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe

class CacheKeyFilesBuilderSpec :
    ShouldSpec({
        context("CacheFilesBuilder.files") {
            should("require at least one file") {
                val builder = CacheKeyFilesBuilder("/tmp").apply { files {} }

                val ex = shouldThrow<IllegalArgumentException> { builder.build() }

                ex.message.shouldBe("Cache key must consist of either 1 or 2 files")
            }

            should("allow no more than 2 files") {
                val builder =
                    CacheKeyFilesBuilder("/tmp").apply {
                        files {
                            +"a.txt"
                            +"b.txt"
                            +"c.txt"
                        }
                    }

                val ex = shouldThrow<IllegalArgumentException> { builder.build() }

                ex.message.shouldBe("Cache key must consist of either 1 or 2 files")
            }

            should("build a list of 2 files") {
                val builder =
                    CacheKeyFilesBuilder("/tmp").apply {
                        files {
                            +"a.txt"
                            +"b.txt"
                        }
                    }

                val result = builder.build()

                result.prefix.shouldBe("/tmp")
                result.files.shouldContainExactly("a.txt", "b.txt")
            }
        }
    })
