#!/usr/bin/env kotlin
@file:Repository("https://repo1.maven.org/maven2")
@file:DependsOn("org.gitlab4j:gitlab4j-api:4.15.7")
@file:DependsOn("io.gitlab.nrf110:labfile:0.16.0")

import labfile.dsl.*
import org.gitlab4j.api.Constants
import org.gitlab4j.api.GitLabApi

val refName = System.getenv("CI_COMMIT_REF_NAME")
val projectId = System.getenv("CI_PROJECT_ID")
val gradleImage = "gradle:8.1-jdk11-jammy"

val version = nextVersion()

pipeline {
    val test by stage()
    val publish by stage()

    version?.let { variables { "VERSION" to it } }

    job("test") {
        stage = test
        image(gradleImage)
        ivyCache()

        script { +shell("./gradlew check --no-daemon") }

        artifacts { reports { codequality = "build/reports/detekt/gitlab.json" } }

        rules { +rule { `if`("\$CI_COMMIT_BRANCH !~ /^v\\d+\\.\\d+$/") } }
    }

    job("publish") {
        stage = publish
        image(gradleImage)
        ivyCache()

        script { +shell("./gradlew publish closeAndReleaseStagingRepository --no-daemon") }

        afterScript {
            +shell(
                """
                    git remote add api-origin https://oauth2:${'$'}{GITLAB_ACCESS_TOKEN}@gitlab.com/${'$'}{GITLAB_USER_LOGIN}/${'$'}{CI_PROJECT_NAME}.git
                    git config user.email "${'$'}{GITLAB_USER_EMAIL}"
                    git config user.name "${'$'}{GITLAB_USER_NAME}"
                    git tag -a v${'$'}{VERSION} -m "Version created by gitlab-ci Build"
                    git push api-origin v${'$'}{VERSION}
                """
                    .trimIndent()
            )
        }

        rules { +rule { `if`("\$VERSION") } }
    }

    job("pages") {
        stage = publish
        image("node:18-alpine")

        script {
            +shell(
                """
                export BASE_PATH="/labfile"
                npm install -g gatsby-cli --legacy-peer-deps
                cd docs
                npm ci --legacy-peer-deps
                gatsby build --prefix-paths
                cd ..
                mv docs/public public
            """
                    .trimIndent()
            )
        }

        artifacts { paths { +"public" } }

        rules { +rule { `if`("\$VERSION") } }
    }
}

fun JobBuilder.ivyCache() =
    this.cache {
        key("labfile") { files { +"checksum.txt" } }

        paths { +"\${HOME}/.ivy2/cache" }
    }

fun nextVersion(): String? {
    val gitlabApi =
        GitLabApi(
            System.getenv(
                "CI_SERVER_URL",
            ),
            System.getenv("GITLAB_ACCESS_TOKEN")
        )
    return extractVersion()?.let {
        val (major, minor) = it
        val tags =
            gitlabApi.tagsApi.getTags(
                projectId,
                Constants.TagOrderBy.UPDATED,
                Constants.SortOrder.DESC,
                "^v${major}.${minor}."
            )
        val patch =
            if (tags.size > 0) {
                val parts = tags[0].name.split('.')
                parts[parts.size - 1].toInt() + 1
            } else 0
        return "${major}.${minor}.${patch}"
    }
}

fun extractVersion(): Pair<Int, Int>? {
    val pattern = "^v(\\d+)\\.(\\d+)$".toRegex()
    return pattern.find(refName)?.let {
        val (major, minor) = it.destructured
        return major.toInt() to minor.toInt()
    }
}
